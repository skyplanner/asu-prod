({
	loadRecord: function (component) {
		var self = this;
		var parameters = {
			recordId: component.get("v.recordId")
		};
		this._invoke(
			component,
			"c.loadRecord",
			parameters,
			function (result) {
				var warning;
				if (result == "pre-dig-app") {
					warning = $A.get("$Label.c.SP_WARNING_PRE_DIG_APP");
				}

				if (warning) {
					var toastEvent = self.getToastEvent(
						"warning",
						"Warning",
						warning,
						"sticky"
					);
					toastEvent.fire();
				}
			},
			function (error) {
				console.log("Error " + error);
			}
		);
	},

	getToastEvent: function (type, title, message, mode) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			title: title,
			message: message,
			duration: "5000",
			key: "info_alt",
			type: type,
			mode: mode
		});
		return toastEvent;
	},

	_invoke: function (component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters) action.setParams(parameters);

		action.setCallback(this, function (response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":
					errors = response.getError();
					e =
						(errors && errors[0] && errors[0].message) ||
						"Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}
});
