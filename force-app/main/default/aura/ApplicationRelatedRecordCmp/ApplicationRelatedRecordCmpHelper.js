({
    COLUMNS_SCHOOL : [
		{ label: 'Is Current', fieldName: 'Is_Current_School__c', type: 'boolean', hideDefaultActions: "true"},
		{ label: 'School name', fieldName: 'Name', type: 'text', hideDefaultActions: "true"},
		{ label: 'State',  fieldName: 'School_State__c', type: 'text' , hideDefaultActions: "true"},
		{ label: 'City', fieldName: 'School_City__c', type: 'text', hideDefaultActions: "true"}
		/*{ label: 'Year', fieldName: 'School_Year__c', type: 'text', hideDefaultActions: "true"}  */       
	],  

	COLUMNS_COURSE : [
		{ label: 'School Name', fieldName: 'SchoolName', type: 'text', hideDefaultActions: "true"},
		{ label: 'Course Name', fieldName: 'Course_Name__c', type: 'text', hideDefaultActions: "true"},
		{ label: 'Course Status',  fieldName: 'Course_Status__c', type: 'text' , hideDefaultActions: "true"},      
		{ label: 'Course Type',  fieldName: 'Course_Type__c', type: 'text' , hideDefaultActions: "true"},  
		{ label: 'Year', fieldName: 'School_Year__c', type: 'text', hideDefaultActions: "true"},      
		{ label: 'Has Honors',  fieldName: 'Honors__c', type: 'text' , hideDefaultActions: "true"},       
		{ label: 'Letter',  fieldName: 'Letter', type: 'text' , hideDefaultActions: "true"},     
		{ label: 'Credits',  fieldName: 'Credits', type: 'text' , hideDefaultActions: "true"}       
	],  
   
    loadSchools : function(component) {
	   var self = this;	
       var params = {
			applicationId: component.get("v.applicationId")		
		};
        this._invoke(
            component,
            'c.getSchools',
            params,
            function(result) {
			  console.log('Result ' + JSON.stringify(result)); 		
			  component.set("v.schoolsList", result);
			  component.set("v.columns", self.COLUMNS_SCHOOL); 
			  var optionsArray = component.get("v.options");
			  optionsArray.push({ label: 'Continue in the application process', value: 'continue'});	
			  optionsArray.push({ label: 'Add another high school', value: 'new'});			 
			  if(result.length > 1){				
				  optionsArray.push({ label: 'Edit an existing reported high School', value: 'edit'});
				  optionsArray.push({ label: 'Delete an existing reported high School', value: 'delete'});		  
			  }	
			  component.set("v.options", optionsArray);			               
            },
            function(error){
              console.log('Error ' + error); 	
            }
        );
	},
	loadCourses : function(component) {
		var self = this;	
		var params = {
			 applicationId: component.get("v.applicationId")		
		 };
		 this._invoke(
			 component,
			 'c.getCourses',
			 params,
			 function(result) {
			   var courses = result;
			   var optionsArray = component.get("v.options");
			   optionsArray.push({ label: 'Continue in the application process', value: 'continue'});
			   optionsArray.push({ label: 'Add a New Course', value: 'new'});  
			  
			   if(courses.length > 0){
				component.set("v.totalCredits", courses[0].Student_Application__r.Total_Credits_Earned__c);
				for (var i = 0; i < courses.length; i++) {
					var row = courses[i];
					if (row.School__r) row.SchoolName = row.School__r.Name;
					if (row.Course_Status__c == 'Completed'){
						row.Letter = row.Letter_Grade_Earned__c;
						row.Credits = row.Credits_Earned__c;
					} 
					else{
						row.Letter = row.Current_Letter_Grade__c; 
						row.Credits = '-';
					}
				  }
				  component.set("v.coursesList", courses);	
				  component.set('v.columns', self.COLUMNS_COURSE);
			
				  optionsArray.push({ label: 'Edit an existing Reported Course', value: 'edit'});
				  optionsArray.push({ label: 'Delete an existing Reported Course', value: 'delete'});  
				 
			   } 
			   component.set("v.options", optionsArray);
			 },
			 function(error){
			   console.log('Error ' + error); 	
			 }
		 );
	 },
    _invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":				
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}	
})