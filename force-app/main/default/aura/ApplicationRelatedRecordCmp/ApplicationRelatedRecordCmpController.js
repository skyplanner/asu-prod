({
    doInit : function(component, event, helper) {
        console.log('Init 2');
        var relatedRecordType = component.get("v.relatedRecordType");       
        console.log(relatedRecordType);    
        switch (relatedRecordType){
            case "School" :   
              helper.loadSchools(component);          
              break;
            case "Course" :   
              helper.loadCourses(component);      
              break;  
        }       
    },

    handleRowSelection: function(component, event, helper) {    
        var selectedRows = event.getParam("selectedRows");
        component.set("v.relatedRecordId", selectedRows[0].Id);    
    }
})