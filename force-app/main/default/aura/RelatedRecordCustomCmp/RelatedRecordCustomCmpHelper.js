({
  
    loadContacts : function(component) {
        var title="Contacts";
        var existingContactArray=[];
        var params = {
             accId: component.get("v.accountId")		
         };
         this._invoke(
             component,
             'c.getAllContact',
             params,
             function(result) {
               var maxCount = component.get("v.contactNumber");
               var viewAll = component.get("v.viewAll");
                if(viewAll || result.length <= maxCount){
                  component.set("v.contactTitle",title + ' ' + '(' + result.length + ')');
                  component.set("v.existingContactList",result);
                }
                else
                {
                    component.set("v.contactTitle",title + ' ' + '(' + maxCount +'+' + ')');                 
                    for(var i=0; i<maxCount;i++)
                    {
                            existingContactArray.push(result[i]);                        
                    }
                    component.set("v.existingContactList",existingContactArray);
                }
             },
             function(error){
              component.set("v.hasError", true);
               console.log('Error ' + error); 	
             }
         );
     },

     loadUserContact : function(component) {
         console.log('UserId' +  $A.get("$SObjectType.CurrentUser.Id"));
         var userId = $A.get("$SObjectType.CurrentUser.Id");
         var params = {
            userId: userId		
         };
         this._invoke(
             component,
             'c.getContactAccess',
             params, 
             function(result) {
               if(result != null) {               
                component.set("v.userContactId" , result.contactId );               
                component.set("v.hasPermission" , result.hasAccess );
              } 
              
             },
             function(error){
               component.set("v.hasError", true);
               console.log('Error ' + error); 	
             }
         );
     },
    
    _invoke: function(component, methodName, parameters, onSuccess, onError) {
		var action, errors, e;
		action = component.get(methodName);

		if (parameters)
			action.setParams(parameters);

		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					onSuccess(response.getReturnValue());
					break;
				case "ERROR":				
					errors = response.getError();
					e = (errors && errors[0] && errors[0].message) || "Unknow Error";
					(onError ? onError : console.error)(e);
			}
		});
		$A.enqueueAction(action);
	}	
})