({
  doInit : function (component, event, helper) {
       console.log('RelatedRecord v.1');
       helper.loadContacts(component);
       helper.loadUserContact(component);
  },
 addContact: function(component, evt, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
            "url": '/asuprepacademy/s/new-contact'
            });
        urlEvent.fire();   
        console.log('Add contact');
  },
  editContact: function(component, evt, helper) {
    var idx = evt.getSource().get("v.name");
    var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        "url": '/asuprepacademy/s/contact/'+idx
        });
    urlEvent.fire();   
    console.log('Edit contact');   
    console.log('idx ' + idx );
}  
})