/**
 * @File Name          : SP_StudentApplications_Trigger.trigger
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 07-14-2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
 **/
trigger SP_StudentApplications_Trigger on Application__c(
	before insert,
	before update,
	before delete
) {
	SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();

	//If trigger custom setting is active
	if (spts.SP_ApplicationTrigger__c || Test.isRunningTest()) {
		if (Trigger.isBefore && Trigger.isDelete) {
			SP_StudentApplicationTrigger_Utility.handleRelatedDocument(
				Trigger.OldMap.keySet()
			);
		}
		if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
			SP_StudentApplicationTrigger_Utility.handleBeforeInsertUpdate(
				Trigger.new,
				Trigger.oldMap
			);
		}
	}
}
