/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 03-04-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-04-2021   dmorales   Initial Version
**/
trigger SP_SchoolTrigger on School__c (before insert, before delete, before update) {
    SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();
    //If trigger custom setting is active
    if(spts.SP_SchoolTrigger__c || Test.isRunningTest()) {
        if(Trigger.isInsert || Trigger.isUpdate){
            SchoolTriggerUtility.handleCurrentSchoolSync(Trigger.new);
        }  
        if(Trigger.isDelete){
            SchoolTriggerUtility.handlePreventCurrentSchoolDeletion(Trigger.old);
        }        
    }   
}