/**
 * @author       Yaneivys Gutierrez
 * @date         03/22/2021
 * @description  Trigger for ASU Prep Data Object
 */
trigger SP_ASUPrepDataTrigger on ASU_Prep_Data__c(after insert) {
    SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();

    if (!spts.SP_ASUPrepDataTrigger__c && !Test.isRunningTest()) {
        System.debug('Bypassing trigger due to custom setting.');
        return;
    }

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            SP_ASUPrepDataTriggerUtility.handleAfterInsert(
                Trigger.NewMap.keySet()
            );
        }
    }
}