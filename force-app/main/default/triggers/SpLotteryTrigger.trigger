/*
 * @author				Yaneivys Gutierrez
 * @date				05/13/2022
 * @description			Lottery Trigger
 * @group
 * @last modified on	05/13/2022
 * @last modified by	05/13/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/13/2022		Yaneivys Gutierrez		Initial Version
 */
trigger SpLotteryTrigger on Lottery__c(before update) {
	SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();

	//If trigger custom setting is active
	if (spts.SP_LotteryTrigger__c || Test.isRunningTest()) {
		if (Trigger.isBefore && Trigger.isUpdate) {
			SpLotteryTriggerUtility.handleBeforeUpdate(
				Trigger.new,
				Trigger.oldMap
			);
		}
	}
}
