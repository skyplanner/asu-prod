/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 03-16-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-16-2021   dmorales   Initial Version
**/
trigger SP_AccountTrigger on Account (before insert, before update) {
    SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();
    //If trigger custom setting is active
    if(spts.SP_AccountTrigger__c || Test.isRunningTest()) {   
        if(Trigger.isInsert || Trigger.isUpdate){
           AccountTriggerUtility.checkHouseholdAccountOwnership(Trigger.new);
        }
    }   
}