/**
 * @author       Yaneivys Gutierrez
 * @date         03/18/2021
 * @description  Trigger for Case Object
 */
trigger SP_CaseTrigger on Case(after insert, after update) {
    SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();

    if (!spts.SP_CaseTrigger__c && !Test.isRunningTest()) {
        System.debug('Bypassing trigger due to custom setting.');
        return;
    }

    if (Trigger.isAfter) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            SP_CaseTriggerUtility.handleAfterInsertUpdate(
                Trigger.New,
                Trigger.OldMap
            );
        }
    }
}