/**
 * @File Name          : SP_ContentDocumentLink_Trigger.trigger
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-22-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   dmorales     Initial Version
**/
trigger SP_ContentDocumentLink_Trigger on ContentDocumentLink (before insert) {

    SP_TriggerSwitch__c spts = SP_TriggerSwitch__c.getOrgDefaults();
    //If trigger custom setting is active
    if(spts.SP_ContentDocumentTrigger__c || Test.isRunningTest()) {
        SP_ContentDocumentLinkTrigger_Utility.handleApplicationDocumentsRestrictions(Trigger.New);   
        SP_ContentDocumentLinkTrigger_Utility.handleContentDocumentSync(Trigger.New);             
    }
 

}