var capp = angular.module("CSelfRegApp", ['ngSanitize']);
capp.controller("selfRegController", function ($scope, $q, $timeout, $location, $window) {
    
    //variables definition in $scope 
    $scope.loading = true;
    $scope.globalErrorMessages = {
       'SITE_EXCEPTION' : 'We are having trouble creating your user.',
       'CUSTOM_EXCEPTION' : 'We have found a problem with the information provided. This could be because similar data already exists in our system.',
       'GENERAL' : 'Sorry!! We are having troubles on our servers. Please try another time.'
    }
    $scope.validation = {
        submitErrors : false,
        passNotMatch : false,
        passPatternNotMatch : false,
        passContainEmail : false,
        emailNotMatch : false,
        birthdayError: false,
        formHasError: false,
        errorBdMsg: '',
        errorMsg : '',
        isLoading: false,
        successCreation: false
    };

  

    $scope.typeUsersList = [{label:'Student', value:'student'},
                            {label:'Parent/Guardian', value:'parent'},
                            {label:'Emancipated minor', value:'emancipated'}];
                            
    $scope.globalCountry = [];
    $scope.globalCountryCode = [];  
    $scope.globalStates = [];  

    $scope.newUser = {
        firstName: '',
        lastName: '',
        email: '',
        confirmEmail: '',
        password: '',
        repeatedPassw: '',       
        phone: '',
        phoneArea: '+1 United States of America',
        birthday: '',
        userAge: 18,       
        address:'',
        zipCode:'',
        city:'',
        state:'',
        country:'United States of America',
        typeUser:'student',
        parentFirstName: '',
        parentLastName: '',
        parentEmail: '',
        optInMarketing : false,
        optInEnrollment: false,
        privacy: false
    }

    $scope.handleBirthdayError = function(errorText) {
        $scope.validation.birthdayError = true;
        $scope.validation.errorBdMsg = errorText;    
        $scope.newUser.userAge = 0;    
      }

    $scope.calculateAge = function(birthday) { // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }  

    $scope.calculateAgeFromString = function (dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
      {
          age--;
      }
      return age;
   }
  
    $scope.$watch("newUser.birthday", function(newVal, oldValue) {
        var oldLength = oldValue ? oldValue.length : 0;
        var newLength = newVal ? newVal.length : 0;
        var day, year;
        var currentDay = new Date();
        var dateB = Date.parse($scope.newUser.birthday);
        var currentYear = currentDay.getFullYear();
        var numbersRegex = /^[\d\/-]+$/;      
        /* checks for MM/DD/YYYY format and valid febrary until 29 and other month restriction such as april until 30 */
        var dateRegex = /(02\/(0[1-9]|[12][0-8])|(0[13578]|1[02])\/(0[1-9]|[12][0-9]|3[01])|(0[469]|11)\/(0[1-9]|[12][0-9]|30))\/(\d{4})/;
          
        if (angular.isDefined(newVal)) {
          if(newLength === 0){
            return;
          }
          if(newLength > 0 && !dateRegex.test($scope.newUser.birthday)){
            $scope.handleBirthdayError("This is not a valid date (format: MM/DD/YYYY)");
          }
          else{
            year = $scope.newUser.birthday.slice(6);
            if (year < currentYear - 120) {
              $scope.handleBirthdayError("It's not possible to have that many grey hairs!");
            } else if ( dateB > currentDay) {
              $scope.handleBirthdayError("Are you from the future?");
            } else {            
               // var dateB = new Date(Date.parse($scope.newUser.birthday));
                $scope.newUser.userAge = $scope.calculateAgeFromString($scope.newUser.birthday);
                 if($scope.newUser.userAge < 5){
                  $scope.handleBirthdayError("You are too young!!");
                }
                else{
                  $scope.validation.birthdayError = false;
                  $scope.validation.errorBdMsg = "";
                }
                       
            }
          }   
        }
             
    });
  
    $scope.$watch("[newUser.email, newUser.confirmEmail]", function(newVal, oldVal) {
            $scope.validateEmailMatch();         
            if(newVal[0] != oldVal[0]){//email was changed
              $scope.validPasswNotEmail();
            } 
    }, true);

    $scope.$watch("[newUser.password, newUser.repeatedPassw]", function(newVal, oldVal) {
      $scope.validatePasswMatch();
      if(newVal[0] != oldVal[0]){//pasw was changed
        $scope.validPasswNotEmail();
        var valid = $scope.validPaswPattern();
        $scope.validation.passPatternNotMatch = !valid;
      } 
   }, true);
    
   $scope.validPaswPattern = function(){
     var str = $scope.newUser.password;
     var countGroups=0;
     var lowerRgx = /[a-z]+/;
     var uperRgx = /[A-Z]+/;
     var numbRgx = /[0-9]+/;
     var specialRgx = /[!%*_\-+=:./?]+/;
     if(lowerRgx.test(str))
          countGroups++;
      if(uperRgx.test(str))
          countGroups++;
      if(numbRgx.test(str))
          countGroups++;
      if(specialRgx.test(str))
          countGroups++;    
      if(countGroups >= 3)    
        return true;
      else 
        return false;       
   }
   $scope.validPasswNotEmail  = function() {
        
        var email = $scope.newUser.email;
        var passw = $scope.newUser.password;  
        if(angular.isDefined(email) && angular.isDefined(passw) && email != '' && passw != ''){      
              var splitEmail = email.split('@');          
              $scope.validation.passContainEmail = passw.includes(splitEmail[0]);         
        }
         else{
              $scope.validation.passContainEmail = false;
        }       
    };

    $scope.validatePasswMatch  = function() {
      var passw = $scope.newUser.password;   
      var passwMatch = $scope.newUser.repeatedPassw;   
      
      if(angular.isDefined(passw) && angular.isDefined(passwMatch) && passw != '' && passwMatch != ''){      
          $scope.validation.passNotMatch = !(passw === passwMatch);         
       }
       else{
          $scope.validation.passNotMatch = false;
       }       
    };

    $scope.validateEmailMatch = function(){
      var email = $scope.newUser.email;   
      var emailMatch = $scope.newUser.confirmEmail;  
     
      if(angular.isDefined(email) && angular.isDefined(emailMatch) && email != '' && emailMatch != ''){      
          $scope.validation.emailNotMatch = !(email === emailMatch);         
       }
       else{
          $scope.validation.emailNotMatch = false;
       }    
    }

    
    $scope.$watch("[validation.passNotMatch, validation.passContainEmail, validation.birthdayError, validation.emailNotMatch, validation.submitErrors]", function(newVal, oldValue) {               
     
      $scope.validation.formHasError = ($scope.validation.passNotMatch || 
                                            $scope.validation.passContainEmail || 
                                            $scope.validation.emailNotMatch || 
                                            $scope.validation.birthdayError ||
                                            $scope.validation.submitErrors )
    }, true);
 

    $scope.registerUser = function () {
      
        $scope.validation.isLoading = true;
        $scope.validation.submitErrors = false;
        return $scope.callGeneric(
            'CustomCommunitySelfRegistrationCtrl',
            'registerUser',
            [$scope.newUser]
            )
            .then(function (result) {
                if (result) {
            
                   if(result.returnUrl && result.errorMessage == ''){                   
                      $scope.validation.successCreation = true;
                   }                    
                   else {                 
                     console.log(result.errorMessage);
                     $scope.validation.submitErrors = true;
                     if(result.errorMessage.includes('ExternalUserCreateException')){
                        $scope.validation.errorMsg = $scope.globalErrorMessages.SITE_EXCEPTION;
                        var errorSplit = result.errorMessage.split("ExternalUserCreateException: "); 
                        if(errorSplit.length > 1){
                          // var serverError = errorSplit[1].replace(/[\[\]]/g, '');
                           $scope.validation.errorMsg += ' The server says: ' + errorSplit[1];
                        }
                     }                       
                  
                     if(result.errorMessage.includes('CommunityManagementCustomException'))   
                        $scope.validation.errorMsg = $scope.globalErrorMessages.CUSTOM_EXCEPTION;                   
                     if(result.errorMessage.includes('GlobalException')) 
                        $scope.validation.errorMsg = $scope.globalErrorMessages.GENERAL;                   
                   } 
                  
                }
                else {
                   console.log(result); 
                   console.log('Error in server');
                   $scope.validation.errorMsg =  $scope.globalErrorMessages.GENERAL;;
                   $scope.validation.submitErrors = true;                   
                }              
            })
            .catch(function (error) {
               console.log(error);
               console.log('error in Remote Call');
               $scope.validation.errorMsg =  $scope.globalErrorMessages.GENERAL;
               $scope.validation.submitErrors = true;                         
            })
            .finally(function () {
               $scope.validation.isLoading = false;   
           /*    if($scope.validation.submitErrors){
                  $scope.newUser.email = '';
                  $scope.validation.formHasError = true;              
               }*/
               console.log('Finally');
            });
    };

    $scope.initData = function (){

      return $scope.callGeneric(
        'CustomCommunitySelfRegistrationCtrl',
        'getPicklistObjects',
         []
        )
        .then(function (result) {
            if (result) {
        //      console.log(result[0]);
              $scope.globalCountry = result.countryPicklist;        
              $scope.globalCountryCode = result.countryCodePicklist;        
              $scope.globalStates = result.statePicklist;        
            }                   
        })
        .catch(function (error) {
           console.log(error);
           console.log('error in Remote Call');                            
        })
        .finally(function () {
          console.log('Finally');
        });
    }

    $scope.callGeneric = function (ctrl, method, params, remoteParams) {      
        var deferred = $q.defer();
        var defaultRemoteParams = {
            buffer: false,
            escape: false
        };
        angular.extend(defaultRemoteParams, remoteParams);
        window[ctrl][method].apply(
            window[ctrl], // value for 'this'
            (params || []).concat([
                function (result, event) {
                    if (event.status && event.statusCode == 200) {
                        //$scope.global.loading = false;
                        deferred.resolve(result, event);
                    }
                    else {
                        //$scope.global.loading = false;
                        deferred.reject(result, event);
                    }
                },
                defaultRemoteParams
            ]));
        return deferred.promise;
    };

    $scope.initData();
 
});
