/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpLotteryBatch, SpLotteryBatchHelper, SpLotteryBatchScheduler
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryBatchTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void batchTest() {
		Id appId = SP_TestDataFactory.createPossibleApplicationForLottery();
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		SpLotteryBatch lotBatch = new SpLotteryBatch();
		Id batchId = Database.executeBatch(lotBatch);

		Test.stopTest();

		Application__c app = [
			SELECT
				Id,
				Lottery__c,
				Lottery_Status__c,
				Lottery_Selection_Number__c,
				Lottery_WL_No__c
			FROM Application__c
			WHERE Id = :appId
		];

		System.assertEquals(
			lId,
			app.Lottery__c,
			'Wrong Lottery Application Assignment'
		);
		System.assertEquals(
			SpApplicationConst.LOT_WAITING_LIST,
			app.Lottery_Status__c,
			'Wrong Lottery Status Application Assignment'
		);
		System.assertEquals(
			null,
			app.Lottery_Selection_Number__c,
			'Wrong Lottery Selection Number Application Assignment'
		);
		System.assertEquals(
			2,
			app.Lottery_WL_No__c,
			'Wrong Lottery Waiting List Order Application Assignment'
		);
	}

	@isTest
	static void schedulerTest() {
		Test.startTest();

		final String CRON_EXP = '0 0 0 3 9 ? 2022';
		String jobId = System.schedule(
			'SpLotteryBatchTest',
			CRON_EXP,
			new SpLotteryBatchScheduler()
		);

		SpLotteryBatchScheduler.scheduleBatch();

		Test.stopTest();

		CronTrigger ct = [
			SELECT Id, CronExpression, TimesTriggered, NextFireTime
			FROM CronTrigger
			WHERE id = :jobId
		];

		System.assertEquals(0, ct.TimesTriggered, 'Should have');
	}

	@isTest
	static void dobelSchedulerTest() {
		final String CRON_EXP = '0 0 0 3 9 ? 2022';
		String jobId = System.schedule(
			'SpLotteryBatchTest',
			CRON_EXP,
			new SpLotteryBatchScheduler()
		);

		SpLotteryBatchScheduler.scheduleBatch();

		Test.startTest();

		SpLotteryBatchScheduler.scheduleBatch();

		Test.stopTest();

		CronTrigger ct = [
			SELECT Id, CronExpression, TimesTriggered, NextFireTime
			FROM CronTrigger
			WHERE id = :jobId
		];

		System.assertEquals(0, ct.TimesTriggered, 'Should have');
	}
}
