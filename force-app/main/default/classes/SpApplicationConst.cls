/**
 * @author				Yaneivys Gutierrez
 * @date				05/05/2022
 * @description			Student Application Constants
 * group
 * @last modified on	05/05/2022
 * @last modified by	05/05/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/05/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpApplicationConst {
	public static final String PREP_DIG_SITE = 'ASU PREP DIGITAL';
	public static final String PREP_DIG = 'Prep Digital';
	public static final String IMMERSION = 'Immersion';
	public static final String FULL_TIME = 'Full Time';
	public static final String IMMERSION_FT = 'Immersion-FT';

	public static final String STATUS_PENDING_FR = 'Pending Final Review';
	public static final String STATUS_WAITLISTED = 'Waitlisted';
	public static final String STATUS_HOLD = 'Hold';

	public static final String SEL_TYPE_AUTOMATIC = 'Automatic';
	public static final String SEL_TYPE_MANUAL = 'Manual';

	public static final String LOT_AVAILABLE = 'Available';
	public static final String LOT_SELECTED = 'Selected';
	public static final String LOT_WAITING_LIST = 'Waiting List';
	public static final String LOT_WITHDREW = 'Lottery Withdrew';
}
