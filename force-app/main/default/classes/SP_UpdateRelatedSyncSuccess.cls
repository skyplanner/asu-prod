/**
 * @File Name          : SP_UpdateRelatedSyncSuccess.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/3/2020   dmorales     Initial Version
**/
public without sharing class SP_UpdateRelatedSyncSuccess {
    @InvocableMethod(label='Set Content Sent'
    description='Set Content Version Sent when Application were Sync Successfully')
    public static void handleSyncProcessSuccess(List<Application__c> appList) {        
        SP_ApplicationsSyncManagement.handleUpdateRelatedContent(appList, false, false, true);        
    }
}