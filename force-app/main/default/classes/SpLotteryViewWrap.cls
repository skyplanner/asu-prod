/*
 * @author				Yaneivys Gutierrez
 * @date				05/04/2022
 * @description			Lottery View Wrap
 * @group
 * @last modified on	05/04/2022
 * @last modified by	05/04/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/04/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryViewWrap {
	@AuraEnabled
	public Boolean canEdit { get; set; }

	@AuraEnabled
	public Lottery__c lottery { get; set; }

	@AuraEnabled
	public List<Application__c> selectedApps { get; set; }

	@AuraEnabled
	public List<Application__c> waitingList { get; set; }
}
