/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpLotteryProcessManager
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryProcessManagerTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void runLotteryNullParamsTest() {
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = null;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		System.assertEquals('error', result.variant, 'Wrong Lottery Creation');
		System.assertEquals(
			System.Label.SP_LOTTERY_DATA_ERROR,
			result.msg,
			'Wrong Lottery Creation'
		);
	}

	@isTest
	static void runLotteryAlreadyCreatedTest() {
		Lottery__c l = [SELECT Id, Name FROM Lottery__c LIMIT 1];
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP POLYTECHNIC';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 5;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		System.assertEquals(
			'warning',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			System.Label.SP_LOTTERY_PRE_CREATED.replace('{0}', l.Name),
			result.msg,
			'Wrong Lottery Creation'
		);
	}

	@isTest
	static void runLotteryNoAppTest() {
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP POLYTECHNIC';
		setup.term = 'Fall';
		setup.year = '2035';
		setup.seats = 5;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		System.assertEquals(
			'warning',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			System.Label.SP_LOTTERY_NO_DATA,
			result.msg,
			'Wrong Lottery Creation'
		);
	}

	@isTest
	static void runLotteryWrongTest() {
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 0;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		System.assertEquals('error', result.variant, 'Wrong Lottery Creation');
		System.assertEquals(
			System.Label.SP_LOTTERY_NO_CREATED,
			result.msg,
			'Wrong Lottery Creation'
		);
	}

	@isTest
	static void runLotteryTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 1;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		lId = [SELECT Id FROM Lottery__c WHERE Id != :lId LIMIT 1].Id;

		System.assertEquals(
			'success',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			lId,
			result.data.lotteryId,
			'Wrong Lottery Created'
		);
	}

	@isTest
	static void runLotterySelecingAllTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 5;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		lId = [SELECT Id FROM Lottery__c WHERE Id != :lId LIMIT 1].Id;

		System.assertEquals(
			'success',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			lId,
			result.data.lotteryId,
			'Wrong Lottery Created'
		);
	}

	@isTest
	static void updateAppSelTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 1;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		lId = [SELECT Id FROM Lottery__c WHERE Id != :lId LIMIT 1].Id;

		System.assertEquals(
			'success',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			lId,
			result.data.lotteryId,
			'Wrong Lottery Created'
		);
	}

	@isTest
	static void updateAppWLTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 1;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.runLottery(setup);

		Test.stopTest();

		lId = [SELECT Id FROM Lottery__c WHERE Id != :lId LIMIT 1].Id;

		System.assertEquals(
			'success',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			lId,
			result.data.lotteryId,
			'Wrong Lottery Created'
		);
	}

	@isTest
	static void inactivateProcessNullTest() {
		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.inactivateProcess(
			null
		);

		Test.stopTest();

		System.assertEquals(
			'error',
			result.variant,
			'Worng Lottery Inactivation process'
		);
		System.assertEquals(
			System.Label.SP_LOTTERY_INVALID_ID_INACTIVATION,
			result.msg,
			'Wrong Inactivation Lottery process'
		);
	}

	@isTest
	static void inactivateProcessWrongIdTest() {
		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.inactivateProcess(
			'a3D8D000000UzkhUAC'
		);

		Test.stopTest();

		System.assertEquals(
			'error',
			result.variant,
			'Worng Lottery Inactivation process'
		);
		System.assertEquals(
			System.Label.SP_LOTTERY_INACTIVATION_ISSUE,
			result.msg,
			'Wrong Inactivation Lottery process'
		);
	}

	@isTest
	static void inactivateProcessTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryProcessManager.inactivateProcess(lId);

		Test.stopTest();

		List<Lottery__c> lList = [
			SELECT Id
			FROM Lottery__c
			WHERE Active__c = FALSE
		];

		System.assertEquals('success', result.variant, 'Lottery not Inactive');
		System.assertEquals(
			System.Label.SP_LOTTERY_INACTIVATION_SUCCESS,
			result.msg,
			'Wrong Inactivation Lottery process'
		);
		System.assertEquals(
			1,
			lList.size(),
			'Wrong Inactive Lottery List Number'
		);
	}

	@isTest
	static void addAppsToLotteryAndReOrderWlTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 1;

		SpMsgResultWrap lotResult = SpLotteryProcessManager.runLottery(setup);
		SpLotteryViewWrap result = SpApplicationManager.getLotteryData(
			lotResult.data.lotteryId
		);
		Application__c app = result.selectedApps[0];
		app.Application_Status__c = 'Withdrew';
		update app;

		SpLotteryProcessManager.reCalculateSelectionNo(
			new Set<Id>{ lotResult.data.lotteryId }
		);

		List<Id> appIds = new List<Id>{ result.waitingList[0].Id };

		Test.startTest();

		Lottery__c l = SpLotteryProcessManager.addAppsToLottery(
			lotResult.data.lotteryId,
			appIds
		);

		Test.stopTest();

		result = SpApplicationManager.getLotteryData(lotResult.data.lotteryId);

		System.assertEquals(
			0,
			result.lottery.Available_Seats__c,
			'Wrong App added to Lottery process'
		);
		System.assertEquals(
			1,
			result.waitingList.size(),
			'Wrong App added to Lottery process'
		);
		System.assertEquals(
			1,
			result.waitingList[0].Lottery_WL_No__c,
			'Wrong App added to Lottery process'
		);
	}

	@isTest
	static void addAppsToLotteryWithAvailSeatsTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 5;

		SpMsgResultWrap lotResult = SpLotteryProcessManager.runLottery(setup);
		Application__c app = new Application__c(
			Lottery__c = lotResult.data.lotteryId,
			Lottery_Status__c = SpApplicationConst.LOT_WAITING_LIST,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = 1,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP DIGITAL',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName4',
			Student_Last_Name__c = 'TestingStudentLastName4',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;

		List<Id> appIds = new List<Id>{ app.Id };

		Test.startTest();

		Lottery__c l = SpLotteryProcessManager.addAppsToLottery(
			lotResult.data.lotteryId,
			appIds
		);

		Test.stopTest();

		SpLotteryViewWrap result = SpApplicationManager.getLotteryData(
			lotResult.data.lotteryId
		);

		System.assertEquals(
			1,
			result.lottery.Available_Seats__c,
			'Wrong App added to Lottery process'
		);
		System.assertEquals(
			0,
			result.waitingList.size(),
			'Wrong App added to Lottery process'
		);
	}

	@isTest
	static void reCalculateSelectionNoTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 5;

		SpMsgResultWrap lotResult = SpLotteryProcessManager.runLottery(setup);
		SpLotteryViewWrap result = SpApplicationManager.getLotteryData(
			lotResult.data.lotteryId
		);
		Application__c app = result.selectedApps[0];
		app.Application_Status__c = 'Withdrew';
		update app;

		Test.startTest();

		SpLotteryProcessManager.reCalculateSelectionNo(
			new Set<Id>{ lotResult.data.lotteryId }
		);

		Test.stopTest();

		result = SpApplicationManager.getLotteryData(lotResult.data.lotteryId);

		System.assertEquals(
			3,
			result.lottery.Available_Seats__c,
			'Wrong reCalculate Selection process'
		);
		System.assertEquals(
			2,
			result.selectedApps.size(),
			'Wrong reCalculate Selection process'
		);
		System.assertEquals(
			0,
			result.waitingList.size(),
			'Wrong reCalculate Selection process'
		);
	}

	@isTest
	static void deleteLotteryTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		SpLotteryProcessManager.deleteLottery(lId);

		Test.stopTest();

		List<Lottery__c> lList = [SELECT Id FROM Lottery__c WHERE Id = :lId];

		System.assertEquals(0, lList.size(), 'Wrong Lottery delete process');
	}
}
