/*
 * @author				Yaneivys Gutierrez
 * @date				05/05/2022
 * @description			Batch Scheduler to Lottery update Waiting List
 * @group
 * @last modified on	05/05/2022
 * @last modified by	05/05/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/05/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryBatchScheduler implements Schedulable {
	public static final String JOB_NAME = 'Lottery Batch';
	private final Integer BATCH_SIZE = 200;

	public void execute(SchedulableContext sc) {
		SpLotteryBatch batch = new SpLotteryBatch();
		Database.executeBatch(batch, BATCH_SIZE);
	}

	private static Boolean findAndAbortJob() {
		List<CronJobDetail> foundJobs = [
			SELECT Id
			FROM CronJobDetail
			WHERE Name = :JOB_NAME
			LIMIT 1
		];

		if (!foundJobs.isEmpty()) {
			List<CronTrigger> foundCronTriggers = [
				SELECT Id
				FROM CronTrigger
				WHERE CronJobDetailId = :foundJobs[0].Id
			];

			if (!foundCronTriggers.isEmpty()) {
				System.abortJob(foundCronTriggers[0].Id);
				return true;
			}
		}

		return true;
	}

	public static void scheduleBatch() {
		if (findAndAbortJob()) {
			String sch = SpLotterySetupMDT.getInstance()
				.LOT_MDT_SETTING.LotteryBatch_CronExp__c;
			String jobID = System.schedule(
				JOB_NAME,
				sch,
				new SpLotteryBatchScheduler()
			);
		}
	}
}
