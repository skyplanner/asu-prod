/*
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Lottery Op[tions Wrap
 * @group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryOptionsWrap {
	@AuraEnabled
	public Boolean canEdit { get; set; }

	@AuraEnabled
	public List<Map<String, String>> gradeOptions { get; set; }

	@AuraEnabled
	public Map<String, List<Map<String, String>>> gradeOptionsDependencies {
		get;
		set;
	}

	@AuraEnabled
	public List<Map<String, String>> siteOptions { get; set; }

	@AuraEnabled
	public List<Map<String, String>> termOptions { get; set; }
}
