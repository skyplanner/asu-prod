/*
 * @author				Yaneivys Gutierrez
 * @date				05/13/2022
 * @description			Lottery Edit Permission Metadata Type Manager Class
 * @group
 * @last modified on	05/13/2022
 * @last modified by	05/13/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/13/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryEditPermissionMDT {
	public static Set<Id> getUserIds() {
		Set<Id> ids = new Set<Id>();

		List<Lottery_Edit_Permission__mdt> permissionMdt = [
			SELECT User_Id__c
			FROM Lottery_Edit_Permission__mdt
		];

		for (Lottery_Edit_Permission__mdt p : permissionMdt) {
			ids.add(p.User_Id__c);
		}

		return ids;
	}
}
