/**
 * @author				Yaneivys Gutierrez
 * @date				05/13/2022
 * @description			Test class for SpLotteryEditPermissionMDT
 * group
 * @last modified on	05/13/2022
 * @last modified by	05/13/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/13/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryEditPermissionMDTTest {
	@isTest
	static void getUserIdsTest() {
		Test.startTest();

		Set<Id> ids = SpLotteryEditPermissionMDT.getUserIds();

		Test.stopTest();

		System.assert(ids.size() > 0, 'Wrong users ids metadata');
	}
}
