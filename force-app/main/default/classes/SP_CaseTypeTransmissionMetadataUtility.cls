/**
 * @author       Yaneivys Gutierrez
 * @date         03/26/2021
 * @description  Utility Class for Case Type Transmission Custom Metadata Types
 */
public without sharing class SP_CaseTypeTransmissionMetadataUtility {
    public static List<String> getPossibleTypesForTransmission() {
        if (
            !Case_Type_Transmission__mdt.sObjectType.getDescribe()
                .isAccessible() ||
            !Schema.sObjectType.Case_Type_Transmission__mdt.fields.Type__c.isAccessible()
        ) {
            System.debug('Case Type Transmission Metadata Type Access denied.');
            return null;
        }

        List<Case_Type_Transmission__mdt> cttList = [
            SELECT Type__c
            FROM Case_Type_Transmission__mdt
        ];

        if (!cttList.isEmpty()) {
            List<String> types = new List<String>();
            for (Case_Type_Transmission__mdt ctt : cttList) {
                types.add(ctt.Type__c);
            }
            return types;
        }

        return null;
    }
}
