/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   04-02-2021   dmorales   Initial Version
**/
@isTest
private class HACommunityUsersManagementTest {
    @TestSetup
    static void makeData(){
        Id STUDENT_RT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        Id contactId = SP_TestDataFactory.createContact('UserContact', 'UserContactLastName', 'usercontact@test.com');   
        Contact contToUpdate = new Contact();
        contToUpdate.Id = contactId;
        contToUpdate.RecordTypeId = STUDENT_RT;
        update contToUpdate;
    }

    @isTest
    static void testUpdateUserProfile() {

        List<Contact> contactCreated = [SELECT Id, RecordType.Name, RecordTypeId FROM Contact WHERE Email = 'usercontact@test.com'];
        System.debug('Contact RT ' + contactCreated[0].RecordType.Name);
        System.debug('Contact RT Id ' + contactCreated[0].RecordTypeId);
        Id userId = SP_TestDataFactory.createUser(false, contactCreated[0].Id, 'ASU Prep Digital - Parent', 'UserContact', 'UserContactLastName', 'usercontact@test.com', 'usercontact@test.com' ); 
        Test.startTest();
           HACommunityUsersManagement.updateStudentUserProfile(userId);
        Test.stopTest();
        List<User> uList = [SELECT Id, Profile.Name FROM User WHERE Id =: userId];
        System.debug('User Profile ' + uList[0].Profile.Name);
        System.assert(uList.size() > 0, 'No User Profile Updated');
    }
}