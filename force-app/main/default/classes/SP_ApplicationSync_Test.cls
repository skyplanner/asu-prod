/**
 * @File Name          : SP_ApplicationSync_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/4/2020   dmorales     Initial Version
**/
@isTest
private class SP_ApplicationSync_Test {
    @TestSetup
    static void makeData(){
        SP_TestDataFactory.createApplicationWithDocument('Accepted');   
    }
    @isTest
    private static void testUpdateRelatedReady() {
         List<Application__c> appList = [SELECT Id, Student_First_Name__c FROM Application__c
                                         WHERE Student_First_Name__c = 'TestingStudentFirstName'
                                         AND Student_Last_Name__c = 'TestingStudentLastName'];
        
        Test.startTest();
           SP_UpdateRelatedReadySync.handleSyncProcessReady(appList);
        Test.stopTest();

        List<ContentVersion> cvList = [SELECT IsReadyToSync__c, Sent__c 
                                        FROM ContentVersion 
                                        WHERE Title = 'TestDocumentApplication'
                                        AND PathOnClient = 'testDocumentApplication.jpg'];

        System.assert(cvList[0].IsReadyToSync__c);                                
      //  System.assert(!cvList[0].Sent__c);                                
    }
    @isTest
    private static void testUpdateSyncSuccess() {
        List<Application__c> appList = [SELECT Id, Student_First_Name__c FROM Application__c
                                        WHERE Student_First_Name__c = 'TestingStudentFirstName'
                                        AND Student_Last_Name__c = 'TestingStudentLastName'];
       
        Test.startTest();
            SP_UpdateRelatedSyncSuccess.handleSyncProcessSuccess(appList);
        Test.stopTest();

        List<ContentVersion> cvList = [SELECT IsReadyToSync__c, Sent__c 
                                        FROM ContentVersion 
                                        WHERE Title = 'TestDocumentApplication'
                                        AND PathOnClient = 'testDocumentApplication.jpg'];

        System.assert(cvList[0].IsReadyToSync__c);                                
       // System.assert(cvList[0].Sent__c);                                
   }
   @isTest
   private static void testUpdateWhenNotAccepted() {
        List<Application__c> appList = [SELECT Id, Student_First_Name__c FROM Application__c
                                        WHERE Student_First_Name__c = 'TestingStudentFirstName'
                                        AND Student_Last_Name__c = 'TestingStudentLastName'];
   
        Test.startTest();
        SP_UpdateRelatedNotAccepted.handleSyncProcessAppNoAccepted(appList);
        Test.stopTest();

        List<ContentVersion> cvList = [SELECT IsReadyToSync__c, Sent__c 
                                        FROM ContentVersion 
                                        WHERE Title = 'TestDocumentApplication'
                                        AND PathOnClient = 'testDocumentApplication.jpg'];

        System.assert(!cvList[0].IsReadyToSync__c);                                
      //  System.assert(!cvList[0].Sent__c);                                
  }
}