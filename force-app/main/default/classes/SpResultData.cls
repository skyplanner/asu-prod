/*
 * @author				Yaneivys Gutierrez
 * @date				05/04/2022
 * @description			Data Result for LWC Wrap
 * @group
 * @last modified on	05/04/2022
 * @last modified by	05/04/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/04/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpResultData {
	@AuraEnabled
	public String lotteryId { get; set; }
}
