/**
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Utility class
 * group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpUtilities {
	public static Schema.DescribeFieldResult getFieldDescribe(
		String objApiName,
		String fieldApiName
	) {
		SObjectType r = ((SObject) (Type.forName('Schema.' + objApiName)
				.newInstance()))
			.getSObjectType();
		DescribeSObjectResult d = r.getDescribe();
		return d.fields.getMap().get(fieldApiName).getDescribe();
	}

	public static List<Map<String, String>> getPicklistPair(
		String objApiName,
		String fieldApiName
	) {
		List<Map<String, String>> torReturn = new List<Map<String, String>>();

		Schema.DescribeFieldResult describe = getFieldDescribe(
			objApiName,
			fieldApiName
		);

		List<Schema.PicklistEntry> picklistValues = describe.getPicklistValues();
		for (Schema.PicklistEntry ple : picklistValues) {
			torReturn.add(
				new Map<String, String>{
					'label' => ple.getLabel(),
					'value' => ple.getValue()
				}
			);
		}
		return torReturn;
	}

	public static Map<String, List<Map<String, String>>> getPicklistDependency(
		String objApiName,
		String fieldApiName
	) {
		Map<String, List<Map<String, String>>> torReturn = new Map<String, List<Map<String, String>>>();

		Schema.DescribeFieldResult describe = getFieldDescribe(
			objApiName,
			fieldApiName
		);

		if (describe.isDependentPicklist()) {
			Schema.sObjectField controlToken = describe.getController();
			Schema.DescribeFieldResult control = controlToken.getDescribe();
			List<Schema.PicklistEntry> controlEntries;

			if (control.getType() != Schema.DisplayType.Boolean) {
				controlEntries = control.getPicklistValues();
			}

			Integer size = controlEntries != null ? controlEntries.size() : 2;
			String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
			for (Schema.PicklistEntry entry : describe.getPicklistValues()) {
				Object validFor = ((Map<String, Object>) JSON.deserializeUntyped(
						JSON.serialize(entry)
					))
					.get('validFor');

				if (
					entry.isActive() &&
					String.isNotEmpty(String.valueOf(validFor))
				) {
					List<String> base64chars = String.valueOf(validFor)
						.split('');
					for (Integer i = 0; i < size; i++) {
						Object controlValue;
						if (controlEntries == null) {
							controlValue = (Object) (i == 1);
						} else if (controlEntries[i].isActive()) {
							controlValue = (Object) controlEntries[i]
								.getValue();
						}

						Integer bitIndex = i / 6;
						if (bitIndex <= base64chars.size() - 1) {
							Integer bitShift = 5 - Math.mod(i, 6);
							if (
								controlValue == null ||
								(base64map.indexOf(base64chars[bitIndex]) &
								(1 << bitShift)) == 0
							) {
								continue;
							}
							if (!torReturn.containsKey((String) controlValue)) {
								torReturn.put(
									(String) controlValue,
									new List<Map<String, String>>()
								);
							}
							torReturn.get((String) controlValue)
								.add(
									new Map<String, String>{
										'label' => entry.getLabel(),
										'value' => entry.getValue()
									}
								);
						}
					}
				}
			}
		} else {
			List<Schema.PicklistEntry> picklistValues = describe.getPicklistValues();
			for (Schema.PicklistEntry ple : picklistValues) {
				torReturn.put(ple.getValue(), null);
			}
		}
		return torReturn;
	}
}
