/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 03-09-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-09-2021   dmorales   Initial Version
**/
public without sharing class ConvertLeadFromFlow {
            
    @InvocableMethod
    public static List<Id> convertLead(List<Contact> contactData) { 
        Contact currentContact = contactData[0];
        Id idContact = findOrConvertLead(currentContact);
        if(idContact  != null){
             List<Id> listId = new List<Id>();
             listId.add(idContact);
             return listId;
        }
               
        return null;      
    }

    private static Id findOrConvertLead(Contact currentContact){
        String firstName = currentContact.FirstName;
        String lastName =  currentContact.LastName;
        String email =  currentContact.Email;
        Id B2C_LEAD_RT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('B2C').getRecordTypeId();
        List<Lead> leads = [SELECT Id, IsConverted, RecordTypeId 
                            FROM Lead 
                            WHERE Email =: email 
                            AND FirstName =: firstName
                            AND LastName =: lastName
                            AND isConverted = FALSE 
                            AND RecordTypeId =: B2C_LEAD_RT];
                            
        System.debug('Leads ' + leads);
        if(leads.size() >= 1) {
            //Only convert the first Lead found
            return convertLead(leads[0], currentContact.AccountId);
        }
        // else lead.size() == 0
        return null;
    }
    
    private static Id convertLead(Lead ld, Id act){
    
    //	LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true AND MasterLabel = 'Qualified'];
        Database.LeadConvert lc = new Database.LeadConvert();
    
        lc.setLeadId(ld.Id);
        if(act != null) {
            lc.setAccountId(act);
        }
    
        lc.setDoNotCreateOpportunity(True);
       // String masterLabel = ld.RecordTypeId == B2S_LEAD_RT ?  'Qualified' : 'Enrolled';
        String masterLabel = 'Enrolled';
      
        lc.setConvertedStatus(masterLabel);
        Id contactId;
        try{
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            if(lcr.isSuccess()) {
                contactId = lcr.getContactId();		
            }
        }
        catch(Exception ex) {
            //	System.debug('Error converting Lead');
            throw new CommunityManagementCustomException('Error converting Lead');
        }
    
        return contactId;
    }
}