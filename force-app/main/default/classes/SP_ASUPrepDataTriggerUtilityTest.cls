/**
 * @author       Yaneivys Gutierrez
 * @date         03/29/2021
 * @description  Test Class for SP_ASUPrepDataTriggerUtility
 */
@isTest
private class SP_ASUPrepDataTriggerUtilityTest {
    @TestSetup
    static void makeData() {
        SP_TestDataFactory.createDataCaseInsert();
    }

    @isTest
    static void testHandleAfterInsertPendingAnswer() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = date.newInstance(2020, 12, 31),
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1217994635',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }

    @isTest
    static void testHandleAfterInsertCompleteAnswer() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = date.newInstance(2020, 12, 31),
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Dirja']
            .Id,
            ASU_ID__c = '1221229685',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '44945'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }

    @isTest
    static void testHandleAfterInsertPendingAnswerWhithoutEffdt() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = null,
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1217994635',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }

    @isTest
    static void testHandleAfterInsertPendingAnswerWhithNullTransactionId() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = null,
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1217994345',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }

    @isTest
    static void testHandleAfterInsertPendingAnswerWhitInvalidTransactionId() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = null,
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1274564345',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }

    @isTest
    static void testHandleAfterInsertIssueInTransmissionNotSent() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = null,
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1279874545',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );

        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }

    @isTest
    static void testHandleAfterInsertIssueInTransmissionSent() {
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        Profile p = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Standard User'
            LIMIT 1
        ];

        User u = new User(
            FirstName = 'TestUser',
            LastName = 'Owner',
            Username = 'testASUPrepData@salesforce.test',
            Alias = 'test',
            ProfileId = p.Id,
            Email = 'testASUPrepData@salesforce.test',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles'
        );
        insert u;

        // Testing Case insertion with known type for transmission
        Case c1 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = null,
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1279874545',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = '',
            OwnerId = u.Id
        );

        insert c1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }
}
