/*
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Lottery Result Wrap
 * @group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryResultWrap {
	@AuraEnabled
	public List<Lottery__c> activeList { get; set; }

	@AuraEnabled
	public List<Lottery__c> inactiveList { get; set; }
}
