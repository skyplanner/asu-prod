/**
 * @File Name          : SP_ContentDocumentLinkTrigger_Utility.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-22-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/8/2020   dmorales     Initial Version
**/
public without sharing class SP_ContentDocumentLinkTrigger_Utility {
    public static final Integer DEFAULT_SIZE = 20000000;
    public static final String  DEFAULT_REGEX = '[^A-z\\s\\d-()]?[\\\\\\^`]?';

    public static void handleContentDocumentSync(List<ContentDocumentLink> cdlList) {

        Map<Id,Id> entityContentMap = new Map<Id,Id>();
        for(ContentDocumentLink cdLinkObj : cdlList){
            entityContentMap.put(cdLinkObj.LinkedEntityId, cdLinkObj.ContentDocumentId);
        }

        Map<ID, Application__c> appMap = new Map<ID, Application__c>([SELECT Id, Application_Status__c, Sync_Status__c
                                                                     FROM Application__c
                                                                     WHERE Id IN :entityContentMap.keySet()]);    
        
                                                                        
                    
        List<Id> contentDocumentIdsList = new List<Id>();                                                                     
        for(Id appObjId : appMap.keySet()){
            Application__c app = appMap.get(appObjId);
            if(app.Application_Status__c == 'Accepted'){          
                contentDocumentIdsList.add(entityContentMap.get(appObjId));
            }
        } 
        
        List<ContentVersion> cvList = [SELECT Id, IsReadyToSync__c, Sent__c, Title, ContentSize, ContentDocumentId  
                                       FROM ContentVersion 
                                       WHERE ContentDocumentId IN: contentDocumentIdsList];

        for(ContentVersion cvObj : cvList){
           cvObj.IsReadyToSync__c = TRUE; 
           cvObj.Sent__c = FALSE; 
        }
          
        update cvList;      
     
    } 

    public static void handleApplicationDocumentsRestrictions(List<ContentDocumentLink> cdlList) {

        Map<Id,Id> entityContentMap = new Map<Id,Id>();
        Map<Id,ContentDocumentLink> contentContentMap = new Map<Id,ContentDocumentLink>();
        for(ContentDocumentLink cdLinkObj : cdlList){
            entityContentMap.put(cdLinkObj.LinkedEntityId, cdLinkObj.ContentDocumentId);
            contentContentMap.put(cdLinkObj.ContentDocumentId, cdLinkObj );
        }

        Map<ID, Application__c> appMap = new Map<ID, Application__c>([SELECT Id, Application_Status__c, Sync_Status__c
                                                                     FROM Application__c
                                                                     WHERE Id IN :entityContentMap.keySet()]);    
        
                                                                        
        //To check only ContentDocument that are related with Application records            
        List<Id> contentDocumentIdsList = new List<Id>();                                                                     
        for(Id appObjId : appMap.keySet()){
             contentDocumentIdsList.add(entityContentMap.get(appObjId));
        }                                                                     
       
        
        List<ContentVersion> cvList = [SELECT Id, IsReadyToSync__c, Sent__c, Title, ContentSize, ContentDocumentId  
                                       FROM ContentVersion 
                                       WHERE ContentDocumentId IN: contentDocumentIdsList];

                                               
        Files_Validation_Setting__c fileSett = Files_Validation_Setting__c.getOrgDefaults();
        Integer defaultContentSize = (fileSett != null && fileSett.Content_Size_Limit__c != null) ?
                                      Integer.valueOf(fileSett.Content_Size_Limit__c) : DEFAULT_SIZE;
        String regexPattern = (fileSett != null && String.ISNOTBLANK(fileSett.Regex_Pattern__c)) ?
                                      fileSett.Regex_Pattern__c : DEFAULT_REGEX;                                        
        String profilesExcluded =  (fileSett != null && String.ISNOTBLANK(fileSett.Profiles_Id_Excluded__c)) ?
                                    fileSett.Profiles_Id_Excluded__c : '';     
        Boolean checkTitle = true;
        if(String.ISNOTBLANK(profilesExcluded)){
            String profileId = UserInfo.getProfileId();
            System.debug('Current Profile ' +  profileId);
            System.debug('profilesExcluded ' +  profilesExcluded);
            System.debug('!profilesExcluded.contains(profileId) ' +  !profilesExcluded.contains(profileId));
            checkTitle = !profilesExcluded.contains(profileId);
        }

        List<ContentVersion> toUpdate = new List<ContentVersion>();

        for(ContentVersion cvObj : cvList){        
           if(checkTitle){
               String title = cvObj.Title;
               System.debug('Title Old ' + cvObj.Title);
               System.debug('regex  ' + regexPattern);
               cvObj.Title = title.replaceAll(regexPattern,'');              
               System.debug('Title New ' + cvObj.Title);
               toUpdate.add(cvObj);      
           }
           if(cvObj.ContentSize > defaultContentSize){
                ContentDocumentLink cdl = contentContentMap.get(cvObj.ContentDocumentId);
                cdl.addError('You have exceed the max size allowed');
           }
          
        }
       
        update toUpdate;      
     
    } 
  
}