/**
 * @author				Yaneivys Gutierrez
 * @date				04/28/2022
 * @description			Manager for Threw Warning In Student Application Component
 * group
 * @last modified on	04/28/2022
 * @last modified by	04/28/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		04/28/2022		Yaneivys Gutierrez		Initial Version
 */
public with sharing class SpThrewWarningInAppCtrl {
	@AuraEnabled
	public static String loadRecord(String recordId) {
		if (String.isNotBlank(recordId)) {
			List<Application__c> apps = [
				SELECT Id, Threw_Warnning_Message__c
				FROM Application__c
				WHERE Id = :recordId
			];
			if (apps.size() > 0) {
				Application__c app = apps[0];
				if (app.Threw_Warnning_Message__c) {
					app.Threw_Warnning_Message__c = false;
                    update app;
					return 'pre-dig-app';
				}
			}
		}
		return '';
	}
}
