/**
 * @author       Yaneivys Gutierrez
 * @date         03/18/2021
 * @description  Utility Class for Case Trigger
 */
public with sharing class SP_CaseTriggerUtility {
    public static void handleAfterInsertUpdate(
        List<Case> newList,
        Map<Id, Case> oldMap
    ) {
        List<ASU_Prep_Data__c> asupdList = new List<ASU_Prep_Data__c>();

        List<Case> caseList = new List<Case>(
            [
                SELECT
                    Id,
                    toLabel(Type),
                    Status,
                    Country_of_Citizenship__c,
                    Effective_Date__c,
                    ContactId,
                    Contact.Name,
                    ASU_ID__c,
                    Term__r.Code__c,
                    Course_Offering_Req__r.Name,
                    Related_Class_1__c,
                    Related_Class_2__c
                FROM Case
                WHERE Id IN :newList
            ]
        );

        for (Case c : caseList) {
            // oldMap is null => Case inserted
            String oldValue = oldMap?.get(c.Id)?.Status;
            if (
                c.Status == 'New' && (oldValue == null || c.Status != oldValue)
            ) {
                List<String> types = SP_CaseTypeTransmissionMetadataUtility.getPossibleTypesForTransmission();
                if (types != null && !types.contains(c.Type)) {
                    System.debug(
                        'Transmission not sent: Type info not recognizable for transmission.'
                    );
                    return;
                }

                String citizenship = '';
                Country_Code__mdt countryCode = SP_CountryCodeMetadataUtility.getCodeByCountry(
                    c.Country_of_Citizenship__c
                );
                if (countryCode != null) {
                    citizenship = countryCode.ALPHA_3__c;
                }

                asupdList.add(
                    new ASU_Prep_Data__c(
                        Case__c = c.Id,
                        Type__c = c.Type,
                        Status__c = 'N',
                        Contact_Name__c = c.Contact.Name,
                        ASU_ID__c = c.ASU_ID__c,
                        Citizenship__c = citizenship,
                        Term_Code__c = c.Term__r.Code__c,
                        Class__c = c.Course_Offering_Req__r.Name,
                        Related_Class_1__c = c.Related_Class_1__c,
                        Related_Class_2__c = c.Related_Class_2__c,
                        Effective_Date__c = c.Effective_Date__c
                    )
                );
            }
        }

        if (!ASU_Prep_Data__c.sObjectType.getDescribe().isCreateable()) {
            System.debug('ASU Prep Data sObject Creation Access denied.');
            return;
        }

        if (asupdList.size() > 0) {
            insert asupdList;
        }
    }
}
