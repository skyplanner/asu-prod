/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 07-07-2021
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   01-21-2021   dmorales   Initial Version
 **/
public without sharing class CustomCommunitySelfRegistrationCtrl {

public CustomCommunitySelfRegistrationCtrl() {
	String expid = ApexPages.currentPage().getParameters().get('expid');
	if (expId != null) {
		Site.setExperienceId(expId);
	}
}

@RemoteAction
public static UserCreationResult registerUser(HACommunityUsersManagement.CommunityUser newUser) {

	String userId;
	String userName = newUser.email;
	UserCreationResult result;

	Savepoint sp = Database.setSavepoint(); 
	HACommunityUsersManagement manager = new HACommunityUsersManagement(newUser);	   	
    
	try{
	 
		Account newAccount = manager.handleEntitiesCreation();
		System.debug('Account Id returned ' + newAccount);
		      
		User u = new User();
		u.Username = userName;
		u.Email = newUser.email;
		u.FirstName = newUser.firstName;
		u.LastName = newUser.lastName;
		String timestamp = String.valueOf(Datetime.now().formatGMT('yyyyMMddHHmmss'));
		Integer nicknameLength = newUser.firstName.length() + timestamp.length();
		String nickname = newUser.firstName + timestamp;
		//check nickname lenght to accomplish with the max length permitted for the field (40)
		if(nicknameLength > 40 ){
			Integer endIndex = newUser.firstName.length() - (nicknameLength - 40);
			nickname = newUser.firstName.substring(0, endIndex) + timestamp;
		}
		u.CommunityNickname = nickname;		

		userId = Site.createExternalUser(u, newAccount.Id, newUser.password);
		
		//Create parent contact after user creation when user.email = user.parentEmail and user is Student under 18
		if(newUser.email == newUser.parentEmail && newUser.typeUser == 'student' && newUser.userAge < 18){
			manager.createContactAndAssociateSA(newAccount, newUser.parentEmail, false);
		}

	/*	if(newUser.typeUser == 'student'){			
			updateUserStudentProfile(userId);
		}*/
		 

	}
	catch(CommunityManagementCustomException customExep) {
		result = new UserCreationResult(null, 'CommunityManagementCustomException: ' +customExep.getMessage());	
		System.debug('CommunityManagementCustomException ' + customExep.getMessage());
	}
	catch(Site.ExternalUserCreateException siteExep) {
		String message = String.join(siteExep.getDisplayMessages(), ', ');
		result = new UserCreationResult(null, 'ExternalUserCreateException: ' + message );	
		System.debug('ExternalUserCreateException ' + siteExep.getMessage());
	}
	catch(Exception ex1) {
		result = new UserCreationResult(null, 'GlobalException: ' + ex1.getMessage());		
		System.debug('Global Exception' + ex1.getMessage());		
		System.debug('Global Exception' + ex1.getStackTraceString());		
	}
	finally {
		//If any exception was thrown then return DB to a save place.
		if(result != null){
			  Database.rollback(sp);
		}	    
	}


	if (userId != null || Test.isRunningTest() ) {
		/**
		 * Allows users to log in to the current site with the given username and password,
		 * then takes them to the startUrl. If startUrl is not a relative path,
		 * it defaults to the site's designated index page.
		 */
	/*	String startUrl = '/asuprepacademy/s/';

		if(newUser.typeUser != 'student'){			
			PageReference pageToReturn =  Site.login(userName, newUser.password, startUrl);
			if(pageToReturn == null) {
				result = new UserCreationResult(null, 'LoginException ');
			}
			else{
				result = new UserCreationResult(pageToReturn, '');
			} 
		}
		else{
		    PageReference pageLogin = new PageReference(Site.getPathPrefix());
			result = new UserCreationResult(pageLogin, '');
		}  */  
		
		if(newUser.typeUser == 'student'){			
			HACommunityUsersManagement.updateStudentUserProfile(userId);
		}	
		
		PageReference pageLogin = new PageReference(Site.getPathPrefix());
		if(result == null){
		   result = new UserCreationResult(pageLogin, ''); 	
		}		  
	}

	return result;
}

@RemoteAction
public static PicklistObjects getPicklistObjects(){
	 PicklistObjects plObjs = new PicklistObjects();
	 plObjs.countryPicklist = getPicklistLabels('Application__c', 'Student_Country_of_Citizenship__c');
	 plObjs.countryCodePicklist = getPicklistLabels('Contact', 'Phone_Country_Code__c');
	 plObjs.statePicklist = getPicklistLabels('Application__c', 'Student_State__c');
     return plObjs;
}

private static Map<String, String> getPicklistLabels(String pObjectApiName, String pFieldName){
	List<String> lstPickvals = new List<String>();
	Schema.SObjectType targetType = Schema.getGlobalDescribe().get(pObjectApiName);
	Sobject objectApiName = targetType.newSObject();
	Schema.sObjectType sobjectType = objectApiName.getSObjectType();
	Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
	Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap();
	List<Schema.PicklistEntry> pickListValues = fieldMap.get(pFieldName).getDescribe().getPickListValues();

	Map<String, String> pairValueLabel = new Map<String, String>();

	for (Schema.PicklistEntry a : pickListValues) {
		pairValueLabel.put(a.getValue(), a.getLabel());
	}
	return pairValueLabel;
}


    /****INNER CLASSES ******/
	public class UserCreationResult {
		@AuraEnabled
		public String errorMessage {set; get;}
		@AuraEnabled
		public PageReference returnUrl {set; get;}

		public UserCreationResult(PageReference page, String error){
			errorMessage = error;
			returnUrl = page;
		}
	}

	public class PicklistObjects {
		@AuraEnabled
		public Map<String, String> countryPicklist {set; get;}
		@AuraEnabled
		public Map<String, String> countryCodePicklist {set; get;}
		@AuraEnabled
		public Map<String, String> statePicklist {set; get;}

	}
}