/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpApplicationManager
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpApplicationManagerTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void getWLAppsTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		List<Id> appIds = new List<Id>(
			(new Map<Id, Application__c>([SELECT Id FROM Application__c]))
				.keySet()
		);

		Test.startTest();

		List<Application__c> apps = SpApplicationManager.getWLApps(lId, appIds);

		Test.stopTest();

		System.assertEquals(
			1,
			apps.size(),
			'Wrong Waiting List Application Number'
		);
	}

	@isTest
	static void getWLTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		List<Application__c> apps = SpApplicationManager.getWLApps(lId);

		Test.stopTest();

		System.assertEquals(
			1,
			apps.size(),
			'Wrong Waiting List Application Number'
		);
	}

	@isTest
	static void getPosiblesWLByLotTest() {
		Id appId = SP_TestDataFactory.createPossibleApplicationForLottery();
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		List<Application__c> apps = SpApplicationManager.getPosiblesWLByLot(
			[
				SELECT
					Id,
					Grade_Applying_For__c,
					Site_of_Interest__c,
					Term_Applying_for__c,
					Year_applying_for__c
				FROM Lottery__c
			]
		);

		Test.stopTest();

		System.assertEquals(
			2,
			apps.size(),
			'Wrong Possible Applications For Lottery'
		);
	}

	@isTest
	static void getSelAppsByLotTest() {
		Set<Id> lIds = (new Map<Id, Lottery__c>([SELECT Id FROM Lottery__c]))
			.keySet();

		Test.startTest();

		Map<Id, List<Application__c>> apps = SpApplicationManager.getSelAppsByLot(
			lIds
		);

		Test.stopTest();

		Id lId = (new List<Id>(lIds))[0];
		List<Application__c> appList = apps.get(lId);

		System.assertEquals(
			1,
			appList.size(),
			'Wrong Selected Application Number'
		);
	}

	@isTest
	static void getSelectedAppsTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		List<Application__c> apps = SpApplicationManager.getSelectedApps(lId);

		Test.stopTest();

		System.assertEquals(
			1,
			apps.size(),
			'Wrong Selected Application Number'
		);
	}

	@isTest
	static void getAppsBySetupTest() {
		Id appId = SP_TestDataFactory.createPossibleApplicationForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP POLYTECHNIC';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 5;

		Test.startTest();

		List<Application__c> apps = SpApplicationManager.getAppsBySetup(setup);

		Test.stopTest();

		System.assertEquals(
			1,
			apps.size(),
			'Wrong Applications Number By Setup'
		);
		System.assertEquals(appId, apps[0].Id, 'Wrong Applications By Setup');
	}

	@isTest
	static void getLotteryDataTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		SpLotteryViewWrap result = SpApplicationManager.getLotteryData(lId);

		Test.stopTest();

		System.assertEquals(lId, result.lottery.Id, 'Wrong Lottery result');
		System.assertEquals(
			1,
			result.selectedApps.size(),
			'Wrong Selected Application Number'
		);
		System.assertEquals(
			1,
			result.waitingList.size(),
			'Wrong Waiting List Application Number'
		);
	}
}
