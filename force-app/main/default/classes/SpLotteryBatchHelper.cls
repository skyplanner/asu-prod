/*
 * @author				Yaneivys Gutierrez
 * @date				05/05/2022
 * @description			Batch Helper for Lottery Process: Update and Reorder Waiting List
 * @group
 * @last modified on	05/05/2022
 * @last modified by	05/05/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/05/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryBatchHelper {
	public static void processLotteries(List<Lottery__c> lotteries) {
		List<Application__c> apps = SpApplicationManager.getPosiblesWLByLot(
			lotteries
		);

		List<Application__c> toUpdate = new List<Application__c>();

		if (apps.size() > 0) {
			Map<Id, List<Application__c>> appsByLotId = new Map<Id, List<Application__c>>();
			for (Application__c app : apps) {
				Id lId = app.Lottery__c != null
					? app.Lottery__c
					: getLotIdForApp(app, lotteries);

				List<Application__c> lApp = appsByLotId.get(lId) != null
					? appsByLotId.get(lId)
					: new List<Application__c>();

				SpLotteryProcessManager.updateAppWL(app, lId, lApp.size() + 1);
				toUpdate.add(app);

				lApp.add(app);
				appsByLotId.put(lId, lApp);
			}
		}

		if (toUpdate.size() > 0) {
			update toUpdate;
		}
	}

	private static Id getLotIdForApp(
		Application__c app,
		List<Lottery__c> lotteries
	) {
		Id lId;
		for (Lottery__c l : lotteries) {
			if (appApply(app, l)) {
				lId = l.Id;
				break;
			}
		}

		return lId;
	}

	private static Boolean appApply(Application__c app, Lottery__c l) {
		return app.Grade_Applying_For__c == l.Grade_Applying_For__c &&
			app.Site_of_Interest__c == l.Site_of_Interest__c &&
			app.Term_Applying_for__c == l.Term_Applying_for__c &&
			app.Year_applying_for__c == l.Year_applying_for__c;
	}
}
