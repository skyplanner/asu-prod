/**
 * @File Name          : SP_UpdateRelatedNotAccepted.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 10-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/3/2020   dmorales     Initial Version
**/
public without sharing class SP_UpdateRelatedNotAccepted {

    @InvocableMethod(label='Reset Content Versions'
    description='Reset Content Versions related to applications when their are change to distint Status than Accepted')
    public static void handleSyncProcessAppNoAccepted(List<Application__c> appList) {

        SP_ApplicationsSyncManagement.handleUpdateRelatedContent(appList, false, false, false);
        
    }
}