/**
 * @File Name          : SP_StudentApplicationTrigger_Utillity.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 6/11/2020, 3:08:01 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
 **/
public without sharing class SP_StudentApplicationTrigger_Utility {
	public static void handleRelatedDocument(Set<Id> appList) {
		List<ContentDocumentLink> cdLinkList = [
			SELECT ContentDocumentId, Id
			FROM ContentDocumentLink
			WHERE LinkedEntityId IN :appList
		];

		List<Id> contentDocumentIds = new List<Id>();
		for (ContentDocumentLink cdlObj : cdLinkList) {
			contentDocumentIds.add(cdlObj.ContentDocumentId);
		}

		List<ContentVersion> cvList = [
			SELECT Id, IsReadyToSync__c, Sent__c, ContentDocumentId
			FROM ContentVersion
			WHERE ContentDocumentId IN :contentDocumentIds
		];

		for (ContentVersion cvObj : cvList) {
			cvObj.IsReadyToSync__c = false;
			cvObj.Sent__c = false;
		}

		update cvList;
	}

	public static void handleBeforeInsertUpdate(
		List<Application__c> appsNew,
		Map<Id, Application__c> appOldMap
	) {
		HA_Management_Settings__c sett = HA_Management_Settings__c.getOrgDefaults();
		List<String> cleanLotteryStatusses = SpLotteryCleanSetupMDT.getCleanStatusses();

		Set<Id> lotIds = new Set<Id>();

		for (Application__c app : appsNew) {
			manageSiteOfInterestChange(app, appOldMap, sett);
			Id lId = manageLotteryFieldsUpdate(app, appOldMap);
			if (lId != null) {
				lotIds.add(lId);
			}
			lId = manageStatusChange(app, appOldMap, cleanLotteryStatusses);
			if (lId != null) {
				lotIds.add(lId);
			}
			app.School_Number__c = manageSchoolNoUpdate(app, appOldMap);
		}

		if (lotIds.size() > 0) {
			reCalculateSelectionNo(lotIds);
		}
	}

	private static void manageSiteOfInterestChange(
		Application__c app,
		Map<Id, Application__c> appOldMap,
		HA_Management_Settings__c sett
	) {
		if (String.isNotBlank(app.Site_of_Interest__c)) {
			String oldSite = appOldMap?.get(app.Id)?.Site_of_Interest__c;

			if (appOldMap == null || app.Site_of_Interest__c != oldSite) {
				if (
					app.Site_of_Interest__c == SpApplicationConst.PREP_DIG_SITE
				) {
					app.Application_Program__c = SpApplicationConst.PREP_DIG;
					app.Application_Form_Type__c = null;
					app.Student_Classification__c = null;
					app.RecordTypeId = sett.AZ_Default_RT__c;
					if (
						appOldMap != null &&
						app.Site_of_Interest__c != oldSite
					) {
						app.Threw_Warnning_Message__c = true;
					}
				} else {
					// Immersion app
					app.Application_Program__c = SpApplicationConst.IMMERSION;
					app.Application_Form_Type__c = SpApplicationConst.FULL_TIME;
					app.Student_Classification__c = SpApplicationConst.IMMERSION_FT;
					app.RecordTypeId = sett.Immersion_RT__c;
				}
			}
		}
	}

	private static void cleanLotFields(Application__c app) {
		app.Lottery__c = null;
		app.Lottery_Status__c = SpApplicationConst.LOT_AVAILABLE;
		app.Lottery_Selection_Date__c = null;
		app.Lottery_Selection_Type__c = null;
		app.Lottery_Selection_Number__c = null;
		app.Lottery_WL_No__c = null;
	}

	private static Id manageLotteryFieldsUpdate(
		Application__c app,
		Map<Id, Application__c> appOldMap
	) {
		String oldSite = appOldMap?.get(app.Id)?.Site_of_Interest__c;
		String oldGrade = appOldMap?.get(app.Id)?.Grade_Applying_For__c;
		String oldTerm = appOldMap?.get(app.Id)?.Term_Applying_for__c;
		String oldYear = appOldMap?.get(app.Id)?.Year_applying_for__c;

		if (
			appOldMap != null &&
			(app.Site_of_Interest__c != oldSite ||
			app.Grade_Applying_For__c != oldGrade ||
			app.Term_Applying_for__c != oldTerm ||
			app.Year_applying_for__c != oldYear)
		) {
			Id lId = app.Lottery__c;
			String stateBefore = app.Lottery_Status__c;

			cleanLotFields(app);

			if (lId != null && stateBefore == SpApplicationConst.LOT_SELECTED) {
				return lId;
			}
		}
		return null;
	}

	private static Id manageStatusChange(
		Application__c app,
		Map<Id, Application__c> appOldMap,
		List<String> cleanLotteryStatusses
	) {
		Id lId;
		String oldStatus = appOldMap?.get(app.Id)?.Application_Status__c;
		if (
			(appOldMap == null || app.Application_Status__c != oldStatus) &&
			cleanLotteryStatusses.contains(app.Application_Status__c)
		) {
			if (app.Lottery_Status__c == SpApplicationConst.LOT_SELECTED) {
				lId = app.Lottery__c;
			}
			app.Lottery_Status__c = SpApplicationConst.LOT_WITHDREW;
			app.Lottery_Selection_Number__c = null;
			app.Lottery_WL_No__c = null;
		}
		return lId;
	}

	private static String manageSchoolNoUpdate(
		Application__c app,
		Map<Id, Application__c> appOldMap
	) {
		String oldSite = appOldMap?.get(app.Id)?.Site_of_Interest__c;
		String oldGrade = appOldMap?.get(app.Id)?.Grade_Applying_For__c;

		if (
			appOldMap == null ||
			(appOldMap != null &&
			(app.Grade_Applying_For__c != oldGrade ||
			app.Site_of_Interest__c != oldSite))
		) {
			return SpSchoolNumberMDT.getInstance()
				.getSettings(
					app.Grade_Applying_For__c,
					app.Site_of_Interest__c
				);
		}
		return app.School_Number__c;
	}

	@future
	private static void reCalculateSelectionNo(Set<Id> lotIds) {
		SpLotteryProcessManager.reCalculateSelectionNo(lotIds);
	}
}
