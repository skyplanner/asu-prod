/**
 * @File Name          : SP_StudentApplicationTrigger_Test.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : Yaneivys GM
 * @Last Modified On   : 05/11/2022, 10:48:35 AM
 * @Modification Log   :
 * Ver		Date		Author			Modification
 * 1.0		6/11/2020	dmorales		Initial Version
 * 1.0		05/11/2022	Yaneivys GM		Adding Lottery Fuctionality
 **/
@IsTest
private class SP_StudentApplicationTrigger_Test {
	@TestSetup
	static void makeData() {
		SP_TestDataFactory.createApplicationWithDocument('Accepted');
	}

	@isTest
	static void relatedDocumentTest() {
		List<ContentVersion> cvListBefore = [
			SELECT IsReadyToSync__c, Sent__c
			FROM ContentVersion
			WHERE
				Title = 'TestDocumentApplication'
				AND PathOnClient = 'testDocumentApplication.jpg'
		];

		System.assert(cvListBefore[0].IsReadyToSync__c);
		System.assert(!cvListBefore[0].Sent__c);

		List<Application__c> appList = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			WHERE
				Student_First_Name__c = 'TestingStudentFirstName'
				AND Student_Last_Name__c = 'TestingStudentLastName'
		];

		Test.startTest();
		delete appList;
		Test.stopTest();

		List<ContentVersion> cvListAfter = [
			SELECT IsReadyToSync__c, Sent__c
			FROM ContentVersion
			WHERE
				Title = 'TestDocumentApplication'
				AND PathOnClient = 'testDocumentApplication.jpg'
		];

		System.assert(!cvListAfter[0].IsReadyToSync__c);
		System.assert(!cvListAfter[0].Sent__c);
	}

	@isTest
	static void manageProgramChangeToImmersionTest() {
		Application__c app = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			LIMIT 1
		];

		app.Grade_Applying_For__c = '09';
		app.Site_of_Interest__c = 'ASU PREP POLYTECHNIC';

		Test.startTest();

		update app;

		Test.stopTest();

		app = [SELECT Id, Application_Program__c FROM Application__c LIMIT 1];

		System.assertEquals(
			SpApplicationConst.IMMERSION,
			app.Application_Program__c,
			'Wrong Application Program Assigned'
		);
	}

	@isTest
	static void manageProgramChangeToPrepDigTest() {
		Application__c app = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			LIMIT 1
		];

		app.Grade_Applying_For__c = '09';
		app.Site_of_Interest__c = 'ASU PREP DIGITAL';

		Test.startTest();

		update app;

		Test.stopTest();

		app = [SELECT Id, Application_Program__c FROM Application__c LIMIT 1];

		System.assertEquals(
			SpApplicationConst.PREP_DIG,
			app.Application_Program__c,
			'Wrong Application Program Assigned'
		);
	}

	@isTest
	static void manageStatusChangeTest() {
		Application__c app = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			LIMIT 1
		];

		Lottery__c l = new Lottery__c();
		l.Seats__c = 2;
		l.Available_Seats__c = 1;
		insert l;

		app.Lottery__c = l.Id;
		app.Lottery_Selection_Number__c = 1;
		app.Lottery_Status__c = SpApplicationConst.LOT_SELECTED;
		app.Application_Status__c = 'Withdrew';

		Test.startTest();

		update app;

		Test.stopTest();

		app = [SELECT Id, Lottery_Status__c FROM Application__c LIMIT 1];

		System.assertEquals(
			SpApplicationConst.LOT_WITHDREW,
			app.Lottery_Status__c,
			'Wrong Lottery Status Assigned'
		);
	}

	@isTest
	static void manageCleanLotteryInfoTest() {
		Application__c app = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			LIMIT 1
		];

		Lottery__c l = new Lottery__c();
		l.Seats__c = 2;
		l.Available_Seats__c = 1;
		insert l;

		app.Lottery__c = l.Id;
		app.Lottery_Selection_Number__c = 1;
		app.Lottery_Status__c = SpApplicationConst.LOT_SELECTED;
		app.Grade_Applying_For__c = '09';
		app.Site_of_Interest__c = 'ASU PREP POLYTECHNIC';

		Test.startTest();

		update app;

		Test.stopTest();

		app = [
			SELECT Id, Lottery_Status__c, Lottery_Selection_Date__c
			FROM Application__c
			LIMIT 1
		];

		System.assertEquals(
			SpApplicationConst.LOT_AVAILABLE,
			app.Lottery_Status__c,
			'Wrong Lottery Status Assigned'
		);
	}
}
