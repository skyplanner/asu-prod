/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 05-27-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-10-2021   dmorales   Initial Version
**/
public without sharing class ApplicationAssignmentByHA {
    @InvocableMethod(label='Assign Application to AA of the same HA')
	public static List<Id> assignByHA(List<HouseholdAccountInfo> haInfoList) {

        Set<String> groupNameSet = new Set<String>();
        Set<Id> applicationIdSet = new Set<Id>();

        for(HouseholdAccountInfo haI : haInfoList){
            groupNameSet.add(haI.groupName);
            applicationIdSet.add((Id)haI.recordId);
        }

        List<GroupMember> groupUsers = [SELECT Group.Name, GroupId, UserOrGroupId 
                                        FROM GroupMember 
                                        WHERE Group.Name IN :groupNameSet];
        
        Map<String, List<GroupMember>> mapGroupMembers = new Map<String, List<GroupMember>>();
        //build a map with each GroupName and its groupMembers (to accomplish with possibles bulk DML)
        for(GroupMember member : groupUsers){
            List<GroupMember> memberList = new List<GroupMember>();
            if(mapGroupMembers.containsKey(member.Group.Name)){
                memberList = mapGroupMembers.get(member.Group.Name);
                memberList.add(member);
                mapGroupMembers.put(member.Group.Name, memberList);
            }else{
                memberList.add(member);
                mapGroupMembers.put(member.Group.Name, memberList);
            }
        }
        //build a map with each Application (to accomplish with possibles bulk DML)
        Map<ID, Application__c> applicationMap = new Map<ID, Application__c>([SELECT Id, OwnerId, Contact__r.AccountId 
                                                                              FROM Application__c 
                                                                              WHERE Id IN :applicationIdSet 
                                                                              AND Contact__c != '']);
        /*if no applicationMap found that means that no application with Contact exist so all of them
         * must be assign by Round Robin*/
        if(applicationMap == null || applicationMap.isEmpty()){
            return new List<Id>(applicationIdSet);
        }          

        /*extract different accounts associated with application*/
        Set<Id> appAccountsId = new  Set<Id>();
        for(Application__c appInMap : applicationMap.values() ){
            appAccountsId.add(appInMap.Contact__r.AccountId);
        }

        
        //build a map for each Account with Applications
        Map<ID, List<Application__c>> applicationAccountMap = new Map<ID, List<Application__c>>();
        List<Application__c> applicationsWithAccount = [SELECT Id, OwnerId, Contact__r.AccountId 
                                                        FROM Application__c 
                                                        WHERE Contact__r.AccountId =: appAccountsId];

        for(Application__c appInAcc : applicationsWithAccount) {
            List<Application__c> appMembers = new List<Application__c>();
            if(applicationAccountMap.containsKey(appInAcc.Contact__r.AccountId)){
                appMembers = applicationAccountMap.get(appInAcc.Contact__r.AccountId);
                appMembers.add(appInAcc);
                applicationAccountMap.put(appInAcc.Contact__r.AccountId, appMembers);
            }else{
                appMembers.add(appInAcc);
                applicationAccountMap.put(appInAcc.Contact__r.AccountId, appMembers);
            }
        }                                               

        List<Application__c> appToUpdate = new List<Application__c>();

        List<Id> idForRoundRobin = new List<Id>();
        for(HouseholdAccountInfo haI : haInfoList){
              List<GroupMember> membersList = mapGroupMembers.get(haI.groupName);         
              Application__c app = applicationMap.get(haI.recordId); 
              /** If app not found in Map, means that the app 
               *  does not have any Contact associated so must be assigned by RR */
              if(app == null){
                idForRoundRobin.add(haI.recordId);
                continue;
              }

              List<Application__c> existinAppUnderHA = applicationAccountMap.get(app.Contact__r.AccountId);

              Boolean alreadyAssignOwner = false;    

              if(existinAppUnderHA.size() > 0){             
                Application__c  appWithOwner = appWithOwnerInGroup(existinAppUnderHA, membersList );
                if(appWithOwner != null){
                    app.OwnerId = appWithOwner.OwnerId;
                    appToUpdate.add(app);
                    alreadyAssignOwner = true; 
                }
              }
               
              if(!alreadyAssignOwner){
                  Boolean ownerAdvisorExist = ownerExistInGroup(membersList, app.OwnerId);
                  if(!ownerAdvisorExist){
                    idForRoundRobin.add(app.Id);
                  }
              }             
         
        } 
        
        update appToUpdate;
        return (idForRoundRobin.size() > 0 ) ? idForRoundRobin : null;
        
    }

    private static Application__c appWithOwnerInGroup(List<Application__c> existinAppUnderHA, List<GroupMember> membersList){
        Boolean exist = false;
        Integer i = 0;
        Application__c appToReturn = new Application__c();
        while(!exist && i < existinAppUnderHA.size()){
            Boolean ownerAdvisorExist = ownerExistInGroup(membersList, existinAppUnderHA[i].OwnerId);
            if(ownerAdvisorExist){
               exist = true;
               appToReturn  = existinAppUnderHA[i];
            }
            i++;
        }
     
        return (exist)? appToReturn : null;
    }

    private static Boolean ownerExistInGroup(List<GroupMember> memberList, Id OwnerId){
        Boolean exist = false;
        Integer i = 0;
        while(!exist && i < memberList.size()){
            if(memberList[i].UserOrGroupId == OwnerId ){
               exist = true;
            }
            i++;
        }

        return exist;
    }

    public class HouseholdAccountInfo {
		@InvocableVariable(required=true label='group name')
		public string groupName;

        @InvocableVariable(required=true label='recordId')
		public string recordId;
        /** Constructor for Invocable Action**/
        public HouseholdAccountInfo(){          
        }
        /** Constructor for TestClasses */
        public HouseholdAccountInfo(String pGroupName, String pRecordId){
            groupName = pGroupName;
            recordId = pRecordId;
        }
	}
}