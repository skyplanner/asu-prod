/*
 * @author				Yaneivys Gutierrez
 * @date				05/05/2022
 * @description			Batch for Lottery Process: Update and Reorder Waiting List
 * @group
 * @last modified on	05/05/2022
 * @last modified by	05/05/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/05/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryBatch implements Database.Batchable<sObject> {
	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(
			'SELECT ' +
			'   Id, Grade_Applying_For__c, Site_of_Interest__c, ' +
			'   Term_Applying_for__c, Year_applying_for__c, ' +
			'   Available_Seats__c, Seats__c, ' +
			'   Active__c, Run_Date__c ' +
			'FROM Lottery__c ' +
			'WHERE Active__c = TRUE'
		);
	}

	public void execute(Database.BatchableContext bc, List<sObject> records) {
		SpLotteryBatchHelper.processLotteries(records);
	}

	public void finish(Database.BatchableContext bc) {
		AsyncApexJob a = [
			SELECT
				Id,
				Status,
				NumberOfErrors,
				JobItemsProcessed,
				TotalJobItems,
				CreatedBy.Email
			FROM AsyncApexJob
			WHERE Id = :bc.getJobId()
		];
		System.debug(
			String.format(
				'The batch Apex Job "{0}" processed: {1} batches with {2} failures.',
				new List<String>{
					SpLotteryBatchScheduler.JOB_NAME,
					a.TotalJobItems?.format(),
					a.NumberOfErrors?.format()
				}
			)
		);
	}
}
