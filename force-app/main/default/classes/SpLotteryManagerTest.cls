/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpLotteryManager
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryManagerTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void getLotteriesTest() {
		Test.startTest();

		SpLotteryResultWrap result = SpLotteryManager.getLotteries();

		Test.stopTest();

		System.assertEquals(
			1,
			result.activeList.size(),
			'Wrong Active Lottery List Number'
		);

		System.assertEquals(
			0,
			result.inactiveList.size(),
			'Wrong Inactive Lottery List Number'
		);
	}

	@isTest
	static void getLotteriesInacTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		Boolean inactivated = SpLotteryManager.inactivateLottery(lId);

		Test.startTest();

		SpLotteryResultWrap result = SpLotteryManager.getLotteries();

		Test.stopTest();

		System.assertEquals(
			0,
			result.activeList.size(),
			'Wrong Active Lottery List Number'
		);

		System.assertEquals(
			1,
			result.inactiveList.size(),
			'Wrong Inactive Lottery List Number'
		);
	}

	@isTest
	static void getLotteryTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		Lottery__c l = SpLotteryManager.getLottery(lId);

		Test.stopTest();

		System.assertEquals(
			'09',
			l.Grade_Applying_For__c,
			'Wrong Grade Lottery'
		);
		System.assertEquals(
			'ASU PREP POLYTECHNIC',
			l.Site_of_Interest__c,
			'Wrong Site Lottery'
		);
	}

	@isTest
	static void getLotteryNullTest() {
		try {
			Test.startTest();

			Lottery__c l = SpLotteryManager.getLottery(null);
			
			Test.stopTest();
		} catch (Exception ex) {
			System.assertEquals(
				'List has no rows for assignment to SObject',
				ex.getMessage(),
				'Wrong get Lottery process'
			);
		}
	}

	@isTest
	static void getLotteriesBySetupTest() {
		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP POLYTECHNIC';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 5;

		Test.startTest();

		List<Lottery__c> lList = SpLotteryManager.getLotteriesBySetup(setup);

		Test.stopTest();

		System.assertEquals(1, lList.size(), 'Wrong Lottery List Number');
	}

	@isTest
	static void createLotteryTest() {
		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2033';
		setup.seats = 5;

		Test.startTest();

		Lottery__c l = SpLotteryManager.createLottery(setup);

		Test.stopTest();

		System.assert(l != null, 'Wrong Lottery created');
		System.assertEquals(
			'ASU PREP DIGITAL',
			l.Site_of_Interest__c,
			'Wrong Site Lottery List Number'
		);
	}

	@isTest
	static void inactivateLotteryTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		Boolean inactivated = SpLotteryManager.inactivateLottery(lId);

		Test.stopTest();

		List<Lottery__c> lList = [
			SELECT Id
			FROM Lottery__c
			WHERE Active__c = FALSE
		];

		System.assertEquals(true, inactivated, 'Lottery not Inactive');
		System.assertEquals(
			1,
			lList.size(),
			'Wrong Inactive Lottery List Number'
		);
	}

	@isTest
	static void inactivateLotteryNullTest() {
		Test.startTest();

		Boolean inactivated = SpLotteryManager.inactivateLottery(null);

		Test.stopTest();

		System.assertEquals(
			false,
			inactivated,
			'Wrong Lottery Inactivation process'
		);
	}
}
