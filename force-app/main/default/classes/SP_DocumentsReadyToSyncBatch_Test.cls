/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 09-14-2020
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   09-14-2020   dmorales   Initial Version
 **/
@isTest
private class SP_DocumentsReadyToSyncBatch_Test {
@isTest
static void testBatch() {
	SP_TestDataFactory.createApplicationWithDocument('Accepted','Success');
	Test.startTest();
	SP_DocumentsReadyToSyncBatch documentBatch = new SP_DocumentsReadyToSyncBatch();
	Database.executeBatch(documentBatch, SP_DocumentsReadyToSyncBatch.BATCH_EXECUTION_SCOPE);
	Test.stopTest();
	List<ContentVersion> cvList = [SELECT IsReadyToSync__c, Sent__c
	                               FROM ContentVersion
	                               WHERE Title = 'TestDocumentApplication'
	                               AND PathOnClient = 'testDocumentApplication.jpg'];

	System.assert (cvList[0].IsReadyToSync__c);
	System.assert (!cvList[0].Sent__c);
}
}