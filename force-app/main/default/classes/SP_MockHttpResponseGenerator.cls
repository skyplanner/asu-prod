/**
 * @author       Yaneivys Gutierrez
 * @date         03/29/2021
 * @description  Interface mock HTTP Response generator implementation
 */
@isTest
public class SP_MockHttpResponseGenerator implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        List<ASU_Prep_Data__c> asupdList = [
            SELECT Id, ASU_ID__c
            FROM ASU_Prep_Data__c
        ];
        System.assert(
            asupdList.size() == 1,
            'At least one ASU Prep Data is expected'
        );

        ASU_Prep_Data__c asupd = asupdList[0];

        // Fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);

        Map<String, String> body = new Map<String, String>();
        body.put('transactionId', asupd.Id);
        body.put('emplid', asupd.ASU_ID__c);
        if (asupd.ASU_ID__c == '1217994635') {
            body.put('status', 'pending');
            body.put(
                'description',
                'Already Enrolled in Class, Add Not Processed.'
            );
        } else if (asupd.ASU_ID__c == '1221229685') {
            body.put('status', 'Complete');
            body.put('description', 'Class Enrolled Successfully.');
        } else if (asupd.ASU_ID__c == '1217994345') {
            body.put('transactionId', '');
            body.put('status', 'pending');
            body.put('description', 'Error in transaction.');
        } else if (asupd.ASU_ID__c == '1274564345') {
            body.put('transactionId', 'transactionIdTest');
            body.put('status', 'pending');
            body.put(
                'description',
                'Already Enrolled in Class, Add Not Processed.'
            );
        }

        res.setBody(
            '{"ASU_CS_PREP_DIGITAL_RESP":' +
            JSON.serialize(body) +
            '}'
        );

        if (asupd.ASU_ID__c == '1279874545') {
            res.setStatusCode(500);
            res.setBody('{"message":"Response code 500 mapped as failure."}');
        }

        return res;
    }
}
