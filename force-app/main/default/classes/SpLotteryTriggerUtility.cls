/*
 * @author				Yaneivys Gutierrez
 * @date				05/13/2022
 * @description			Lottery Trigger Utility
 * @group
 * @last modified on	05/13/2022
 * @last modified by	05/13/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/13/2022		Yaneivys Gutierrez		Initial Version
 */
public with sharing class SpLotteryTriggerUtility {
	public static void handleBeforeUpdate(
		List<Lottery__c> lotNew,
		Map<Id, Lottery__c> lotOldMap
	) {
		Set<Id> users = SpLotteryEditPermissionMDT.getUserIds();
		Boolean canEdit = users.contains(UserInfo.getUserId());

		for (Lottery__c l : lotNew) {
			if (!canEdit) {
				l.addError(System.Label.SP_VAL_WRONG_USER);
			}
			if (l.Seats__c == null) {
				l.addError(System.Label.SP_VAL_NULL);
			}
			Lottery__c lOld = lotOldMap.get(l.Id);
			if (lOld.Seats__c > l.Seats__c) {
				l.addError(System.Label.SP_VAL_UNDERFLOW);
			}
			if (lOld.Seats__c < l.Seats__c) {
				l.Available_Seats__c += l.Seats__c - lOld.Seats__c;
			}
		}
	}
}
