/**
 * @author				Yaneivys Gutierrez
 * @date				04/28/2022
 * @description			Test class for SpThrewWarningInAppCtrl
 * group
 * @last modified on	04/28/2022
 * @last modified by	04/28/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		04/28/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpThrewWarningInAppCtrlTest {
	@isTest
	static void loadRecordTest() {
		Id appId = SP_TestDataFactory.createImmersionApp();

		Application__c app = new Application__c();
		app.Id = appId;
		app.Grade_Applying_For__c = '08';
		app.Site_of_Interest__c = 'ASU PREP DIGITAL';
		update app;

		Test.startTest();

		String result = SpThrewWarningInAppCtrl.loadRecord(appId);

		Test.stopTest();

		System.assertEquals(
			'pre-dig-app',
			result,
			'Wrong result for warning application app.'
		);
	}

	@isTest
	static void loadRecordImmersionTest() {
		Id appId = SP_TestDataFactory.createImmersionApp();

		Test.startTest();

		String result = SpThrewWarningInAppCtrl.loadRecord(appId);

		Test.stopTest();

		System.assertEquals(
			'',
			result,
			'Wrong result for warning application app.'
		);
	}
}
