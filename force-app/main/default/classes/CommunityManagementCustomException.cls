/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 02-01-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   02-01-2021   dmorales   Initial Version
**/
public class CommunityManagementCustomException extends Exception{    
}