/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpLotteryViewCntrl
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryViewCntrlTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void fetchLotteryDataNullTest() {
		try {
			Test.startTest();

			SpLotteryViewWrap result = SpLotteryViewCntrl.fetchLotteryData(
				null
			);

			Test.stopTest();
		} catch (Exception ex) {
			System.assertEquals(
				'Script-thrown exception',
				ex.getMessage(),
				'Wrong Lottery Data process'
			);
		}
	}

	@isTest
	static void fetchLotteryDataTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		SpLotteryViewWrap result = SpLotteryViewCntrl.fetchLotteryData(lId);

		Test.stopTest();

		System.assertEquals(
			lId,
			result.lottery.Id,
			'Wrong fetch Lottery Data process'
		);
		System.assertEquals(
			1,
			result.selectedApps.size(),
			'Wrong fetch Lottery Data process'
		);
		System.assertEquals(
			1,
			result.waitingList.size(),
			'Wrong fetch Lottery Data process'
		);
	}

	@isTest
	static void addAppsToLotteryNullTest() {
		try {
			Test.startTest();

			SpLotteryViewWrap result = SpLotteryViewCntrl.addAppsToLottery(
				null,
				null
			);

			Test.stopTest();
		} catch (Exception ex) {
			System.assertEquals(
				'Script-thrown exception',
				ex.getMessage(),
				'Wrong Lottery Data process'
			);
		}
	}

	@isTest
	static void addAppsToLotteryTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 1;

		SpMsgResultWrap lotResult = SpLotteryProcessManager.runLottery(setup);
		SpLotteryViewWrap result = SpApplicationManager.getLotteryData(
			lotResult.data.lotteryId
		);
		Application__c app = result.selectedApps[0];
		app.Application_Status__c = 'Withdrew';
		update app;

		SpLotteryProcessManager.reCalculateSelectionNo(
			new Set<Id>{ lotResult.data.lotteryId }
		);

		List<Id> appIds = new List<Id>{ result.waitingList[0].Id };

		Test.startTest();

		result = SpLotteryViewCntrl.addAppsToLottery(
			lotResult.data.lotteryId,
			appIds
		);

		Test.stopTest();

		System.assertEquals(
			lotResult.data.lotteryId,
			result.lottery.Id,
			'Wrong fetch Lottery Data process'
		);
		System.assertEquals(
			1,
			result.selectedApps.size(),
			'Wrong fetch Lottery Data process'
		);
		System.assertEquals(
			1,
			result.waitingList.size(),
			'Wrong fetch Lottery Data process'
		);
	}

	@isTest
	static void saveLotteryNullTest() {
		Lottery__c l = [SELECT Id FROM Lottery__c LIMIT 1];
		l.Seats__c = null;

		try {
			Test.startTest();

			SpLotteryViewCntrl.saveLottery(l);

			Test.stopTest();
		} catch (Exception ex) {
			System.assert(
				ex.getMessage().contains('Script-thrown exception'),
				'Wrong Seats Update process'
			);
		}
	}

	@isTest
	static void saveLotteryTest() {
		Lottery__c l = [SELECT Id FROM Lottery__c LIMIT 1];
		l.Seats__c = 9;

		Test.startTest();

		SpLotteryViewCntrl.saveLottery(l);

		Test.stopTest();

		l = [SELECT Id, Available_Seats__c FROM Lottery__c LIMIT 1];

		System.assertEquals(
			8,
			l.Available_Seats__c,
			'Wrong Seats Update process'
		);
	}
}
