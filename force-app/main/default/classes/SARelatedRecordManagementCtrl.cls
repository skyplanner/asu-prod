/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 04-20-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-08-2021   dmorales   Initial Version
**/
public without sharing class SARelatedRecordManagementCtrl {
    @AuraEnabled
    public static List<School__c> getSchools(String applicationId) {
        if (!Schema.sObjectType.School__c.IsQueryable()) {
            return null;
         }
        return [ 
                   SELECT Id, 
                          Name, 
                          Is_Current_School__c, 
                          School_State__c,
                          School_City__c
                   FROM   School__c 
                   WHERE Student_Application__c = : applicationId
                   ORDER BY Is_Current_School__c DESC
                 ];
    }

    @AuraEnabled
    public static List<Self_Report_Transcript__c> getCourses(String applicationId) {
         if (!Schema.sObjectType.Self_Report_Transcript__c.IsQueryable()) {
            return null;
         }
          return [ SELECT Id, 
                    Course_Name__c, 
                    Course_Status__c, 
                    Course_Type__c, 
                    School_Year__c,
                    Honors__c, 
                    Current_Letter_Grade__c,
                    Letter_Grade_Earned__c,
                    Credits_Earned__c,
                    School__r.Name,
                    Student_Application__r.Total_Credits_Earned__c 
                    FROM Self_Report_Transcript__c 
                    WHERE Student_Application__c = : applicationId     
                    ORDER BY School__r.Name, Course_Name__c
                 ];
    }

    @AuraEnabled
    public static list<Contact> getAllContact(string accId)
    {
       if (!Schema.sObjectType.Contact.IsQueryable()) {
            return null;
       }
       return [ SELECT Id, 
          Name, 
          Phone,
          Email,
          Birthdate, 
          RecordTypeId,
          RecordType.Name
          FROM Contact 
          WHERE AccountId = : accId     
          ORDER BY Name
       ];
     
    }

    public static User getContactUser(String userId)
    {
        if (!Schema.sObjectType.User.IsQueryable()) {
            return null;
        }
        else {
            List<User> users = [SELECT ContactId,
                            Contact.Name, 
                            Contact.RecordTypeId, 
                            Contact.Current_Age__c , 
                            Contact.Is_Emancipated__c,
                            ProfileId 
                            FROM User
                            WHERE Id =: userId ];
           if(users.size() > 0  && users[0].ContactId != null ){
              return users[0];
           }
        
        return null;
        }
      
    }

    @AuraEnabled    
    public static ContactAccessWrapper getContactAccess(String userId)
    {
        User currentUser = getContactUser(userId);
        if(currentUser != null){
            HA_Management_Settings__c haSettings = HA_Management_Settings__c.getOrgDefaults();
            Id parentProfileId = haSettings.Parent_Profile_Id__c;
            Id contactParentRT = haSettings.Parent_RT__c;
            Boolean hasPermission =  (currentUser.ProfileId == parentProfileId && 
                                      currentUser.Contact.RecordTypeId == contactParentRT) ;                                      
            ContactAccessWrapper contactAccess = new  ContactAccessWrapper();
            contactAccess.hasAccess = hasPermission;
            contactAccess.contactId = currentUser.ContactId;
            return contactAccess;           
        }        
        
        return null;

    }

    public class ContactAccessWrapper {
        @AuraEnabled
        public Id contactId {set;get;}
        @AuraEnabled
        public Boolean hasAccess {set;get;}
    }
}