/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpUtilities
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpUtilitiesTest {
	@isTest
	static void getFieldDescribeTest() {
		Test.startTest();

		Schema.DescribeFieldResult result = SpUtilities.getFieldDescribe(
			Schema.sObjectType.Application__c.getName(),
			'Grade_Applying_For__c'
		);

		Test.stopTest();

		System.assert(result != null, 'Wrong DescribeFieldResult');
	}

	@isTest
	static void getPicklistPairTest() {
		Test.startTest();

		List<Map<String, String>> result = SpUtilities.getPicklistPair(
			Schema.sObjectType.Application__c.getName(),
			'Grade_Applying_For__c'
		);

		Test.stopTest();

		System.assert(result.size() > 0, 'Wrong Picklist Pair');
	}

	@isTest
	static void getPicklistDependencyTest() {
		Test.startTest();

		Map<String, List<Map<String, String>>> result = SpUtilities.getPicklistDependency(
			Schema.sObjectType.Application__c.getName(),
			'Site_of_Interest__c'
		);

		Test.stopTest();

		System.assert(result.keySet().size() > 0, 'Wrong Picklist Dependency');
	}

	@isTest
	static void getPicklistWithoutDependencyTest() {
		Test.startTest();

		Map<String, List<Map<String, String>>> result = SpUtilities.getPicklistDependency(
			Schema.sObjectType.Application__c.getName(),
			'Grade_Applying_For__c'
		);

		Test.stopTest();

		System.assert(result.keySet().size() > 0, 'Wrong Picklist Dependency');
	}
}
