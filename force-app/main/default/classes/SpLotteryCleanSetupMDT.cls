/*
 * @author				Yaneivys Gutierrez
 * @date				05/05/2022
 * @description			Lottery Clean Setup Metadata Type Manager Class
 * @group
 * @last modified on	05/05/2022
 * @last modified by	05/05/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/05/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryCleanSetupMDT {
	public static List<String> getCleanStatusses() {
		List<String> statusses = new List<String>();

		List<Lottery_Clean_Setup__mdt> setsMdt = [
			SELECT Application_Status__c
			FROM Lottery_Clean_Setup__mdt
		];

		for (Lottery_Clean_Setup__mdt s : setsMdt) {
			statusses.add(s.Application_Status__c);
		}

		return statusses;
	}
}
