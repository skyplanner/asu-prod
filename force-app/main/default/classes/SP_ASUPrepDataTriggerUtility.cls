/**
 * @author       Yaneivys Gutierrez
 * @date         03/22/2021
 * @description  Utility Class for ASU Prep Data Trigger
 */
public with sharing class SP_ASUPrepDataTriggerUtility {
    private class Response {
        private String transactionId;
        private String emplid;
        private String status;
        private String description;
    }

    private class ResponseData {
        private Response ASU_CS_PREP_DIGITAL_RESP;
    }

    private static String JSONStringUpdate(String jsonString) {
        return jsonString.replace('transaction-id', 'transactionId');
    }

    private static void processResponse(Response response) {
        if (response.transactionId == null || response.transactionId == '') {
            System.debug('No transaction Id in response from PeopleSoft');
            return;
        }

        if (!ASU_Prep_Data__c.sObjectType.getDescribe().isAccessible()) {
            System.debug('ASU Prep Data sObject Access denied.');
            return;
        }

        List<ASU_Prep_Data__c> asupdList = [
            SELECT Id, Case__c
            FROM ASU_Prep_Data__c
            WHERE Id = :response.transactionId
        ];

        if (asupdList == null || asupdList.size() <= 0) {
            System.debug('ASU Prep Data sObject null.');
            return;
        }

        Case relCase = new Case(Id = asupdList[0].Case__c);

        if (response.status == 'Pending') {
            relCase.Status = response.status;
            relCase.Reason = 'Errors in Request';
            relCase.Description = response.description;
        } else if (response.status == 'Complete') {
            relCase.Status = response.status;
            relCase.Reason = '';
            relCase.Description = response.description;
        }

        if (!Case.sObjectType.getDescribe().isUpdateable()) {
            System.debug('Case object Update Access denied.');
            return;
        }

        update relCase;
    }

    private static void reportResponse(Id transId, string message) {
        Messaging.reserveSingleEmailCapacity(2);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        List<ASU_Prep_Data__c> asupd = [
            SELECT
                Id,
                Case__r.CaseNumber,
                Case__r.Owner.FirstName,
                Case__r.Owner.Username
            FROM ASU_Prep_Data__c
            WHERE Id = :transId
        ];
        String[] toAddresses = new List<String>();
        if (asupd.size() > 0 && asupd[0].Case__r.Owner.Username != null) {
            toAddresses.add(asupd[0].Case__r.Owner.Username);
        }

        List<String> emails = SP_TransmReportEmailMetadataUtility.getTransmissionReportEmails();
        if (emails != null && emails.size() > 0) {
            toAddresses.addAll(emails);
        }

        if (toAddresses.size() <= 0) {
            System.debug('Not email to inform about transmission failed.');
            return;
        }

        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Support System');
        mail.setSubject(
            'Error in Transmission for the Case ' + asupd[0].Case__r.CaseNumber
        );
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setHtmlBody(
            '<p>Dear ' +
            asupd[0].Case__r.Owner.FirstName +
            ',</p>' +
            '<p>An error has been detected when the Case: ' +
            asupd[0].Case__r.CaseNumber +
            ' was transmitted to Peoplesoft.</p>' +
            '<p>These are the details of the error:</p>' +
            message +
            '<br/>' +
            '<b>Transmission Id</b>: ' +
            asupd[0].Id +
            '</p>' +
            '<p>Please contact your Salesforce Administrator to solve this issue.</p>'
        );

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
    }

    @future(callout=true)
    private static void handleRequestPeopleSoft(Id transId, String body) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:PeopleSoftIntegration');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json;charset=UTF-8');
        req.setBody(body);

        Http http = new Http();

        try {
            HTTPResponse res = http.send(req);

            if (res.getStatusCode() == 200 || res.getStatusCode() == 201) {
                ResponseData data = (ResponseData) JSON.deserialize(
                    JSONStringUpdate(res.getBody()),
                    ResponseData.class
                );

                processResponse(data.ASU_CS_PREP_DIGITAL_RESP);
            } else {
                String msg =
                    '<p><b>Response Status Code</b>: ' +
                    res.getStatusCode() +
                    '<br/>' +
                    '<b>Response Body</b>: ' +
                    res.getBody();

                reportResponse(transId, msg);
            }
        } catch (Exception e) {
            System.debug(
                'Exception transmitting Case to Peoplesoft: ' + e.getMessage()
            );
            reportResponse(transId, e.getMessage());
        }
    }

    public static void handleAfterInsert(Set<Id> newMapIds) {
        if (!ASU_Prep_Data__c.sObjectType.getDescribe().isAccessible()) {
            System.debug('ASU Prep Data sObject Access denied.');
            return;
        }

        List<ASU_Prep_Data__c> asupdList = [
            SELECT
                Id,
                Type__c,
                Status__c,
                Contact_Name__c,
                ASU_ID__c,
                Citizenship__c,
                Term_Code__c,
                Class__c,
                Related_Class_1__c,
                Related_Class_2__c,
                Effective_Date__c
            FROM ASU_Prep_Data__c
            WHERE Id IN :newMapIds
        ];

        for (ASU_Prep_Data__c asupd : asupdList) {
            Map<String, String> asupdMap = new Map<String, String>();
            asupdMap.put('transaction-id', asupd.Id);
            asupdMap.put('type', asupd.Type__c != null ? asupd.Type__c : '');
            asupdMap.put(
                'status',
                asupd.Status__c != null ? asupd.Status__c : ''
            );
            asupdMap.put(
                'contactName',
                asupd.Contact_Name__c != null ? asupd.Contact_Name__c : ''
            );
            asupdMap.put(
                'emplid',
                asupd.ASU_ID__c != null ? asupd.ASU_ID__c : ''
            );
            asupdMap.put(
                'citizenship',
                asupd.Citizenship__c != null ? asupd.Citizenship__c : ''
            );
            asupdMap.put(
                'strm',
                asupd.Term_Code__c != null ? asupd.Term_Code__c : ''
            );
            asupdMap.put('class', asupd.Class__c != null ? asupd.Class__c : '');
            asupdMap.put(
                'relatedClass1',
                asupd.Related_Class_1__c != null ? asupd.Related_Class_1__c : ''
            );
            asupdMap.put(
                'relatedClass2',
                asupd.Related_Class_2__c != null ? asupd.Related_Class_2__c : ''
            );
            asupdMap.put(
                'effdt',
                asupd.Effective_Date__c != null
                    ? DateTime.newInstance(
                              asupd.Effective_Date__c.year(),
                              asupd.Effective_Date__c.month(),
                              asupd.Effective_Date__c.day()
                          )
                          .format('yyyy-MM-dd')
                    : ''
            );

            handleRequestPeopleSoft(asupd.Id, JSON.serialize(asupdMap));
        }
    }
}
