/**
 * @description       : Test class to check if a value exists in a picklist field on SA object.
 * @group             : 
 * @last modified on  : 05-11-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   04-02-2021   @svelazquez   Initial Version
**/
@isTest
private class CheckValueExistInPicklistTest {

    @isTest static void checkIfValueIsOnPicklistOK(){
        CheckValueExistInPicklist.PicklistData data = new CheckValueExistInPicklist.PicklistData();
        data.objectName = 'Application__c';
        data.picklistField = 'Student_Classification__c';
        data.valueToCheck = 'AZ-FT';

        Test.startTest();
        List<CheckValueExistInPicklist.ReturnData> results = CheckValueExistInPicklist.checkIfValueIsOnPicklist(
                                new List<CheckValueExistInPicklist.PicklistData>{data});
        Test.stopTest();

        System.assertEquals(true, results[0].valueExist, 'Picklist value does not exist');
    }

    @isTest static void checkIfValueIsOnPicklist_ValueNoExists(){
        CheckValueExistInPicklist.PicklistData data = new CheckValueExistInPicklist.PicklistData();
        data.objectName = 'Application__c';
        data.picklistField = 'Student_Classification__c';
        data.valueToCheck = 'Fake Value';

        Test.startTest();
        List<CheckValueExistInPicklist.ReturnData> results = CheckValueExistInPicklist.checkIfValueIsOnPicklist(
                                new List<CheckValueExistInPicklist.PicklistData>{data});
        Test.stopTest();

        System.assertNotEquals(true, results[0].valueExist, 'Picklist value exists');
    }

    
    @isTest static void checkIfValueIsOnPicklist_FieldNoExists(){
        CheckValueExistInPicklist.PicklistData data = new CheckValueExistInPicklist.PicklistData();
        data.objectName = 'Application__c';
        data.picklistField = 'Fake_Field__c';
        data.valueToCheck = 'AZ-FT/PT';

        Test.startTest();
        List<CheckValueExistInPicklist.ReturnData> results = CheckValueExistInPicklist.checkIfValueIsOnPicklist(
                                new List<CheckValueExistInPicklist.PicklistData>{data});
        Test.stopTest();

        System.assertNotEquals(true, results[0].valueExist, 'The Field: Student_Classific__c exists');
    }
}