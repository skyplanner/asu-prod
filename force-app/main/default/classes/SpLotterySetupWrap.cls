/*
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Lottery Setup Wrap
 * @group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotterySetupWrap {
	@AuraEnabled
	public String grade { get; set; }

	@AuraEnabled
	public String site { get; set; }

	@AuraEnabled
	public String term { get; set; }

	@AuraEnabled
	public String year { get; set; }

	@AuraEnabled
	public Decimal seats { get; set; }
}
