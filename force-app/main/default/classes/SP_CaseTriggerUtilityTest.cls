/**
 * @author       Yaneivys Gutierrez
 * @date         03/26/2021
 * @description  Test Class for SP_CaseTriggerUtility
 */
@isTest
private class SP_CaseTriggerUtilityTest {
    @TestSetup
    static void makeData() {
        SP_TestDataFactory.createDataCaseInsert();
    }

    @isTest
    static void testHandleAfterInsertUpdate() {
        // Testing Case insertion with unknown type for transmission
        Case case1 = new Case(
            Type = 'Unknown type for transmission',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = date.newInstance(2020, 12, 31),
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Dirja']
            .Id,
            ASU_ID__c = '1221229685',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '44945'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert case1;

        List<ASU_Prep_Data__c> asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(
            0,
            asupdList.size(),
            'No expected ASU Prep Data created'
        );

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new SP_MockHttpResponseGenerator());

        // Testing Case insertion with known type for transmission
        Case case2 = new Case(
            Type = 'Request for Registration',
            Status = 'New',
            Country_of_Citizenship__c = 'United States',
            Effective_Date__c = date.newInstance(2020, 12, 31),
            ContactId = [SELECT Id FROM Contact WHERE LastName = 'Edwards']
            .Id,
            ASU_ID__c = '1217994635',
            Term__c = [SELECT Id FROM hed__Term__c WHERE Code__c = '2214']
            .Id,
            Course_Offering_Req__c = [
                SELECT Id
                FROM hed__Course_Offering__c
                WHERE Name = '40960'
            ]
            .Id,
            Related_Class_1__c = '',
            Related_Class_2__c = ''
        );
        insert case2;

        asupdList = [SELECT Id FROM ASU_Prep_Data__c];
        System.assertEquals(1, asupdList.size(), 'One ASU Prep Data expected');
    }
}
