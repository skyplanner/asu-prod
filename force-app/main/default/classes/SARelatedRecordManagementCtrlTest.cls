/**
 * @description       : 
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : @svelazquez
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   04-03-2021   @svelazquez   Initial Version
**/
@isTest
private class SARelatedRecordManagementCtrlTest {
    @TestSetup
    static void makeData(){
        Id appId = SP_TestDataFactory.createApplicationWithoutDocument();
        List<School__c> schools = SP_TestDataFactory.createSchool(appId,1,true);
        SP_TestDataFactory.createCourse(appId, schools[0].Id);
        Id accId = SP_TestDataFactory.createAccount('Juan Perez', '(102) 1234566');
        SP_TestDataFactory.createContact(accId,'John','Edwards', 'jedwards@test.com');
    }

    @isTest static void getSchoolsAndCoursesTest(){
        List<School__c> schoolist;
        List<Self_Report_Transcript__c> coursesList;
        Id appId = SP_TestDataFactory.getApplication();
        
        Test.startTest();
        schoolist = SARelatedRecordManagementCtrl.getSchools(appId);
        coursesList = SARelatedRecordManagementCtrl.getCourses(appId);
        Test.stopTest();

        System.assertEquals(1, schoolist.size(), 
        'There are no Schools related with the SA specified');
        System.assertEquals(1, coursesList.size(), 
            'There are no Courses related with the SA specified');        
    }

    @isTest static void getAllContactTest(){
        Id accId = SP_TestDataFactory.getAccount().Id;

        Test.startTest();
        list<Contact> contacts = SARelatedRecordManagementCtrl.getAllContact(accId);
        Test.stopTest();

        System.assertEquals(1, contacts.size(), 
        'There are no Contacts related with the Account specified');
    }

    @isTest static void getContactAccessTestOK(){
        SARelatedRecordManagementCtrl.ContactAccessWrapper contactAccess;
        Id contId = SP_TestDataFactory.getContact();

        Test.startTest();
        Id userId = SP_TestDataFactory.createUser(false, 
                                                    contId, 
                                                    'ASU Prep Digital - Parent',
                                                    'Leslie',
                                                    'Brandon',
                                                    'leslie.brandon@test.com',
                                                    'leslie.brandon@test.com');
        contactAccess = SARelatedRecordManagementCtrl.getContactAccess(userId);
        Test.stopTest();

        System.assertNotEquals(null, contactAccess, 
        'The Contact Access is null');
    }

    @isTest static void getContactAccessTestNull(){
        SARelatedRecordManagementCtrl.ContactAccessWrapper contactAccess;

        Test.startTest();
        Id userId = SP_TestDataFactory.createUser(true, 
                                                    null, 
                                                    'System Administrator',
                                                    'Leslie',
                                                    'Brandon',
                                                    'leslie.brandon@test.com',
                                                    'leslie.brandon@test.com');
        contactAccess = SARelatedRecordManagementCtrl.getContactAccess(userId);
        Test.stopTest();

        System.assertEquals(null, contactAccess, 
        'The Contact Access should have a null value');
    }
}