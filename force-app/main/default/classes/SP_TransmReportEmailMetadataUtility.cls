/**
 * @author       Yaneivys Gutierrez
 * @date         03/29/2021
 * @description  Utility Class for Transmission Report Email Custom Metadata Types
 */
public without sharing class SP_TransmReportEmailMetadataUtility {
    public static List<String> getTransmissionReportEmails() {
        if (
            !Transmission_Report_Email__mdt.sObjectType.getDescribe()
                .isAccessible() ||
            !Schema.sObjectType.Transmission_Report_Email__mdt.fields.Email__c.isAccessible()
        ) {
            System.debug(
                'Transmission Report Email Metadata Type Access denied.'
            );
            return null;
        }

        List<Transmission_Report_Email__mdt> emailsReport = [
            SELECT Email__c
            FROM Transmission_Report_Email__mdt
        ];

        if (!emailsReport.isEmpty()) {
            List<String> emails = new List<String>();
            for (Transmission_Report_Email__mdt email : emailsReport) {
                emails.add(email.Email__c);
            }
            return emails;
        }

        return null;
    }
}
