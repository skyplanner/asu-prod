/**
 * @author       Yaneivys Gutierrez
 * @date         03/29/2021
 * @description  Test Class for SP_CaseTypeTransmissionMetadataUtility
 */
@isTest
private class SP_CaseTypeTransmMetadataUtilityTest {
    @isTest
    static void testGetCodeByCountry() {
        List<String> types = SP_CaseTypeTransmissionMetadataUtility.getPossibleTypesForTransmission();
        System.assert(
            types.size() > 0,
            'At least one ossible Type for transmission'
        );
    }
}
