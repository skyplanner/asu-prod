/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 03-26-2021
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   03-04-2021   dmorales   Initial Version
 **/
public with sharing class SchoolTriggerUtility {

public static void handleCurrentSchoolSync(List<School__c> schools) {

	Map<Id, School__c> currentSchoolApplicationMap = new Map<Id, School__c>();
	Map<Id, School__c> additionalSchoolApplicationMap = new Map<Id, School__c>();
	List<Id> currentSchoolList = new List<Id>();
	for(School__c schoolObj : schools) {
		if(schoolObj.Student_Application__c != null) {
			if(schoolObj.Is_Current_School__c) {
				if(currentSchoolApplicationMap.containsKey(schoolObj.Student_Application__c)) {
					//There are more than one Current School been inserted or updated for the same SA
					//throw error
					schoolObj.addError('You can\'t have more than one Current School for the same Student Application');
				}
				else{
					currentSchoolApplicationMap.put(schoolObj.Student_Application__c, schoolObj);
					currentSchoolList.add(schoolObj.Id);
				}
			}
			else{
				additionalSchoolApplicationMap.put(schoolObj.Student_Application__c, schoolObj);
			}
		}

	}

	List<School__c> existintCurrentSchoolsSA = [ SELECT Id, Student_Application__c
	                                             FROM School__c
	                                             WHERE Student_Application__c IN: currentSchoolApplicationMap.keySet()
	                                             AND Id NOT IN: currentSchoolList //to avoid recursively update actual triggered school
	                                             AND Is_Current_School__c =  TRUE];

	if(existintCurrentSchoolsSA.size() > 0) {
		for(School__c existingSchool : existintCurrentSchoolsSA) {
			existingSchool.Is_Current_School__c = FALSE;
		}
		update existintCurrentSchoolsSA;
	}

	List<Application__c> applicationToUpdate = [SELECT Id, Current_School_City_and_State__c, 
													   Current_School_Name__c, Additional_Schools__c  
	                                            FROM Application__c
												WHERE Id IN: currentSchoolApplicationMap.keySet()
												OR Id IN: additionalSchoolApplicationMap.keySet()];

	for(Application__c appObj : applicationToUpdate) {
		if(currentSchoolApplicationMap.containsKey(appObj.Id)){
			School__c currentSchool = currentSchoolApplicationMap.get(appObj.Id);
			appObj.Current_School_Name__c = currentSchool.Name;
			appObj.Current_School_City_and_State__c = currentSchool.School_City__c  + ',' + currentSchool.School_State__c;
		}
		if(additionalSchoolApplicationMap.containsKey(appObj.Id) && appObj.Additional_Schools__c == 'No'){
			appObj.Additional_Schools__c = 'Yes';
		}	
	}

	update applicationToUpdate;


}

public static void handlePreventCurrentSchoolDeletion(List<School__c> schools) {
	Set<Id> additionalSchoolApplication = new Set<Id>();
	Set<Id> additionalSchool = new Set<Id>();
	for(School__c schoolObj : schools) {
		
		if(schoolObj.Student_Application__c != null) {
			if(schoolObj.Is_Current_School__c) {
				schoolObj.addError('You can\'t delete Current School associated with a Student Application');
			}
			else{
				additionalSchoolApplication.add(schoolObj.Student_Application__c);
				additionalSchool.add(schoolObj.Id);
			}
		}
	}

	List<Application__c> appWithSchools = [SELECT Id ,Additional_Schools__c, 
												 (SELECT Id 
												  FROM Schools__r 
												  WHERE Is_Current_School__c = FALSE
												  AND Id NOT IN : additionalSchool) 
										   FROM Application__c
										   WHERE Id IN: additionalSchoolApplication];

	List<Application__c>  appToUpdate = new List<Application__c>();
    for(Application__c appObj : appWithSchools){
		 List<School__c> appObjSchools = appObj.Schools__r;
		 if(appObjSchools.size() ==  0){
			appObj.Additional_Schools__c = 'No';
			appToUpdate.add(appObj);
		 } 
	}
	
	update appToUpdate;	

}
}