/**
 * @author       Yaneivys Gutierrez
 * @date         03/25/2021
 * @description  Utility Class for Country Code Custom Metadata Types
 */
public without sharing class SP_CountryCodeMetadataUtility {
    public static Country_Code__mdt getCodeByCountry(String country) {
        if (
            !Country_Code__mdt.sObjectType.getDescribe().isAccessible() ||
            !Schema.sObjectType.Country_Code__mdt.fields.ALPHA_3__c.isAccessible()
        ) {
            System.debug('Country Code Metadata Type Access denied.');
            return null;
        }

        List<Country_Code__mdt> codes = [
            SELECT ALPHA_3__c
            FROM Country_Code__mdt
            WHERE Country__c = :country
        ];

        if (!codes.isEmpty()) {
            return codes[0];
        }

        return null;
    }
}
