/**
 * @author				Yaneivys Gutierrez
 * @date				05/12/2022
 * @description			Test class for SpLotteryViewCntrl
 * group
 * @last modified on	05/12/2022
 * @last modified by	05/12/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/12/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryCntrlTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void getOptionsTest() {
		Test.startTest();

		SpLotteryOptionsWrap result = SpLotteryCntrl.getOptions();

		Test.stopTest();

		System.assert(
			result.gradeOptions.size() > 0,
			'Wrong get Option process'
		);
		System.assert(
			result.siteOptions.size() > 0,
			'Wrong get Option process'
		);
		System.assert(
			result.termOptions.size() > 0,
			'Wrong get Option process'
		);
	}

	@isTest
	static void fetchLotteriesTest() {
		Test.startTest();

		SpLotteryResultWrap result = SpLotteryCntrl.fetchLotteries();

		Test.stopTest();

		System.assertEquals(
			1,
			result.activeList.size(),
			'Wrong fetch Lotteries process'
		);
		System.assertEquals(
			0,
			result.inactiveList.size(),
			'Wrong fetch Lotteries process'
		);
	}

	@isTest
	static void runLotteryExcepTest() {
		try {
			Test.startTest();

			SpMsgResultWrap result = SpLotteryCntrl.runLottery(null);

			Test.stopTest();
		} catch (Exception ex) {
			System.assertEquals(
				'Script-thrown exception',
				ex.getMessage(),
				'Wrong Lottery Data process'
			);
		}
	}

	@isTest
	static void runLotteryTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;
		SP_TestDataFactory.createPossibleAppsForLottery();

		SpLotterySetupWrap setup = new SpLotterySetupWrap();
		setup.grade = '09';
		setup.site = 'ASU PREP DIGITAL';
		setup.term = 'Fall';
		setup.year = '2030';
		setup.seats = 1;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryCntrl.runLottery(setup);

		Test.stopTest();

		lId = [SELECT Id FROM Lottery__c WHERE Id != :lId LIMIT 1].Id;

		System.assertEquals(
			'success',
			result.variant,
			'Wrong Lottery Creation'
		);
		System.assertEquals(
			lId,
			result.data.lotteryId,
			'Wrong Lottery Created'
		);
	}

	@isTest
	static void inactivateProcessTest() {
		Id lId = [SELECT Id FROM Lottery__c LIMIT 1].Id;

		Test.startTest();

		SpMsgResultWrap result = SpLotteryCntrl.inactivateProcess(lId);

		Test.stopTest();

		List<Lottery__c> lList = [
			SELECT Id
			FROM Lottery__c
			WHERE Active__c = FALSE
		];

		System.assertEquals('success', result.variant, 'Lottery not Inactive');
		System.assertEquals(
			System.Label.SP_LOTTERY_INACTIVATION_SUCCESS,
			result.msg,
			'Wrong Inactivation Lottery process'
		);
		System.assertEquals(
			1,
			lList.size(),
			'Wrong Inactive Lottery List Number'
		);
	}
}
