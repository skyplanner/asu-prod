/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 08-13-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-23-2021   dmorales   Initial Version
**/
public with sharing class CheckValueExistInPicklist {

    @InvocableMethod(label='Check If Value exist in Picklist')
    public static List<ReturnData> checkIfValueIsOnPicklist(List<PicklistData> dataIn) {

        ReturnData returnValue = new ReturnData();
        returnValue.valueExist = false;
        PicklistData data = dataIn[0];
        if(STRING.ISBLANK(data.valueToCheck)){
           return new List<ReturnData>{returnValue};
        }

        try{
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(data.objectName);
            Sobject objectApiName = targetType.newSObject();
            Schema.sObjectType sobjectType = objectApiName.getSObjectType();
            Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
            Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap();
            List<Schema.PicklistEntry> pickListValues = fieldMap.get(data.picklistField).getDescribe().getPickListValues();
        
            for (Schema.PicklistEntry a : pickListValues) {
                if(a.getLabel() == data.valueToCheck || a.getValue() == data.valueToCheck){                    
                     returnValue.valueExist = true;
                     returnValue.picklistLabel = a.getLabel();
                     returnValue.picklistValue = a.getValue();
                     break;
                }                 
            }
           
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
        }

        return new List<ReturnData>{returnValue};
     
    }

    public class PicklistData {
		@InvocableVariable(required=true label='Picklist API Name ')
		public string picklistField;

        @InvocableVariable(required=true label='Object API Name ')
		public string objectName;

		@InvocableVariable(required=false label='Value to Check')
		public string valueToCheck;
    }
    
    public class ReturnData {
		@InvocableVariable(required=true label='Value Exist')
		public Boolean valueExist;

        @InvocableVariable(required=true label='Picklist Label')
        public String picklistLabel;	
        
        @InvocableVariable(required=true label='Picklist Value')
		public String picklistValue;	
	}
}