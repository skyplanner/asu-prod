/**
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Lottery Manager class
 * group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryManager {
	public static SpLotteryResultWrap getLotteries() {
		SpLotteryResultWrap result = new SpLotteryResultWrap();
		result.activeList = new List<Lottery__c>();
		result.inactiveList = new List<Lottery__c>();

		List<Lottery__c> lotteries = [
			SELECT
				Id,
				Name,
				Grade_Applying_For__c,
				Site_of_Interest__c,
				Term_Applying_for__c,
				Year_applying_for__c,
				Available_Seats__c,
				Seats__c,
				Run_Date__c,
				Active__c,
				Inactivate_Date__c
			FROM Lottery__c
			ORDER BY CreatedDate DESC
		];

		for (Lottery__c l : lotteries) {
			if (l.Active__c) {
				result.activeList.add(l);
			} else {
				result.inactiveList.add(l);
			}
		}

		return result;
	}

	public static Lottery__c getLottery(Id lId) {
		return [
			SELECT
				Id,
				Name,
				Grade_Applying_For__c,
				Site_of_Interest__c,
				Term_Applying_for__c,
				Year_applying_for__c,
				Available_Seats__c,
				Seats__c,
				Run_Date__c,
				Active__c,
				Inactivate_Date__c
			FROM Lottery__c
			WHERE Id = :lId
		];
	}

	public static List<Lottery__c> getLotteriesBySetup(
		SpLotterySetupWrap setup
	) {
		return [
			SELECT Id, Name
			FROM Lottery__c
			WHERE
				Grade_Applying_For__c = :setup.grade
				AND Site_of_Interest__c = :setup.site
				AND Term_Applying_for__c = :setup.term
				AND Year_applying_for__c = :setup.year
				AND Seats__c = :setup.seats
		];
	}

	public static Lottery__c createLottery(SpLotterySetupWrap setup) {
		Lottery__c l = new Lottery__c();
		l.Grade_Applying_For__c = setup.grade;
		l.Site_of_Interest__c = setup.site;
		l.Term_Applying_for__c = setup.term;
		l.Year_applying_for__c = setup.year;
		l.Available_Seats__c = setup.seats;
		l.Seats__c = setup.seats;
		l.Active__c = true;
		l.Run_Date__c = Datetime.now();
		l.Inactivate_Date__c = null;

		insert l;
		return l;
	}

	public static Boolean inactivateLottery(Id lId) {
		List<Lottery__c> lotteries = [
			SELECT Id
			FROM Lottery__c
			WHERE Id = :lId
		];
		if (lotteries.size() > 0) {
			Lottery__c l = lotteries[0];
			l.Inactivate_Date__c = Datetime.now();
			l.Active__c = false;

			update l;

			return true;
		}

		return false;
	}
}
