/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpLotteryCleanSetupMDT
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryCleanSetupMDTTest {
	@isTest
	static void getCleanStatussesTest() {
		Test.startTest();

		List<String> statusses = SpLotteryCleanSetupMDT.getCleanStatusses();

		Test.stopTest();

		System.assert(statusses.size() > 0, 'Wrong statuses metadata');
	}
}
