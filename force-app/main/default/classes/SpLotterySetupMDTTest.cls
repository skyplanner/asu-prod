/**
 * @author				Yaneivys Gutierrez
 * @date				05/11/2022
 * @description			Test class for SpLotterySetupMDT
 * group
 * @last modified on	05/11/2022
 * @last modified by	05/11/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/11/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotterySetupMDTTest {
	@isTest
	static void getCleanStatussesTest() {
		Test.startTest();

		Lottery_Setup__mdt sett = SpLotterySetupMDT.getInstance()
			.LOT_MDT_SETTING;

		Test.stopTest();

		System.assert(sett != null, 'Wrong Lottery setup metadata');
	}
}
