/*
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Lottery LWC Cntrl
 * @group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public with sharing class SpLotteryCntrl {
	@AuraEnabled
	public static SpLotteryOptionsWrap getOptions() {
		SpLotteryOptionsWrap wrap = new SpLotteryOptionsWrap();

		Set<Id> users = SpLotteryEditPermissionMDT.getUserIds();
		wrap.canEdit = users.contains(UserInfo.getUserId());

		wrap.gradeOptions = SpUtilities.getPicklistPair(
			Schema.sObjectType.Application__c.getName(),
			'Grade_Applying_For__c'
		);
		wrap.gradeOptionsDependencies = SpUtilities.getPicklistDependency(
			Schema.sObjectType.Application__c.getName(),
			'Site_of_Interest__c'
		);
		wrap.siteOptions = SpUtilities.getPicklistPair(
			Schema.sObjectType.Application__c.getName(),
			'Site_of_Interest__c'
		);
		wrap.termOptions = SpUtilities.getPicklistPair(
			Schema.sObjectType.Application__c.getName(),
			'Term_Applying_for__c'
		);

		return wrap;
	}

	@AuraEnabled
	public static SpLotteryResultWrap fetchLotteries() {
		return SpLotteryManager.getLotteries();
	}

	@AuraEnabled
	public static SpMsgResultWrap runLottery(SpLotterySetupWrap setup) {
		try {
			return SpLotteryProcessManager.runLottery(setup);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static SpMsgResultWrap inactivateProcess(Id lId) {
		return SpLotteryProcessManager.inactivateProcess(lId);
	}
}
