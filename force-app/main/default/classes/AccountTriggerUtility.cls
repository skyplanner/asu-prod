/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 03-17-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   03-16-2021   dmorales   Initial Version
**/
public with sharing class AccountTriggerUtility {

    public static final Id HA_ACCOUNT_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household Account').getRecordTypeId();
    
    public static void checkHouseholdAccountOwnership(List<Account> accountList) {

           List<Account> householdAccounts = new List<Account> ();
           List<Id> householdOwners = new List<Id> ();

           for(Account act : accountList){
               if(act.RecordTypeId == HA_ACCOUNT_RT){
                   householdAccounts.add(act);
                   householdOwners.add(act.OwnerId);
               }
           }

           Map<Id, User> ownersMap = new Map<Id, User>([SELECT Id, UserRoleId FROM User WHERE Id IN :householdOwners]);

           for(Account hhAct : householdAccounts){
              User ownerAccount = ownersMap.get(hhAct.OwnerId);
              if(ownerAccount !=null && ownerAccount.UserRoleId == null){
                hhAct.addError('The Household Account must have a Role Assigned');   
              }
           }
           
    }
}