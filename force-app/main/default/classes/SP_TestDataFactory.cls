/**
 * @File Name          : SP_TestDataFactory.cls
 * @Description        :
 * @Author             : dmorales
 * @Group              :
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-23-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
 **/
@isTest
public class SP_TestDataFactory {
	public static final Id HA_ACCOUNT_RT = Schema.SObjectType.Account.getRecordTypeInfosByName()
		.get('Household Account')
		.getRecordTypeId();
	public static final Id B2C_LEAD_RT = Schema.SObjectType.Lead.getRecordTypeInfosByName()
		.get('B2C')
		.getRecordTypeId();

	public static void createApplicationWithDocument(String appStatus) {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Application_Status__c = appStatus
		);
		insert app;

		ContentVersion contentVersion = new ContentVersion(
			Title = 'TestDocumentApplication',
			PathOnClient = 'testDocumentApplication.jpg',
			VersionData = Blob.valueOf('Test Content'),
			IsMajorVersion = true
		);

		insert contentVersion;
		List<ContentDocument> documents = [
			SELECT Id, Title, LatestPublishedVersionId
			FROM ContentDocument
		];

		//create ContentDocumentLink  record
		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.LinkedEntityId = app.id;
		cdl.ContentDocumentId = documents[0].Id;
		cdl.shareType = 'V';
		insert cdl;
	}

	public static void createApplicationWithDocument(
		String appStatus,
		String syncStatus
	) {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Application_Status__c = appStatus,
			Sync_Status__c = syncStatus
		);
		insert app;

		ContentVersion contentVersion = new ContentVersion(
			Title = 'TestDocumentApplication',
			PathOnClient = 'testDocumentApplication.jpg',
			VersionData = Blob.valueOf('Test Content'),
			IsMajorVersion = true
		);

		insert contentVersion;
		List<ContentDocument> documents = [
			SELECT Id, Title, LatestPublishedVersionId
			FROM ContentDocument
		];

		//create ContentDocumentLink  record
		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.LinkedEntityId = app.id;
		cdl.ContentDocumentId = documents[0].Id;
		cdl.shareType = 'V';
		insert cdl;
	}

	public static Id createApplicationWithoutDocument() {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;
		return app.Id;
	}

	public static Id createLottery(Boolean active) {
		Lottery__c l = new Lottery__c(
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP POLYTECHNIC',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Available_Seats__c = 4,
			Seats__c = 5,
			Run_Date__c = Datetime.now(),
			Active__c = active,
			Inactivate_Date__c = active ? null : Datetime.now()
		);
		insert l;
		return l.Id;
	}

	public static Id createSelectedApplicationWithLottery(Id lId) {
		Application__c app = new Application__c(
			Lottery__c = lId,
			Lottery_Status__c = SpApplicationConst.LOT_SELECTED,
			Lottery_Selection_Date__c = Datetime.now(),
			Lottery_Selection_Type__c = SpApplicationConst.SEL_TYPE_AUTOMATIC,
			Lottery_Selection_Number__c = 1,
			Lottery_WL_No__c = null,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP POLYTECHNIC',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstNameSEL',
			Student_Last_Name__c = 'TestingStudentLastNameSEL',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;
		return app.Id;
	}

	public static Id createWlApplicationWithLottery(Id lId) {
		Application__c app = new Application__c(
			Lottery__c = lId,
			Lottery_Status__c = SpApplicationConst.LOT_WAITING_LIST,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = 1,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP POLYTECHNIC',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstNameWL',
			Student_Last_Name__c = 'TestingStudentLastNameWL',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;
		return app.Id;
	}

	public static Id createPossibleApplicationForLottery() {
		Application__c app = new Application__c(
			Lottery__c = null,
			Lottery_Status__c = SpApplicationConst.LOT_AVAILABLE,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = null,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP POLYTECHNIC',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstNamePosWL',
			Student_Last_Name__c = 'TestingStudentLastNamePosWL',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;
		return app.Id;
	}

	public static void createPossibleAppsForLottery() {
		Application__c app = new Application__c(
			Lottery__c = null,
			Lottery_Status__c = SpApplicationConst.LOT_AVAILABLE,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = null,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP DIGITAL',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName1',
			Student_Last_Name__c = 'TestingStudentLastName1',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;
		Application__c app2 = new Application__c(
			Lottery__c = null,
			Lottery_Status__c = SpApplicationConst.LOT_AVAILABLE,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = null,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP DIGITAL',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName2',
			Student_Last_Name__c = 'TestingStudentLastName2',
			Application_Status__c = 'Pending Final Review'
		);
		insert app2;
		Application__c app3 = new Application__c(
			Lottery__c = null,
			Lottery_Status__c = SpApplicationConst.LOT_AVAILABLE,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = null,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP DIGITAL',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2030',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName3',
			Student_Last_Name__c = 'TestingStudentLastName3',
			Application_Status__c = 'Pending Final Review'
		);
		insert app3;
	}

	public static Id createNotPossibleApplicationForLottery() {
		Application__c app = new Application__c(
			Lottery__c = null,
			Lottery_Status__c = SpApplicationConst.LOT_AVAILABLE,
			Lottery_Selection_Date__c = null,
			Lottery_Selection_Type__c = null,
			Lottery_Selection_Number__c = null,
			Lottery_WL_No__c = null,
			Grade_Applying_For__c = '09',
			Site_of_Interest__c = 'ASU PREP POLYTECHNIC',
			Term_Applying_for__c = 'Fall',
			Year_applying_for__c = '2033',
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstNameWL',
			Student_Last_Name__c = 'TestingStudentLastNameWL',
			Application_Status__c = 'Pending Final Review'
		);
		insert app;
		return app.Id;
	}

	public static Id createApplication(
		String studentEmail,
		String parentEmail,
		String applicationStatus
	) {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Student_email_address__c = studentEmail,
			Parent_Legal_Guardian_Email_Address__c = parentEmail,
			Application_Status__c = applicationStatus
		);
		insert app;
		return app.Id;
	}

	public static Id createApplication(Id contactId, String applicationStatus) {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Application_Status__c = applicationStatus,
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Contact__c = contactId
		);
		insert app;
		return app.Id;
	}

	public static Id createApplication(
		Id contactId,
		String applicationStatus,
		Id ownerId
	) {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Application_Status__c = applicationStatus,
			Contact__c = contactId,
			OwnerId = ownerId
		);
		insert app;
		return app.Id;
	}

	public static Id createPrepDigApp() {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Grade_Applying_For__c = '08',
			Site_of_Interest__c = 'ASU PREP DIGITAL'
		);
		insert app;
		return app.Id;
	}

	public static Id createImmersionApp() {
		Application__c app = new Application__c(
			Application_Form_Type__c = 'Full Time',
			Session_Assignment__c = 'Spring 1',
			Session_Start_Year__c = '2022',
			Student_First_Name__c = 'TestingStudentFirstName',
			Student_Last_Name__c = 'TestingStudentLastName',
			Grade_Applying_For__c = '08',
			Site_of_Interest__c = 'ASU PREP POLYTECHNIC'
		);
		insert app;
		return app.Id;
	}

	public static Id getApplication() {
		return [SELECT Id, Name FROM Application__c][0].Id;
	}

	public static void createDataCaseInsert() {
		Account asu = new Account(
			Name = 'ASU',
			Account_Status__c = 'Active',
			Phone = '7869658745',
			BillingStreet = '1233 Main St.',
			BillingCity = 'Miami',
			BillingState = 'FL',
			BillingCountry = 'United States',
			BillingPostalCode = '33332'
		);
		insert asu;
		hed__Term__c term2214 = new hed__Term__c(
			Name = '2021 Summer',
			Code__c = '2214',
			hed__Account__c = asu.Id
		);
		insert term2214;
		hed__Course__c course = new hed__Course__c(
			Name = 'Testing Course',
			hed__Account__c = asu.Id
		);
		insert course;
		hed__Course_Offering__c co44945 = new hed__Course_Offering__c(
			Name = '44945',
			hed__Course__c = course.Id,
			hed__Term__c = term2214.Id
		);
		hed__Course_Offering__c co40960 = new hed__Course_Offering__c(
			Name = '40960',
			hed__Course__c = course.Id,
			hed__Term__c = term2214.Id
		);
		insert new List<hed__Course_Offering__c>{ co44945, co40960 };
		Contact contactDirja = new Contact(
			FirstName = 'David',
			LastName = 'Dirja',
			AccountId = asu.Id
		);
		Contact contactEdwards = new Contact(
			FirstName = 'Aidan',
			LastName = 'Edwards',
			AccountId = asu.Id
		);
		insert new List<Contact>{ contactDirja, contactEdwards };
	}

	public static void createHouseholdAccount(Id ownerUserId) {
		Account hhAccount = new Account(
			Name = 'James Doe (HA)',
			RecordTypeId = HA_ACCOUNT_RT,
			BillingStreet = 'Test Street',
			OwnerId = ownerUserId
		);

		insert hhAccount;
	}

	public static Id createAccount(String pName, String pPhone) {
		Account newAccount = new Account(
			Name = pName,
			Phone = pPhone,
			BillingStreet = '1233 Main St.',
			BillingCity = 'Miami',
			BillingState = 'FL',
			BillingCountry = 'United States',
			BillingPostalCode = '33332'
		);

		insert newAccount;
		return newAccount.Id;
	}

	public static Id createAccount(String pName, String pPhone, Id contactId) {
		Account newAccount = new Account(
			Name = pName,
			Phone = pPhone,
			BillingStreet = 'Test Street',
			Primary_Contact__c = contactId
		);

		insert newAccount;
		return newAccount.Id;
	}

	public static Account getAccount() {
		return [SELECT Id, Name FROM Account][0];
	}

	public static Id createContact(
		Id accId,
		String fName,
		String lName,
		String email
	) {
		Contact newContact = new Contact(
			FirstName = fName,
			LastName = lName,
			Email = email,
			AccountId = accId
		);
		insert newContact;
		return newContact.Id;
	}

	public static Id createContact(String fName, String lName, String email) {
		Id accountId = createAccount('Test Account', '123456789');
		Contact newContact = new Contact(
			FirstName = fName,
			LastName = lName,
			Email = email,
			AccountId = accountId,
			Birthdate = Date.newInstance(2012, 12, 10),
			MailingStreet = 'Test Street'
		);
		insert newContact;
		return newContact.Id;
	}

	public static Id getContact() {
		return [SELECT Id, Name FROM Contact][0].Id;
	}

	public static Id createUser(
		Boolean withRole,
		Id contId,
		String profileName,
		String pFirstName,
		String pLastName,
		String pUserName,
		String pEmail
	) {
		Id roleId, proffileId;

		if (withRole) {
			roleId = [
				SELECT Id
				FROM UserRole
				WHERE DeveloperName = 'Sales_Lead'
				LIMIT 1
			]
			.Id;
		}

		proffileId = [SELECT Id FROM Profile WHERE Name = :profileName].Id;

		User newUser = new User(
			FirstName = pFirstName,
			LastName = pLastName,
			Email = pEmail,
			Username = pUserName,
			CompanyName = 'TEST',
			Title = 'title',
			Alias = 'lbrandon',
			TimeZoneSidKey = 'America/Los_Angeles',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			IsActive = true,
			ContactId = contId,
			ProfileId = proffileId,
			UserRoleId = roleId
		);

		insert newUser;
		return newUser.Id;
	}

	public static Id createAdvisorUser(
		String fName,
		String lName,
		String email
	) {
		Id proffileId = [SELECT Id FROM Profile WHERE Name = 'B2C Rep'].Id;

		User newUser = new User(
			FirstName = fName,
			LastName = lName,
			Email = email,
			Username = email,
			CompanyName = 'TEST',
			Title = 'title',
			Alias = 'advisor',
			TimeZoneSidKey = 'America/Los_Angeles',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			IsActive = true,
			ProfileId = proffileId
		);

		insert newUser;

		Id advisorGroup = [SELECT Id FROM Group WHERE Name LIKE '%Advisors%']
		.Id;

		GroupMember groupMember = new GroupMember();
		groupMember.GroupId = advisorGroup;
		groupMember.UserOrGroupId = newUser.Id;

		insert groupMember;

		return newUser.Id;
	}

	public static Id getUser(String pUserName) {
		return [SELECT Id FROM User WHERE Username = :pUserName].Id;
	}

	public static List<School__c> createSchool(
		Id applicationId,
		Integer count,
		Boolean onlyOneCurrent
	) {
		List<School__c> schools = new List<School__c>();
		Boolean currentSchoolValue;

		for (Integer i = 0; i < count; i++) {
			if (onlyOneCurrent == true) {
				if (i == 0) {
					currentSchoolValue = true;
				} else {
					currentSchoolValue = false;
				}
			} else {
				currentSchoolValue = true;
			}

			School__c newSchool = new School__c(
				Name = 'Miami Dade School ' + String.valueOf(i + 1),
				School_City__c = 'Miami',
				School_State__c = 'FL',
				Is_Current_School__c = currentSchoolValue,
				Student_Application__c = applicationId
			);

			schools.add(newSchool);
		}

		insert schools;
		return schools;
	}

	public static void createLead(String fName, String lName, String email) {
		Lead newLead = new Lead(
			FirstName = fName,
			LastName = lName,
			Email = email,
			isConverted = false,
			RecordTypeId = B2C_LEAD_RT,
			Company = 'Household'
		);
		insert newLead;
	}

	public static void createCourse(Id applicationId, Id schoolId) {
		Self_Report_Transcript__c course = new Self_Report_Transcript__c(
			Course_Name__c = 'USA Advanced History',
			Course_Status__c = 'Completed',
			Course_Type__c = 'Social Studies',
			Honors__c = 'Yes',
			Letter_Grade_Earned__c = 'A+',
			Credits_Earned__c = 5,
			School__c = schoolId,
			Student_Application__c = applicationId
		);
		insert course;
	}
}
