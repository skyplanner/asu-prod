/*
 * @author				Yaneivys Gutierrez
 * @date				05/03/2022
 * @description			Lottery Setup Metadata Type Manager Class
 * @group
 * @last modified on	05/03/2022
 * @last modified by	05/03/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/03/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotterySetupMDT {
	private static SpLotterySetupMDT instance = null;
	public final Lottery_Setup__mdt LOT_MDT_SETTING;

	public SpLotterySetupMDT() {
		LOT_MDT_SETTING = getDefaultSettings();
	}

	public static SpLotterySetupMDT getInstance() {
		if (instance == null) {
			instance = new SpLotterySetupMDT();
		}
		return instance;
	}

	public Lottery_Setup__mdt getSettings(String criteria) {
		Lottery_Setup__mdt result = null;
		List<Lottery_Setup__mdt> setsMdt = [
			SELECT Application_Status__c, LotteryBatch_CronExp__c
			FROM Lottery_Setup__mdt
			WHERE Criteria__c = :criteria
		];
		if (!setsMdt.isEmpty()) {
			result = setsMdt[0];
		}
		return result;
	}

	private Lottery_Setup__mdt getDefaultSettings() {
		return getSettings(null);
	}
}
