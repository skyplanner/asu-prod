/**
 * @author				Yaneivys Gutierrez
 * @date				05/13/2022
 * @description			Test class for SpLotteryTriggerUtility
 * group
 * @last modified on	05/13/2022
 * @last modified by	05/13/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/13/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpLotteryTriggerUtilityTest {
	@TestSetup
	static void makeData() {
		Id lId = SP_TestDataFactory.createLottery(true);
		SP_TestDataFactory.createSelectedApplicationWithLottery(lId);
		SP_TestDataFactory.createWlApplicationWithLottery(lId);
	}

	@isTest
	static void handleBeforeUpdateWrongUserTest() {
		Lottery__c l = [SELECT Id FROM Lottery__c LIMIT 1];
		l.Seats__c = 25;
		User u = [
			SELECT Id
			FROM User
			WHERE Id != :UserInfo.getUserId()
			LIMIT 1
		];

		try {
			Test.startTest();

			System.runAs(u) {
				update l;
			}

			Test.stopTest();
		} catch (Exception ex) {
			System.assert(
				ex.getMessage().contains(System.Label.SP_VAL_WRONG_USER),
				'Wrong Seats Update process'
			);
		}
	}

	@isTest
	static void handleBeforeUpdateNullTest() {
		Lottery__c l = [SELECT Id FROM Lottery__c LIMIT 1];
		l.Seats__c = null;

		try {
			Test.startTest();

			update l;

			Test.stopTest();
		} catch (Exception ex) {
			System.assert(
				ex.getMessage().contains(System.Label.SP_VAL_NULL),
				'Wrong Seats Update process'
			);
		}
	}

	@isTest
	static void handleBeforeUpdateUnderflowTest() {
		Lottery__c l = [SELECT Id FROM Lottery__c LIMIT 1];
		l.Seats__c = 1;

		try {
			Test.startTest();

			update l;

			Test.stopTest();
		} catch (Exception ex) {
			System.assert(
				ex.getMessage().contains(System.Label.SP_VAL_UNDERFLOW),
				'Wrong Seats Update process'
			);
		}
	}

	@isTest
	static void handleBeforeUpdateValidTest() {
		Lottery__c l = [SELECT Id FROM Lottery__c LIMIT 1];
		l.Seats__c = 9;

		Test.startTest();

		update l;

		Test.stopTest();

		l = [SELECT Id, Available_Seats__c FROM Lottery__c LIMIT 1];

		System.assertEquals(
			8,
			l.Available_Seats__c,
			'Wrong Seats Update process'
		);
	}
}
