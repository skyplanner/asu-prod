/**
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Lottery Process Manager class
 * group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpLotteryProcessManager {
	public static SpMsgResultWrap runLottery(SpLotterySetupWrap setup) {
		SpMsgResultWrap result = new SpMsgResultWrap();
		result.title = 'Error';
		result.variant = 'error';

		if (
			String.isBlank(setup.grade) ||
			String.isBlank(setup.site) ||
			String.isBlank(setup.term) ||
			setup.year == null ||
			setup.seats == null
		) {
			result.msg = System.Label.SP_LOTTERY_DATA_ERROR;
			return result;
		}

		List<Lottery__c> lotteries = SpLotteryManager.getLotteriesBySetup(
			setup
		);
		if (lotteries != null && lotteries.size() > 0) {
			result.title = 'Warning';
			result.variant = 'warning';
			result.msg = String.format(
				System.Label.SP_LOTTERY_PRE_CREATED,
				new List<String>{ lotteries[0].Name }
			);
			return result;
		}

		List<Application__c> apps = SpApplicationManager.getAppsBySetup(setup);
		if (apps == null || apps.size() <= 0) {
			result.title = 'Warning';
			result.variant = 'warning';
			result.msg = System.Label.SP_LOTTERY_NO_DATA;
			return result;
		}

		Lottery__c l = runLotteryProcess(setup, apps);
		if (l == null) {
			result.msg = System.Label.SP_LOTTERY_NO_CREATED;
			return result;
		}

		result.title = 'Success';
		result.variant = 'success';
		result.msg = System.Label.SP_LOTTERY_CREATED;
		result.data = new SpResultData();
		result.data.lotteryId = l.Id;
		return result;
	}

	private static Lottery__c runLotteryProcess(
		SpLotterySetupWrap setup,
		List<Application__c> apps
	) {
		if (setup.seats <= 0) {
			return null;
		}

		List<Application__c> appsToUpdate = new List<Application__c>();
		Lottery__c l = SpLotteryManager.createLottery(setup);

		Integer seats = Integer.valueOf(setup.seats);
		Integer appSize = apps.size();
		Integer minSelectedNo = seats < appSize ? seats : appSize;
		Integer order = 1;

		while (order <= minSelectedNo) {
			Integer appCurrentSize = apps.size();
			Integer i = Integer.valueof((Math.random() * (appCurrentSize - 1)));

			Application__c app = apps[i];
			updateAppSel(
				app,
				l.Id,
				l.Run_Date__c,
				SpApplicationConst.SEL_TYPE_AUTOMATIC,
				order
			);
			appsToUpdate.add(app);
			apps.remove(i);

			order++;
		}

		l.Available_Seats__c = seats > appsToUpdate.size()
			? (seats - appsToUpdate.size())
			: 0;

		order = 1;
		for (Application__c app : apps) {
			updateAppWL(app, l.Id, order);
			appsToUpdate.add(app);

			order++;
		}

		update l;
		update appsToUpdate;
		return l;
	}

	public static void updateAppSel(
		Application__c app,
		Id lId,
		Datetime runDate,
		String selectionType,
		Integer order
	) {
		app.Lottery__c = lId;
		app.Lottery_Selection_Date__c = runDate;
		app.Lottery_Selection_Type__c = selectionType;
		app.Lottery_Status__c = SpApplicationConst.LOT_SELECTED;
		app.Lottery_Selection_Number__c = order;
		app.Lottery_WL_No__c = null;
		app.Application_Status__c = SpApplicationConst.STATUS_HOLD;
	}

	public static void updateAppWL(Application__c app, Id lId, Integer order) {
		app.Lottery__c = lId;
		app.Lottery_Status__c = SpApplicationConst.LOT_WAITING_LIST;
		app.Lottery_Selection_Number__c = null;
		app.Lottery_WL_No__c = order;
		app.Application_Status__c = SpApplicationConst.STATUS_WAITLISTED;
	}

	public static SpMsgResultWrap inactivateProcess(Id lId) {
		SpMsgResultWrap result = new SpMsgResultWrap();
		result.title = 'Error';
		result.variant = 'error';

		if (lId == null) {
			result.msg = System.Label.SP_LOTTERY_INVALID_ID_INACTIVATION;
			return result;
		}

		Boolean inactivated = SpLotteryManager.inactivateLottery(lId);

		if (!inactivated) {
			result.msg = System.Label.SP_LOTTERY_INACTIVATION_ISSUE;
			return result;
		}

		result.title = 'Success';
		result.variant = 'success';
		result.msg = System.Label.SP_LOTTERY_INACTIVATION_SUCCESS;
		return result;
	}

	public static Lottery__c addAppsToLottery(Id lId, List<Id> appIds) {
		Lottery__c l = SpLotteryManager.getLottery(lId);
		List<Application__c> apps = SpApplicationManager.getWLApps(lId, appIds);
		List<Application__c> selectedApps = SpApplicationManager.getSelectedApps(
			lId
		);

		if (l != null && apps != null && l.Available_Seats__c >= apps.size()) {
			Integer order = selectedApps.size() + 1;
			for (Application__c app : apps) {
				updateAppSel(
					app,
					l.Id,
					Datetime.now(),
					SpApplicationConst.SEL_TYPE_MANUAL,
					order
				);
				order++;
			}
			l.Available_Seats__c = l.Available_Seats__c > apps.size()
				? (l.Available_Seats__c - apps.size())
				: 0;
			update l;
			update apps;
			reOrderWaitingList(l);
		}
		return l;
	}

	private static void reOrderWaitingList(Lottery__c l) {
		List<Application__c> apps = SpApplicationManager.getWLApps(l.Id);

		Integer order = 1;
		for (Application__c app : apps) {
			app.Lottery_Selection_Number__c = null;
			app.Lottery_WL_No__c = order;

			order++;
		}

		if (apps.size() > 0) {
			update apps;
		}
	}

	public static void reCalculateSelectionNo(Set<Id> lotIds) {
		List<Application__c> toUpdate = new List<Application__c>();
		List<Lottery__c> lotsToUpdate = new List<Lottery__c>();

		Map<Id, List<Application__c>> appsByLot = SpApplicationManager.getSelAppsByLot(
			lotIds
		);
		Map<Id, Lottery__c> lotteries = new Map<Id, Lottery__c>(
			[
				SELECT Id, Seats__c, Available_Seats__c
				FROM Lottery__c
				WHERE Id IN :appsByLot.keySet()
			]
		);

		for (Id lId : appsByLot.keySet()) {
			Integer order = 1;
			for (Application__c app : appsByLot.get(lId)) {
				app.Lottery_Selection_Number__c = order;
				toUpdate.add(app);
				order++;
			}

			order--;
			Lottery__c l = lotteries.get(lId);
			l.Available_Seats__c = l.Seats__c - order;
			lotsToUpdate.add(l);
		}

		if (toUpdate.size() > 0) {
			update toUpdate;
		}

		if (lotsToUpdate.size() > 0) {
			update lotsToUpdate;
		}
	}

	public static void deleteLottery(Id lId) {
		List<Application__c> apps = new List<Application__c>();
		for (Application__c app : [
			SELECT Id
			FROM Application__c
			WHERE Lottery__c = :lId
		]) {
			app.Lottery__c = null;
			app.Lottery_Selection_Date__c = null;
			app.Lottery_Selection_Type__c = null;
			app.Lottery_Status__c = null;
			app.Lottery_Selection_Number__c = null;
			app.Lottery_WL_No__c = null;
			app.Application_Status__c = SpApplicationConst.STATUS_PENDING_FR;
			apps.add(app);
		}

		if (apps.size() > 0) {
			update apps;
		}
		delete [SELECT Id FROM Lottery__c WHERE Id = :lId];
	}
}
