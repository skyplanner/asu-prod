/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   04-05-2021   dmorales   Initial Version
**/
@isTest
private class  ApplicationAssignmentByHATest {
    @TestSetup
    static void makeData(){
        Id userId = SP_TestDataFactory.createAdvisorUser('TestAdvisorFirstName', 'TestAdvisorLastName', 'testadvisor@test.com');
    }
    @IsTest
    static void testAssignmentByHA() {

        User userAdvisor = [SELECT Id from User WHERE UserName = 'testadvisor@test.com'];

        Id accountId = SP_TestDataFactory.createAccount('AccountTest', '123456789');
        Id contactId = SP_TestDataFactory.createContact(accountId, 'FirstNameTest', 'LastNameTest', 'contactexist@test.com');
        Id contactIdOther = SP_TestDataFactory.createContact(accountId, 'FirstNameTest Other', 'LastNameTest Other', 'contactexistother@test.com');
        Id appIdOther = SP_TestDataFactory.createApplication(contactIdOther, 'Household Information Completed', userAdvisor.Id);
        Id appId = SP_TestDataFactory.createApplication(contactId, 'Household Information Completed');
        ApplicationAssignmentByHA.HouseholdAccountInfo info = new ApplicationAssignmentByHA.HouseholdAccountInfo('Advisors', appId);
        List<ApplicationAssignmentByHA.HouseholdAccountInfo> listInfo = new List<ApplicationAssignmentByHA.HouseholdAccountInfo>();
           listInfo.add(info);
        Test.startTest();
         List<Id> idApps =  ApplicationAssignmentByHA.assignByHA(listInfo);
        Test.stopTest();
        
        System.assert(idApps == null, 'No Assign By HA');
    }

    @IsTest
    static void testAssignmentbyRR() {

        Id accountId = SP_TestDataFactory.createAccount('AccountTest', '123456789');
        Id contactId = SP_TestDataFactory.createContact(accountId, 'FirstNameTest', 'LastNameTest', 'contactexist@test.com');     
        Id appId = SP_TestDataFactory.createApplication(contactId, 'Household Information Completed');
        ApplicationAssignmentByHA.HouseholdAccountInfo info = new ApplicationAssignmentByHA.HouseholdAccountInfo('Advisors', appId);
        List<ApplicationAssignmentByHA.HouseholdAccountInfo> listInfo = new List<ApplicationAssignmentByHA.HouseholdAccountInfo>();
        listInfo.add(info);
        Test.startTest();
         List<Id> idApps =  ApplicationAssignmentByHA.assignByHA(listInfo);
        Test.stopTest();
        
        System.assert(idApps != null, 'Assign By HA');
    }
}