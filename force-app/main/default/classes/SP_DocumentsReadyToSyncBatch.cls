/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 10-15-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   09-14-2020   dmorales   Initial Version
**/
global without sharing class SP_DocumentsReadyToSyncBatch implements Database.Batchable<sObject>{

    global static final Integer BATCH_EXECUTION_SCOPE = 200;

    global SP_DocumentsReadyToSyncBatch() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Name ' +
                       'FROM Application__c ' +
					   'WHERE Sync_Status__c = \'Success\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Application__c> data) {
        SP_ApplicationsSyncManagement.handleUpdateRelatedContent(data,TRUE,FALSE,TRUE);             
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('SP_DocumentsReadyToSyncBatch -> finish');
    }
}