/**
 * @description       : 
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : @svelazquez
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   04-02-2021   @svelazquez   Initial Version
**/
@isTest
private class AccountTriggerUtilityTest {
    @TestSetup
    static void makeData(){
        SP_TestDataFactory.createUser(true, 
                                        null, 
                                        'System Administrator',
                                        'Leslie',
                                        'Brandon',
                                        'leslie.brandon@test.com',
                                        'leslie.brandon@test.com');
        SP_TestDataFactory.createUser(false, 
                                        null, 
                                        'System Administrator',
                                        'Marlon',
                                        'Brandon',
                                        'marlon.brandon@test.com',
                                        'marlon.brandon@test.com');
    }
    
    @isTest static void checkHouseholdAccountOwnershipOK(){
        Test.startTest();
        Id userId = SP_TestDataFactory.getUser('leslie.brandon@test.com');
        SP_TestDataFactory.createHouseholdAccount(userId);
        Test.stopTest();

        system.assertEquals(1, [SELECT count() FROM Account],
            'There is no Household Account inserted');
    }

    @isTest static void checkHouseholdAccountOwnershipError(){
        DmlException expectedException;
        Test.startTest();
        try{
            Id userId = SP_TestDataFactory.getUser('marlon.brandon@test.com');
            SP_TestDataFactory.createHouseholdAccount(userId);
        }
        catch (DmlException dmx){
            expectedException = dmx;
        }
        Test.stopTest();

        system.assertNotEquals(null, expectedException,
            'The Household Account must have a Role Assigned');
        system.assertEquals(0, [SELECT count() FROM Account],
            'The database should be unchanged');
    }
}