/*
 * @author				Yaneivys Gutierrez
 * @date				05/04/2022
 * @description			Lottery LWC Cntrl
 * @group
 * @last modified on	05/04/2022
 * @last modified by	05/04/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/04/2022		Yaneivys Gutierrez		Initial Version
 */
public with sharing class SpLotteryViewCntrl {
	@AuraEnabled
	public static SpLotteryViewWrap fetchLotteryData(Id lId) {
		try {
			return SpApplicationManager.getLotteryData(lId);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static SpLotteryViewWrap addAppsToLottery(Id lId, List<Id> apps) {
		try {
			Lottery__c l = SpLotteryProcessManager.addAppsToLottery(lId, apps);
			return fetchLotteryData(l?.Id);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static SpLotteryViewWrap saveLottery(Lottery__c l) {
		try {
			update l;
			return fetchLotteryData(l?.Id);
		} catch (DmlException e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}
