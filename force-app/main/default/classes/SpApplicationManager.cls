/**
 * @author				Yaneivys Gutierrez
 * @date				05/02/2022
 * @description			Student Application Manager class
 * group
 * @last modified on	05/02/2022
 * @last modified by	05/02/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/02/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpApplicationManager {
	public static List<Application__c> getWLApps(Id lId, List<Id> appIds) {
		return [
			SELECT Id
			FROM Application__c
			WHERE
				Lottery__c = :lId
				AND Lottery_Status__c = :SpApplicationConst.LOT_WAITING_LIST
				AND Id IN :appIds
			ORDER BY Lottery_WL_No__c ASC
		];
	}

	public static List<Application__c> getWLApps(Id lId) {
		return [
			SELECT Id
			FROM Application__c
			WHERE
				Lottery__c = :lId
				AND Lottery_Status__c = :SpApplicationConst.LOT_WAITING_LIST
			ORDER BY Lottery_WL_No__c ASC
		];
	}

	public static List<Application__c> getPosiblesWLByLot(
		List<Lottery__c> lotteries
	) {
		Lottery_Setup__mdt setMdt = SpLotterySetupMDT.getInstance()
			.LOT_MDT_SETTING;
		List<String> appStatus = setMdt.Application_Status__c.split(',');

		Set<String> grades = new Set<String>();
		Set<String> sites = new Set<String>();
		Set<String> terms = new Set<String>();
		Set<String> years = new Set<String>();

		for (Lottery__c l : lotteries) {
			grades.add(l.Grade_Applying_For__c);
			sites.add(l.Site_of_Interest__c);
			terms.add(l.Term_Applying_for__c);
			years.add(l.Year_applying_for__c);
		}

		return [
			SELECT
				Id,
				Lottery__c,
				Grade_Applying_For__c,
				Site_of_Interest__c,
				Term_Applying_for__c,
				Year_applying_for__c
			FROM Application__c
			WHERE
				((Application_Status__c IN :appStatus
				AND Lottery_Status__c = :SpApplicationConst.LOT_AVAILABLE)
				OR Lottery_Status__c = :SpApplicationConst.LOT_WAITING_LIST)
				AND Grade_Applying_For__c IN :grades
				AND Site_of_Interest__c IN :sites
				AND Term_Applying_for__c IN :terms
				AND Year_applying_for__c IN :years
			ORDER BY Pending_Final_Review_Date__c ASC
		];
	}

	public static Map<Id, List<Application__c>> getSelAppsByLot(Set<Id> lIds) {
		Map<Id, List<Application__c>> apps = new Map<Id, List<Application__c>>();

		for (Lottery__c l : [
			SELECT
				Id,
				(
					SELECT Id, Name
					FROM Student_Applications__r
					WHERE Lottery_Status__c = :SpApplicationConst.LOT_SELECTED
					ORDER BY Lottery_Selection_Number__c ASC
				)
			FROM Lottery__c
			WHERE Id IN :lIds AND Active__c = TRUE
		]) {
			apps.put(l.Id, l.Student_Applications__r);
		}

		return apps;
	}

	public static List<Application__c> getSelectedApps(Id lId) {
		return [
			SELECT Id
			FROM Application__c
			WHERE
				Lottery__c = :lId
				AND Lottery_Status__c = :SpApplicationConst.LOT_SELECTED
			ORDER BY Lottery_Selection_Number__c ASC
		];
	}

	public static List<Application__c> getAppsBySetup(
		SpLotterySetupWrap setup
	) {
		Lottery_Setup__mdt setMdt = SpLotterySetupMDT.getInstance()
			.LOT_MDT_SETTING;
		List<String> appStatus = setMdt.Application_Status__c.split(',');

		return [
			SELECT Id
			FROM Application__c
			WHERE
				Application_Status__c IN :appStatus
				AND Lottery__c = NULL
				AND Grade_Applying_For__c = :setup.grade
				AND Site_of_Interest__c = :setup.site
				AND Term_Applying_for__c = :setup.term
				AND Year_applying_for__c = :setup.year
			ORDER BY Pending_Final_Review_Date__c ASC
		];
	}

	public static SpLotteryViewWrap getLotteryData(Id lId) {
		SpLotteryViewWrap result = new SpLotteryViewWrap();

		Set<Id> users = SpLotteryEditPermissionMDT.getUserIds();
		result.canEdit = users.contains(UserInfo.getUserId());
		result.lottery = SpLotteryManager.getLottery(lId);
		result.selectedApps = [
			SELECT
				Id,
				Name,
				Student_Last_Name__c,
				Student_First_Name__c,
				Lottery_Selection_Number__c,
				Application_Status__c,
				Term_Applying_for__c,
				Grade_Applying_For__c,
				Owner.Alias,
				CreatedDate,
				Lottery_Status__c,
				Pending_Final_Review_Date__c
			FROM Application__c
			WHERE
				Lottery__c = :lId
				AND Lottery_Status__c = :SpApplicationConst.LOT_SELECTED
			ORDER BY Lottery_Selection_Number__c ASC
		];
		result.waitingList = [
			SELECT
				Id,
				Name,
				Student_Last_Name__c,
				Student_First_Name__c,
				Lottery_WL_No__c,
				Application_Status__c,
				Term_Applying_for__c,
				Grade_Applying_For__c,
				Owner.Alias,
				CreatedDate,
				Lottery_Status__c,
				Pending_Final_Review_Date__c
			FROM Application__c
			WHERE
				Lottery__c = :lId
				AND Lottery_Status__c = :SpApplicationConst.LOT_WAITING_LIST
			ORDER BY Lottery_WL_No__c ASC
		];

		return result;
	}
}
