/**
 * @author				Yaneivys Gutierrez
 * @date				05/10/2022
 * @description			Test class for SpSchoolNumberMDT
 * group
 * @last modified on	05/10/2022
 * @last modified by	05/10/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/10/2022		Yaneivys Gutierrez		Initial Version
 */
@IsTest
private class SpSchoolNumberMDTTest {
	@TestSetup
	static void makeData() {
		SP_TestDataFactory.createApplicationWithoutDocument();
	}

	@isTest
	static void schoolNoUpdateTest() {
		Application__c app = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			LIMIT 1
		];
		app.Grade_Applying_For__c = '09';
		app.Site_of_Interest__c = 'ASU PREP POLYTECHNIC';

		Test.startTest();

		update app;

		Test.stopTest();

		app = [SELECT Id, School_Number__c FROM Application__c LIMIT 1];

		System.assertEquals(
			SpSchoolNumberMDT.getInstance()
				.getSettings('09', 'ASU PREP POLYTECHNIC'),
			app.School_Number__c,
			'Wrong SchoolNumber Assigned'
		);
	}

	@isTest
	static void schoolNoUpdateNullMapTest() {
		Application__c app = [
			SELECT Id, Student_First_Name__c
			FROM Application__c
			LIMIT 1
		];
		app.Grade_Applying_For__c = '09';
		app.Site_of_Interest__c = 'ASU PREP POLYTECHNIC';

		SpSchoolNumberMDT mdt = SpSchoolNumberMDT.getInstance();
		mdt.schoolNoMap = null;

		Test.startTest();

		update app;

		Test.stopTest();

		app = [SELECT Id, School_Number__c FROM Application__c LIMIT 1];

		System.assertEquals(
			SpSchoolNumberMDT.getInstance()
				.getSettings('09', 'ASU PREP POLYTECHNIC'),
			app.School_Number__c,
			'Wrong SchoolNumber Assigned'
		);
	}

	@isTest
	static void schoolNoUpdateNullParamsTest() {
		Test.startTest();

		SpSchoolNumberMDT.getInstance().getSettings('09', '');

		Test.stopTest();

		System.assertEquals(
			SpSchoolNumberMDT.getInstance().getSettings('09', ''),
			null,
			'Wrong SchoolNumber Assigned'
		);
	}
}
