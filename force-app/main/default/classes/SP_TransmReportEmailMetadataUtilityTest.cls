/**
 * @author       Yaneivys Gutierrez
 * @date         03/29/2021
 * @description  Test Class for SP_TransmReportEmailMetadataUtility
 */
@isTest
private class SP_TransmReportEmailMetadataUtilityTest {
    @isTest
    static void testGetTransmissionReportEmails() {
        List<String> emails = SP_TransmReportEmailMetadataUtility.getTransmissionReportEmails();
        System.assert(emails.size() > 0, 'At least one Email it\'s necessary');
    }
}
