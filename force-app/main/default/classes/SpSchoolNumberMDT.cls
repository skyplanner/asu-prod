/*
 * @author				Yaneivys Gutierrez
 * @date				05/10/2022
 * @description			School Number Metadata Type Manager Class
 * @group
 * @last modified on	05/10/2022
 * @last modified by	05/10/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/10/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpSchoolNumberMDT {
	private static SpSchoolNumberMDT instance = null;
	public List<School_Number_mapping__mdt> schoolNoMap;

	public SpSchoolNumberMDT() {
		schoolNoMap = getMapping();
	}

	public static SpSchoolNumberMDT getInstance() {
		if (instance == null) {
			instance = new SpSchoolNumberMDT();
		}
		return instance;
	}

	public String getSettings(String grade, String site) {
		if (String.isBlank(grade) || String.isBlank(site)) {
			return null;
		}

		if (schoolNoMap == null) {
			schoolNoMap = getMapping();
		}

		String result;

		for (School_Number_mapping__mdt mdt : schoolNoMap) {
			if (
				mdt.Grade_Applying_for__c == grade &&
				mdt.Site_of_Interest__c == site
			) {
				result = mdt.School_Number__c;
				break;
			}
		}

		return result;
	}

	private List<School_Number_mapping__mdt> getMapping() {
		return [
			SELECT
				Id,
				Grade_Applying_for__c,
				Site_of_Interest__c,
				School_Number__c
			FROM School_Number_mapping__mdt
		];
	}
}
