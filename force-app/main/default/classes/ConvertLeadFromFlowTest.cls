/**
 * @description       : 
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : @svelazquez
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   04-03-2021   @svelazquez   Initial Version
**/
@isTest
private class ConvertLeadFromFlowTest {
    @TestSetup
    static void makeData(){
        SP_TestDataFactory.createLead('Jhon', 'Doe', 'jdoe@test.com');
        SP_TestDataFactory.createAccount('Juan Perez', '(102) 1234566');
    }

    @isTest static void convertLeadTestOK(){
        Account acc = SP_TestDataFactory.getAccount();
        Contact contactToConvert =  new Contact(
                                        FirstName = 'Jhon',
                                        LastName = 'Doe',
                                        Email = 'jdoe@test.com',
                                        AccountId = acc.Id);

        Test.startTest();
        List<Id> result = ConvertLeadFromFlow.convertLead(new List<Contact>{contactToConvert});
        Test.stopTest();

        System.assertNotEquals(null, result, 'The lead was not converted');
    }

    @isTest static void convertLeadTestNotExists(){
        Account acc = SP_TestDataFactory.getAccount();
        Contact contactToConvert =  new Contact(
                                        FirstName = 'Jhonny',
                                        LastName = 'Dep',
                                        Email = 'jdoe@test.com',
                                        AccountId = acc.Id);

        Test.startTest();
        List<Id> result = ConvertLeadFromFlow.convertLead(new List<Contact>{contactToConvert});
        Test.stopTest();

        System.assertEquals(null, result, 'The lead exists');
    }
}