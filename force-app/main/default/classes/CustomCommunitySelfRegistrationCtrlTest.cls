/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 04-05-2021
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   04-02-2021   dmorales   Initial Version
**/
@isTest
private with sharing class CustomCommunitySelfRegistrationCtrlTest {
    private static final Id STUDENT_RT = 
            Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Student').getRecordTypeId();
    private static final Id PARENT_RT = 
            Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Parent').getRecordTypeId();
    
    @isTest
    static void testConstructor(){
        
        Test.startTest();           
            CustomCommunitySelfRegistrationCtrl ctrl = new CustomCommunitySelfRegistrationCtrl();
        Test.stopTest();      
        System.assert(ctrl != null , 'Error trying to create Controller');
    }
    
    @isTest
    static void testCreateStudentWithSA(){
        Id appId = SP_TestDataFactory.createApplication('fakestudent@student.test', '', 'Student Information in Progress');
        Test.startTest();
            HACommunityUsersManagement.CommunityUser newUser = getUser('testLastName','testFirstName', 'fakestudent@student.test');
            CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();      
        List<Account> accts = [SELECT Id from Account WHERE Name LIKE '%testLastName%(HA)%'];
        List<Contact> contacts = [SELECT Id from Contact WHERE LastName = 'testLastName' AND RecordTypeId =: STUDENT_RT];
        List<Application__c> applContact = [SELECT Id, Contact__c from Application__c WHERE Id =: appId];

        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(accts.size() > 0 , 'No Account Created');
        System.assert(contacts.size() > 0 , 'No Contact Created');
        System.assert(applContact[0].Contact__c == contacts[0].Id  , 'No Application associated with Student Contact');
    }

    @isTest
    static void testCreateThrowCustomException() {
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('',19, 'fake', 'fakeStudent@fail.test', '');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage != '' , 'No CustomException Thrown');
    }

    @isTest
    static void testCreateEqualParentStudentEmail() {
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('StudentEqual',15, 'student', 'equalmail@test.com', 'equalmail@test.com');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
        List<Account> accts = [SELECT Id from Account WHERE Name LIKE '%parentName%(HA)%'];
        List<Contact> contactsStud = [SELECT Id from Contact WHERE LastName = 'StudentEqual' AND RecordTypeId =: STUDENT_RT];
        List<Contact> contactsParent = [SELECT Id from Contact WHERE LastName = 'parentLastName' AND RecordTypeId =: PARENT_RT];
        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(accts.size() > 0 , 'No Account Created');
        System.assert(contactsStud.size() > 0 , 'No Contact Student');
        System.assert(contactsParent.size() > 0 , 'No Contact Parent');
    }

    @isTest
    static void testCreateParentStudentEmail() {
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('StudentNoEqual',15, 'student', 'studentmail@test.com', 'parentmail@test.com');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
        List<Account> accts = [SELECT Id from Account WHERE Name LIKE '%parentName%(HA)%'];
        List<Contact> contactsStud = [SELECT Id FROM Contact WHERE LastName = 'StudentNoEqual' AND RecordTypeId =: STUDENT_RT];
        List<Contact> contactsParent = [SELECT Id FROM Contact WHERE Email = 'parentmail@test.com' AND RecordTypeId =: PARENT_RT];
        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(accts.size() > 0 , 'No Account Created');
        System.assert(contactsStud.size() > 0 , 'No Contact Student');
        System.assert(contactsParent.size() > 0 , 'No Contact Parent');
    }

    @isTest
    static void testCreateParentWithSA() {
        Id appId = SP_TestDataFactory.createApplication('', 'parent@test.com', 'Student Information in Progress');
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('Parent', 60, 'parent', 'parent@test.com', '');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
        List<Account> accts = [SELECT Id from Account WHERE Name LIKE '%Parent%(HA)%'];
        List<Contact> contactsParent = [SELECT Id from Contact WHERE LastName = 'Parent' AND RecordTypeId =: PARENT_RT];
        List<Application__c> applContact = [SELECT Id, Contact__c from Application__c WHERE Id =: appId];

        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(accts.size() > 0 , 'No Account Created');
        System.assert(contactsParent.size() > 0 , 'No Contact Parent');
        System.assert(applContact[0].Contact__c == contactsParent[0].Id  , 'No Application associated with Parent Contact');
    }

    @isTest
    static void testCreateEmancipated() {
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('Emancipated', 15, 'emancipated', 'emancipated@test.com', '');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
        List<Account> accts = [SELECT Id from Account WHERE Name LIKE '%Emancipated%(HA)%'];
        List<Contact> contactsEmancip = [SELECT Id from Contact WHERE LastName = 'Emancipated' 
          AND RecordTypeId =: STUDENT_RT AND Is_Emancipated__c = TRUE];

        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(accts.size() > 0 , 'No Account Created');
        System.assert(contactsEmancip.size() > 0 , 'No Contact Emancipated');
    }

    @isTest
    static void testCreateFromLead() {
        SP_TestDataFactory.createLead('LeadFirstNameTest', 'LeadLastNameTest', 'studentlead@test.com');
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('LeadLastNameTest', 'LeadFirstNameTest', 'studentlead@test.com');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
        List<Account> accts = [SELECT Id from Account WHERE Name LIKE '%LeadLastNameTest%(HA)%'];
        List<Contact> contactsLead = [SELECT Id from Contact WHERE LastName = 'LeadLastNameTest'  AND RecordTypeId =: STUDENT_RT ];
        List<Lead> leadConverted = [SELECT Id from Lead WHERE LastName = 'LeadLastNameTest'  AND isConverted = TRUE ];

        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(accts.size() > 0 , 'No Account Created');
        System.assert(contactsLead.size() > 0 , 'No Contact From Lead');
        System.assert(leadConverted.size() > 0 , 'No Lead Converted');
    }
    
    @isTest
    static void testCreateForExistingHA() {
        Id contactId = SP_TestDataFactory.createContact('ContactHAFirstNameTest', 'ContactHALastNameTest', 'contactexist@test.com');
        Id accountId = SP_TestDataFactory.createAccount('ContactHAFirstNameTest ContactHALastNameTest', '123456789', contactId);
        Test.startTest();
          HACommunityUsersManagement.CommunityUser newUser = getUser('ContactHALastNameTest', 'ContactHAFirstNameTest', 'contactexist@test.com');
          CustomCommunitySelfRegistrationCtrl.UserCreationResult result = CustomCommunitySelfRegistrationCtrl.registerUser(newUser);
        Test.stopTest();   
      
        List<Contact> contacts = [SELECT AccountId, Id from Contact WHERE LastName = 'ContactHALastNameTest' AND FirstName = 'ContactHAFirstNameTest' AND Email = 'contactexist@test.com'];
        
        System.assert(result != null , 'No Result Returned');
        System.assert(result.errorMessage == '' , 'Some exception was thrown');
        System.assert(contacts.size() > 0 , 'No Contact Created');
        System.assert(contacts[0].AccountId == accountId , 'Error updating AccountId to Contact');
    }

    @isTest
    static void testGetPicklist() {
        
        CustomCommunitySelfRegistrationCtrl.PicklistObjects picklObj = CustomCommunitySelfRegistrationCtrl.getPicklistObjects();     
        System.assert(picklObj != null , 'No Result Returned');
        System.assert(picklObj.countryPicklist.size() > 0 , 'No Country PIcklist');
        System.assert(picklObj.countryCodePicklist.size() > 0 , 'No Country Code Picklist Returned');
        System.assert(picklObj.statePicklist.size() > 0 , 'No State Picklist Returned');

    }

    static HACommunityUsersManagement.CommunityUser getUser(String lastN, Integer age, String type, String email, String parentEmail){
        HACommunityUsersManagement.CommunityUser newUser = new HACommunityUsersManagement.CommunityUser();
        newUser.firstName =  'test';
        newUser.lastName =  lastN;
        newUser.email =  email;
        newUser.confirmEmail =  email;
        newUser.password =  'test123';
        newUser.repeatedPassw =  'test123';
        newUser.phone =  '123456789';
        newUser.phoneArea =  '+1 United States of America';
        newUser.birthday =  '05/05/2000';
        newUser.userAge =  age;
        newUser.address =  'Address';
        newUser.city =  'City';
        newUser.state =  'State';
        newUser.zipCode =  '123456';
        newUser.country =  'United States of America';
        newUser.typeUser =  type;
        if(type == 'student' && age < 18){
            newUser.parentFirstName =  'parentName';
            newUser.parentLastName =  'parentLastName';
            newUser.parentEmail =  parentEmail;
        }
        
        newUser.optInMarketing =  true;
        newUser.optInEnrollment =  true;
        newUser.privacy =  true;
        return newUser;
    }
    static HACommunityUsersManagement.CommunityUser getUser(String lName, String fName, String email){
        HACommunityUsersManagement.CommunityUser newUser = new HACommunityUsersManagement.CommunityUser();
        newUser.firstName =  fName;
        newUser.lastName =  lName;
        newUser.email =  email;
        newUser.confirmEmail =  email;
        newUser.password =  'test123';
        newUser.repeatedPassw =  'test123';
        newUser.phone =  '123456789';
        newUser.phoneArea =  '+1 United States of America';
        newUser.birthday =  '05/05/2000';
        newUser.userAge =  20;
        newUser.address =  'Address';
        newUser.city =  'City';
        newUser.state =  'State';
        newUser.zipCode =  '123456';
        newUser.country =  'United States of America';
        newUser.typeUser =  'student';
        newUser.parentFirstName =  '';
        newUser.parentLastName =  '';
        newUser.parentEmail =  '';           
        newUser.optInMarketing =  true;
        newUser.optInEnrollment =  true;
        newUser.privacy =  true;
        return newUser;
    }
}