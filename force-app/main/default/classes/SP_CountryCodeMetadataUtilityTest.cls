/**
 * @author       Yaneivys Gutierrez
 * @date         03/29/2021
 * @description  Test Class for SP_CountryCodeMetadataUtility
 */
@isTest
private class SP_CountryCodeMetadataUtilityTest {
    @isTest
    static void testGetCodeByCountry() {
        Country_Code__mdt codeUS = SP_CountryCodeMetadataUtility.getCodeByCountry(
            'United States'
        );
        System.assert(codeUS.ALPHA_3__c == 'USA', 'Wrong country code');

        Country_Code__mdt codeNull = SP_CountryCodeMetadataUtility.getCodeByCountry(
            'Testing Country'
        );
        System.assert(codeNull == null, 'Wrong country code');
    }
}
