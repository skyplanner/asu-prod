/**
 * @description       : 
 * @group             : 
 * @last modified on  : 04-04-2021
 * @last modified by  : @svelazquez
 * Modifications Log 
 * Ver   Date         Author        Modification
 * 1.0   04-02-2021   @svelazquez   Initial Version
**/
@isTest
private class SchoolTriggerUtilityTest {
    @TestSetup
    static void makeData(){
        SP_TestDataFactory.createApplicationWithoutDocument();
    }
    @isTest static void handleCurrentSchoolSyncOK(){
        Test.startTest();
        Id appId = SP_TestDataFactory.getApplication();
        List<School__c> schoolList = SP_TestDataFactory.createSchool(appId, 2, true);
        Test.stopTest();

        System.assertEquals(1, [SELECT count() FROM Application__c], 
                            'No Applications created');

        System.assertEquals(2, schoolList.size(), 'No School created'); 
    }

    @isTest static void handleCurrentSchoolSyncError(){
        List<School__c> schoolList;
        DmlException expectedException;

        Test.startTest();
        try{
            Id appId = SP_TestDataFactory.getApplication();
            schoolList = SP_TestDataFactory.createSchool(appId, 2, false);
        }
        catch (DmlException dmx){
            expectedException = dmx;
        }
        Test.stopTest();
        
        System.assertNotEquals(null, expectedException,
        'More than one Current School is not allowed for the same Student Application');

        System.assertEquals(0, [SELECT count() FROM School__c], 'The database should be unchanged'); 
        
    }

    @isTest static void handlePreventCurrentSchoolDeletionOK(){
        List<School__c> schoolList;

        Test.startTest();
        Id appId = SP_TestDataFactory.getApplication();
        schoolList = SP_TestDataFactory.createSchool(appId, 2, true);
        delete schoolList[1];
        Test.stopTest();
        
        System.assertEquals(1, [SELECT count() FROM School__c], 
                                'No School Deleted');
        System.assertEquals(1, [SELECT count() FROM Application__c], 
                                'No Applications created');
        
    }

    @isTest static void handlePreventCurrentSchoolDeletionError(){
        DmlException expectedException;
        List<School__c> schoolList;

        Test.startTest();
        try{
            Id appId = SP_TestDataFactory.getApplication();
            schoolList = SP_TestDataFactory.createSchool(appId, 2, true);
            delete schoolList[0];
        }
        catch (DmlException dmx){
            expectedException = dmx;
        }
        Test.stopTest();
        
        System.assertNotEquals(null, expectedException,
        'The Current School associated with a Student Application can not be deleted ');

        System.assertNotEquals(null, schoolList, 'The database should be unchanged'); 
        
    }
}