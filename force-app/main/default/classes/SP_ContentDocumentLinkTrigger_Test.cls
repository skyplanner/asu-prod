/**
 * @File Name          : SP_ContentDocumentLinkTrigger_Test.cls
 * @Description        : 
 * @Author             : dmorales
 * @Group              : 
 * @Last Modified By   : dmorales
 * @Last Modified On   : 08-22-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   dmorales     Initial Version
**/
@isTest
private class SP_ContentDocumentLinkTrigger_Test {
    @TestSetup
    static void makeData(){
       SP_TestDataFactory.createApplicationWithDocument('Accepted');
       
    }

    @isTest
    static void testUpdateContentVersionSync() {
        List<ContentVersion> cvListBefore = [SELECT IsReadyToSync__c, Sent__c 
                                             FROM ContentVersion 
                                             WHERE Title = 'TestDocumentApplication'
                                             AND PathOnClient = 'testDocumentApplication.jpg'];

        System.assert(cvListBefore[0].IsReadyToSync__c);                                
        System.assert(!cvListBefore[0].Sent__c);         
    }
}