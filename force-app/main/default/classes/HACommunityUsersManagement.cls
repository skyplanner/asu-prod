/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 07-02-2021
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   02-05-2021   dmorales   Initial Version
 **/
public without sharing class HACommunityUsersManagement {

public final Id HA_ACCOUNT_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household Account').getRecordTypeId();
public final Id STUDENT_CONTACT_RT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Student').getRecordTypeId();
public final Id PARENT_CONTACT_RT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Parent').getRecordTypeId();
public final Id B2S_LEAD_RT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('B2S').getRecordTypeId();
public final Id B2C_LEAD_RT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('B2C').getRecordTypeId();

public CommunityUser user {set; get;}
public Boolean isStudentUnder18 {set;get;}

public Id ownerId {set; get;}

public HACommunityUsersManagement() {

}

public HACommunityUsersManagement(CommunityUser pUser) {
    user = pUser;
    HA_Management_Settings__c sett = HA_Management_Settings__c.getOrgDefaults();
    ownerId = (sett.HA_Owner__c != null && sett.HA_Owner__c != '') ? sett.HA_Owner__c : '0054p000003BreBAAS'; //asu@theskyplanner.com User if no Setting were found
    isStudentUnder18 = (pUser.typeUser == 'student' && pUser.userAge < 18);
}

public Account handleEntitiesCreation(){

	Account newAccount;
	String email = user.email;

	if(isStudentUnder18) {
		email = user.parentEmail;
	}

	newAccount = findOrNewHA(email);

	Id cont;
	Id contStudent;

	if(isStudentUnder18) {
		createContactAndAssociateSA(newAccount, user.email, true);

		if(user.email != user.parentEmail){
		  createContactAndAssociateSA(newAccount, email, false);
		}		
	}
	else{
		if(user.typeUser == 'parent') {
			createContactAndAssociateSA(newAccount, email, false);		
		}
		else{
			createContactAndAssociateSA(newAccount, email, true);
		}
    }
    
    return newAccount;
}

public void createContactAndAssociateSA(Account newAccount, String email, Boolean isStudent){
    Id cont = findOrNewContact(newAccount, email, isStudent);
	findStudentApplications(email, cont, isStudent);
}

private Account findOrNewHA(String email){

	List<Account> act = [SELECT Id, Primary_Contact__c,
	                     Primary_Contact__r.AccountId,
	                     Primary_Contact__r.Email
	                     FROM Account
	                     WHERE Primary_Contact__r.Email =: email                     /* AND RecordTypeId =: HA_ACCOUNT_RT */
	                                                      ORDER BY CreatedDate DESC];

	if(act.size() >= 1) {
		//Only considered the first Account that match
		return act[0];
	}
	//else NO Account exists
	Account newAccount = new Account();
	newAccount.RecordTypeId = HA_ACCOUNT_RT;
	newAccount.Name = isStudentUnder18 ? user.parentFirstName + ' ' + user.parentLastName  :
	                  user.firstName + ' ' + user.lastName;
	newAccount.Name += ' (HA) ';
	String countryCode = extractSubstringWithPattern(user.phoneArea, '^\\+?(\\d+)');
	String concatCountryCode = (countryCode != '')? '( +' + countryCode + ') ' : countryCode;
	newAccount.Phone = concatCountryCode + user.phone;
	newAccount.Account_Status__c  = 'Active';
	newAccount.BillingStreet  = user.address;
	newAccount.OwnerId = ownerId;
	try {
		insert newAccount;
		return newAccount;
	}
	catch(Exception ex) {
		throw new CommunityManagementCustomException('Error trying to create a new HA Account');
	}

}

public static String extractSubstringWithPattern(String word, String regex){
//^\+?(\d+)
    Pattern myPattern = Pattern.compile(regex); 
    Matcher myMatcher = myPattern.matcher(word); 
	
	String found = '';
	if(myMatcher.find()) {
	  found = myMatcher.group(1); 	
	}

	return found;
}

private Id findOrNewContact(Account account, String email, Boolean isStudent ){

	System.debug('findOrNewContact for ' + email + ' isStudent ' + isStudent + ' isUnder18 ' + isStudentUnder18);
	   System.debug('account.Primary_Contact__c ' + account.Primary_Contact__c);
	   System.debug('account.Id ' + account.Id);
	   System.debug('account.Primary_Contact__r.AccountId ' + account.Primary_Contact__r.AccountId);

	if (account.Primary_Contact__c != null && !(isStudentUnder18 && isStudent)) {
		if(account.Id != account.Primary_Contact__r.AccountId) {
			//Contact and Account are not associated correctly so I have to update the Contact.
			Contact contactToUpdate = new Contact();
			contactToUpdate.Id = account.Primary_Contact__c;
			contactToUpdate.AccountId = account.Id;
			/**If multiple Contact-Accounts is enable then the older Account remains in the relationship*/
			update contactToUpdate;
		}

		return account.Primary_Contact__c;
	}

	//else a new Account was created before and I will find if there are contacts to associate with this account
	Id contactId = findContact(email, account, isStudent);
	//If the Account was already created then update it's Primary Contact.
	if(account.Primary_Contact__c == null && !(isStudentUnder18 && isStudent)) {
		account.Primary_Contact__c = contactId;
		account.BillingStreet = user.address;
		account.BillingState = user.state;
		account.BillingCity = user.city;
		account.BillingPostalCode = user.zipCode;
		account.BillingCountry = user.country;
		update account;
	}
	return contactId;

}

private Id findContact(String email, Account act, Boolean isStudent){

	System.debug('Parametros ' + email + ' act ' + JSON.serializePretty(act) + ' isStudent ' + isStudent + ' isStdUnder18 ' + isStudentUnder18);
	String firstName = (isStudentUnder18) ? ((isStudent) ? (user.firstName) : (user.parentFirstName)) : user.firstName;
	String lastName =  (isStudentUnder18) ? ((isStudent) ? (user.lastName) : (user.parentLastName)) : user.lastName;
	String[] bdSplit = user.birthday.split('/');
	Date birthdate = Date.newInstance(Integer.valueOf(bdSplit[2]), //YYYY
									  Integer.valueOf(bdSplit[0]), //MM
									  Integer.valueOf(bdSplit[1])); //DD

	
	List<Contact> contList = [SELECT Id, AccountId, Birthdate
							  FROM Contact 
							  WHERE Email =: email 
							  AND FirstName =: firstName
							  AND LastName =: lastName];
	Id contactId;
		/** Create or update Contact data from Registration Form**/
		Contact cont = new Contact();
		cont.Is_Emancipated__c = (user.typeUser == 'emancipated');		
		cont.RecordTypeId = !isStudent ? PARENT_CONTACT_RT : STUDENT_CONTACT_RT;		
		cont.Birthdate = (isStudentUnder18) ? ((isStudent) ? birthdate : null) : birthdate;											  
		cont.MailingStreet = user.address;
		cont.MailingState = user.state;
		cont.MailingCity = user.city;
		cont.MailingPostalCode = user.zipCode;
		cont.MailingCountry = user.country;
		cont.Opt_in_to_Marketing__c = user.optInMarketing;
		cont.Opt_in_to_enrollment__c = user.optInEnrollment;
		cont.Phone = user.phone;
		cont.Phone_Country_Code__c = user.phoneArea;
		if(isStudentUnder18 && isStudent) {
			cont.Parent_Legal_Guardian_Email__c = user.parentEmail;
			cont.Parent_Legal_Guardian_Last_Name__c = user.parentLastName;
			cont.Parent_Legal_Guardian_Name__c = user.parentFirstName;
		}

	/** Check if contact exist with same data, or Lead exist to Convert, or if new contact will be created */
	if(contList.size() >= 1) {
		//If there are more than one Contact with the same email (because maybe other process duplicate contacts)
		//then considered only the first one.
	//	Contact toUpdate = contList[0];
		cont.Id = contList[0].Id;
		cont.AccountId = contList[0].AccountId;
		for(Contact contObj : contList){
            if(((isStudentUnder18 && isStudent) || !isStudentUnder18) && (contObj.Birthdate == birthdate)){				
				cont.Id = contObj.Id;
				cont.AccountId = contObj.AccountId;
			}
		}
       
        if(cont.AccountId != act.Id) {
			//Always update the AccountId for Contact.
			cont.AccountId = act.Id;		
		}
		update cont;
		contactId = cont.Id;
	}
	else { //(contList.size() == 0)
		contactId =  findOrConvertLead(email, act, isStudent);
		System.debug('findOrConvertLead contactId ' + contactId);
        //Create and fill contact info from Registration Form
			if(contactId == null){
				cont.AccountId = act.Id;
				cont.OwnerId = ownerId;
				cont.Email = email;
				cont.FirstName = firstName;
				cont.LastName =  lastName;				
				try{
					insert cont;
					contactId = cont.Id;
				}
				catch(Exception ex) {
					throw new CommunityManagementCustomException('Error creating Contact');
				}
			}
			else{ //Lead convertion was successfull			
				cont.Id = contactId;
				update cont;
				System.debug('UPdate Contact RT AFTER LEAD ' + cont);
			}	
	
	}
	return contactId;
}

private Id findOrConvertLead(String email, Account act, Boolean isStudent){
	String firstName = (isStudentUnder18) ? ((isStudent) ? (user.firstName) : (user.parentFirstName)) : user.firstName;
	String lastName =  (isStudentUnder18) ? ((isStudent) ? (user.lastName) : (user.parentLastName)) : user.lastName;
	List<Lead> leads = [SELECT Id, IsConverted, RecordTypeId 
						FROM Lead 
						WHERE Email =: email 
						AND FirstName =: firstName
						AND LastName =: lastName
						AND isConverted = FALSE 
						AND RecordTypeId =: B2C_LEAD_RT];
						
    System.debug('Leads ' + leads);
	if(leads.size() >= 1) {
		//Only convert the first Lead found
		return convertLead(leads[0], act);
	}
	// else lead.size() == 0
	return null;
}

private Id convertLead(Lead ld, Account act){

//	LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true AND MasterLabel = 'Qualified'];
	Database.LeadConvert lc = new Database.LeadConvert();

	lc.setLeadId(ld.Id);
	if(act.Id != null) {
		lc.setAccountId(act.Id);
	}

    lc.setDoNotCreateOpportunity(True);
   // String masterLabel = ld.RecordTypeId == B2S_LEAD_RT ?  'Qualified' : 'Enrolled';
    String masterLabel = 'Enrolled';
  
	lc.setConvertedStatus(masterLabel);
	Id contactId;
	try{
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		if(lcr.isSuccess()) {
			contactId = lcr.getContactId();		
		}
	}
	catch(Exception ex) {
		//	System.debug('Error converting Lead');
		throw new CommunityManagementCustomException('Error converting Lead');
	}

	return contactId;
}

private void findStudentApplications(String email, Id contactId, Boolean isStudent){
	
	String query = 'SELECT Id, Contact__c FROM Application__c WHERE ';
	if(isStudent) {
		query += ' Student_email_address__c =: email ';
	}
	else{
		query += ' Parent_Legal_Guardian_Email_Address__c =: email ';
	}
	query += ' AND Contact__c = \'\'';

	try{
		List<sObject> applications = Database.query(query);
		List<Application__c> applicationsToUpdate = new List<Application__c>();
		for(sObject app : applications) {
			Application__c appObj = (Application__c) app;
			appObj.Contact__c = contactId;
			applicationsToUpdate.add(appObj);
		}
		update applicationsToUpdate;
	} catch(Exception ex){
		throw new CommunityManagementCustomException('Error associating SA with Contact');
	}
	
}

@future
public static void updateStudentUserProfile(Id userId)
{   
	 // Get those records based on the IDs
	 List<User> userList = [SELECT Id, ProfileId, ContactId, Contact.RecordTypeId 
						    FROM User 
							WHERE Id =: userId];
     if(userList.size() > 0){
		HA_Management_Settings__c haSettings = HA_Management_Settings__c.getOrgDefaults();
		String studentProfileId = haSettings.Student_Profile_Id__c; 
		String studentRT = haSettings.Student_RT__c; 
		User u  = userList[0];	
		if(!String.isEmpty(studentProfileId) && !String.isEmpty(studentRT)
			&& (u.ContactId != null ) && u.Contact.RecordTypeId == studentRT ){
				u.ProfileId = studentProfileId;
				update u;
		}		
	 }
     							
	 // Process records
}

public class CommunityUser {
@AuraEnabled
public String firstName {set; get;}
@AuraEnabled
public String lastName {set; get;}
@AuraEnabled
public String email {set; get;}
@AuraEnabled
public String confirmEmail {set; get;}
@AuraEnabled
public String password {set; get;}
@AuraEnabled
public String repeatedPassw {set; get;}
// @AuraEnabled
// public String communityNickName {set;get;}
@AuraEnabled
public String phone {set; get;}
@AuraEnabled
public String phoneArea {set; get;}
@AuraEnabled
public String birthday {set; get;}
@AuraEnabled
public Integer userAge {set; get;}
@AuraEnabled
public String address {set; get;}
@AuraEnabled
public String city {set; get;}
@AuraEnabled
public String state {set; get;}
@AuraEnabled
public String zipCode {set; get;}
@AuraEnabled
public String country {set; get;}
@AuraEnabled
public String typeUser {set; get;}
@AuraEnabled
public String parentFirstName {set; get;}
@AuraEnabled
public String parentLastName {set; get;}
@AuraEnabled
public String parentEmail {set; get;}
@AuraEnabled
public Boolean optInMarketing {set; get;}
@AuraEnabled
public Boolean optInEnrollment {set; get;}
@AuraEnabled
public Boolean privacy {set; get;}
}
}