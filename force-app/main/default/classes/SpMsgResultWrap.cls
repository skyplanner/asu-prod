/*
 * @author				Yaneivys Gutierrez
 * @date				05/04/2022
 * @description			Message Result for LWC Wrap
 * @group
 * @last modified on	05/04/2022
 * @last modified by	05/04/2022
 * Modifications Log
 * Ver		Date			Author					Modification
 * 1.0		05/04/2022		Yaneivys Gutierrez		Initial Version
 */
public inherited sharing class SpMsgResultWrap {
	@AuraEnabled
	public String title { get; set; }

	@AuraEnabled
	public String msg { get; set; }

	@AuraEnabled
	public String variant { get; set; }

	@AuraEnabled
	public SpResultData data { get; set; }
}
