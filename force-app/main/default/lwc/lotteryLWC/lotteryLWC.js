import { LightningElement } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getOptions from "@salesforce/apex/SpLotteryCntrl.getOptions";
import fetchLotteries from "@salesforce/apex/SpLotteryCntrl.fetchLotteries";
import runLottery from "@salesforce/apex/SpLotteryCntrl.runLottery";
import SR_LOTTERY from "@salesforce/resourceUrl/lotteryResources";
import LABEL_LOTTERY_NO_DATA from "@salesforce/label/c.SP_LOTTERY_NO_DATA";
import LABEL_ACTIVE_LOT from "@salesforce/label/c.SP_ACTIVE_LOT";
import LABEL_INACTIVE_LOT from "@salesforce/label/c.SP_INACTIVE_LOT";
import LABEL_CREATE_LOT from "@salesforce/label/c.SP_CREATE_LOT";

export default class LotteryLWC extends NavigationMixin(LightningElement) {
	labels = {
		noData: LABEL_LOTTERY_NO_DATA,
		createLot: LABEL_CREATE_LOT,
		activeTitle: LABEL_ACTIVE_LOT,
		inactiveTitle: LABEL_INACTIVE_LOT
	};
	loading = true;
	lotteriesActive;
	lotteriesInactive;
	hasActiveLotteries;
	hasInactiveLotteries;

	gradeOptions;
	gradeOptionsDependencies;
	siteOptions;
	siteComboDisabled;
	termOptions;

	selectedGrade;
	selectedSite;
	selectedTerm;
	year;
	seats;

	runDisabled;

	canEdit = false;

	lotteryIcon;
	lotteryBallIcon;
	lotteryBillIcon;

	connectedCallback() {
		this.lotteryIcon = SR_LOTTERY + "/img/lottery.svg";
		this.lotteryBallsIcon = SR_LOTTERY + "/img/lottery-balls.svg";

		this.loadOptions();
		this.loadLotteries();
	}

	//#region ( Fetching Data Methods )

	loadOptions() {
		getOptions({})
			.then((r) => {
				this.canEdit = r.canEdit;
				this.gradeOptions = r.gradeOptions;
				this.gradeOptionsDependencies = r.gradeOptionsDependencies;
				this.siteOptions = [];
				this.siteComboDisabled = true;
				this.termOptions = r.termOptions;
				this.updateRunDisabled();
				this.loading = false;
			})
			.catch((error) => {
				this.onError(error);
				this.updateRunDisabled();
				this.loading = false;
			});
	}

	loadLotteries() {
		fetchLotteries({})
			.then((r) => {
				let nameUrl;
				this.lotteriesActive = r.activeList.map((row) => {
					nameUrl = `/${row.Id}`;
					return { ...row, nameUrl };
				});
				this.lotteriesInactive = r.inactiveList.map((row) => {
					nameUrl = `/${row.Id}`;
					return { ...row, nameUrl };
				});
				this.hasActiveLotteries = this.lotteriesActive.length > 0;
				this.hasInactiveLotteries = this.lotteriesInactive.length > 0;
			})
			.catch((error) => {
				this.onError(error);
			});
	}

	//#endregion

	//#region ( Field Selection On Change )

	gradeOnChange(event) {
		if (this.selectedGrade != event.target.value) {
			this.selectedSite = null;
		}
		this.selectedGrade = event.target.value;
		this.siteOptions = this.gradeOptionsDependencies[this.selectedGrade];
		this.siteComboDisabled = this.siteOptions.length <= 0;

		this.updateRunDisabled();
	}

	siteOnChange(event) {
		this.selectedSite = event.target.value;
		this.updateRunDisabled();
	}

	termOnChange(event) {
		this.selectedTerm = event.target.value;
		this.updateRunDisabled();
	}

	yearOnChange(event) {
		this.year = event.target.value;
		this.updateRunDisabled();
	}

	seatsOnChange(event) {
		this.seats = event.target.value;
		this.updateRunDisabled();
	}

	//#endregion

	//#region ( Handle Child Actions )

	handleUpdateLotteries(event) {
		this.loadLotteries();
	}

	//#endregion

	//#region ( Actions )

	runLottery(event) {
		this.loading = true;

		if (this.runDisabled) {
			this.showMsgLwc("error", "Error!", labels.noData);
			this.loading = false;
			return;
		}
		runLottery({
			setup: {
				grade: this.selectedGrade,
				site: this.selectedSite,
				term: this.selectedTerm,
				year: this.year,
				seats: this.seats
			}
		})
			.then((r) => {
				this.showMsgLwc(r.variant, r.title, r.msg);
				if (r.variant == "success") {
					this[NavigationMixin.Navigate]({
						type: "standard__recordPage",
						attributes: {
							objectApiName: "Lottery__c",
							recordId: r.data.lotteryId,
							actionName: "view"
						}
					});
				}
				this.loading = false;
			})
			.catch((error) => {
				this.onError(error);
				this.loading = false;
			});
	}

	updateRunDisabled() {
		this.runDisabled =
			!this.selectedGrade ||
			!this.selectedSite ||
			!this.selectedTerm ||
			!this.year ||
			!this.seats;
	}

	//#endregion

	//#region ( Message Methods )

	onSuccess(msg) {
		this.showMsgLwc("success", "Success!", msg);
	}

	onError(err) {
		console.log("error:", err);
		this.showMsgLwc("error", "Error!", err.body.message);
	}

	showMsgLwc(aVariant, aTitle, aMessage) {
		const evt = new ShowToastEvent({
			title: aTitle,
			message: aMessage,
			variant: aVariant
		});
		this.dispatchEvent(evt);
	}

	//#endregion
}
