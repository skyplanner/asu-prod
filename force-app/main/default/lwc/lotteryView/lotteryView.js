import { LightningElement, api } from "lwc";
import { getRecordNotifyChange } from "lightning/uiRecordApi";
import { loadStyle } from "lightning/platformResourceLoader";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import fetchLotteryData from "@salesforce/apex/SpLotteryViewCntrl.fetchLotteryData";
import addAppsToLottery from "@salesforce/apex/SpLotteryViewCntrl.addAppsToLottery";
import saveLottery from "@salesforce/apex/SpLotteryViewCntrl.saveLottery";
import USER_ID from "@salesforce/user/Id";
import SR_LOTTERY from "@salesforce/resourceUrl/lotteryResources";
import LABEL_LOTTERY_ISSUE_ADDING_APPS from "@salesforce/label/c.SP_LOTTERY_ISSUE_ADDING_APPS";
import LABEL_SELECTED_APPS from "@salesforce/label/c.SP_SELECTED_APPS";
import LABEL_NO_SELECTED_APPS from "@salesforce/label/c.SP_NO_SELECTED_APPS";
import LABEL_WAITING_LIST from "@salesforce/label/c.SP_WAITING_LIST";
import LABEL_NO_WAITING_LIST from "@salesforce/label/c.SP_NO_WAITING_LIST";
import LABEL_VAL_NULL from "@salesforce/label/c.SP_VAL_NULL";
import LABEL_VAL_UNDERFLOW from "@salesforce/label/c.SP_VAL_UNDERFLOW";

const columnsSelected = [
	{
		label: "Lottery Selection No.",
		fieldName: "Lottery_Selection_Number__c",
		editable: false
	},
	{
		label: "Application",
		fieldName: "nameUrl",
		type: "url",
		typeAttributes: { label: { fieldName: "Name" }, target: "_blank" }
	},
	{
		label: "Student Last Name",
		fieldName: "Student_Last_Name__c",
		editable: false
	},
	{
		label: "Student First Name",
		fieldName: "Student_First_Name__c",
		editable: false
	},
	{
		label: "Status",
		fieldName: "Application_Status__c",
		editable: false
	},
	{
		label: "Term Applying for",
		fieldName: "Term_Applying_for__c",
		editable: false
	},
	{
		label: "Grade Applying For",
		fieldName: "Grade_Applying_For__c",
		editable: false
	},
	{
		label: "Owner",
		fieldName: "alias",
		editable: false
	},
	{
		label: "Created Date",
		fieldName: "CreatedDate",
		type: "date",
		editable: false
	},
	{
		label: "Lottery Status",
		fieldName: "Lottery_Status__c",
		editable: false
	},
	{
		label: "Submission Date",
		fieldName: "Pending_Final_Review_Date__c",
		type: "date",
		editable: false,
		typeAttributes: {
			day: "numeric",
			month: "short",
			year: "numeric",
			hour: "2-digit",
			minute: "2-digit",
			second: "2-digit",
			hour12: true
		}
	}
];
const columnsWaitingList = [
	{
		label: "Waiting List Order",
		fieldName: "Lottery_WL_No__c",
		editable: false
	},
	{
		label: "Application",
		fieldName: "nameUrl",
		type: "url",
		typeAttributes: { label: { fieldName: "Name" }, target: "_blank" }
	},
	{
		label: "Student Last Name",
		fieldName: "Student_Last_Name__c",
		editable: false
	},
	{
		label: "Student First Name",
		fieldName: "Student_First_Name__c",
		editable: false
	},
	{
		label: "Status",
		fieldName: "Application_Status__c",
		editable: false
	},
	{
		label: "Term Applying for",
		fieldName: "Term_Applying_for__c",
		editable: false
	},
	{
		label: "Grade Applying For",
		fieldName: "Grade_Applying_For__c",
		editable: false
	},
	{
		label: "Owner",
		fieldName: "alias",
		editable: false
	},
	{
		label: "Created Date",
		fieldName: "CreatedDate",
		type: "date",
		editable: false
	},
	{
		label: "Lottery Status",
		fieldName: "Lottery_Status__c",
		editable: false
	},
	{
		label: "Submission Date",
		fieldName: "Pending_Final_Review_Date__c",
		type: "date",
		editable: false,
		typeAttributes: {
			day: "numeric",
			month: "short",
			year: "numeric",
			hour: "2-digit",
			minute: "2-digit",
			second: "2-digit",
			hour12: true
		}
	}
];

export default class LotteryViewLWC extends LightningElement {
	@api recordId;
	labels = {
		issueAddingApp: LABEL_LOTTERY_ISSUE_ADDING_APPS,
		selectedApps: LABEL_SELECTED_APPS,
		noSelectedApps: LABEL_NO_SELECTED_APPS,
		waitingList: LABEL_WAITING_LIST,
		noWaitingList: LABEL_NO_WAITING_LIST,
		validationNull: LABEL_VAL_NULL,
		validationUnderflow: LABEL_VAL_UNDERFLOW
	};
	lottery = {
		id: "",
		name: "",
		grade: "",
		site: "",
		term: "",
		year: "",
		availability: 0,
		initialSeats: 0,
		active: true,
		inactivateDate: null
	};
	seats;
	canEdit = false;
	modeEdit = false;
	loading = true;
	lotteryExist = false;
	lotteryIcon;
	selectedApps = [];
	waitingList = [];
	columnsSelected = columnsSelected;
	columnsWaitingList = columnsWaitingList;
	rowOffset = 0;
	hasSelectedApps;
	hasWaitingList;

	availabilityText = "0 seats available";
	availabilityClass = "";
	noAvailability = true;
	selectionRows = [];
	selectedAppIds = [];

	constructor() {
		super();
		Promise.all([
			loadStyle(this, SR_LOTTERY + "/css/lotteryView.css")
		]).then(() => {});
	}

	connectedCallback() {
		this.lotteryIcon = SR_LOTTERY + "/img/lottery.svg";
		this.loadRelatedList();
	}

	loadRelatedList() {
		fetchLotteryData({
			lId: this.recordId
		})
			.then((r) => {
				this.updateLotteryData(r);
				this.loading = false;
			})
			.catch((error) => {
				this.loading = false;
				this.onError(error);
			});
	}

	updateLotteryData(r) {
		this.lottery = {
			id: r.lottery.Id,
			name: r.lottery.Name,
			grade: r.lottery.Grade_Applying_For__c,
			site: r.lottery.Site_of_Interest__c,
			term: r.lottery.Term_Applying_for__c,
			year: r.lottery.Year_applying_for__c,
			availability: r.lottery.Available_Seats__c,
			initialSeats: r.lottery.Seats__c,
			active: r.lottery.Active__c,
			inactivateDate: r.lottery.Inactivate_Date__c
		};
		this.lotteryExist = true;
		this.seats = this.lottery.initialSeats;
		this.canEdit = r.canEdit;
		if (!this.lottery.active) {
			this.canEdit = false;
		}

		this.availabilityText =
			(this.lottery.availability == 0
				? "No"
				: this.lottery.availability) +
			" seat" +
			(this.lottery.availability == 0 || this.lottery.availability == 1
				? ""
				: "s") +
			" available";
		this.availabilityClass =
			"availability-text " +
			(this.lottery.availability == 0 ? "no-available" : "available");

		let nameUrl;
		let alias;
		this.selectedApps = r.selectedApps.map((row) => {
			nameUrl = `/${row.Id}`;
			alias = row.Owner.Alias;
			return { ...row, nameUrl, alias };
		});
		this.waitingList = r.waitingList.map((row) => {
			nameUrl = `/${row.Id}`;
			alias = row.Owner.Alias;
			return { ...row, nameUrl, alias };
		});
		this.hasSelectedApps = this.selectedApps.length > 0;
		this.hasWaitingList = this.waitingList.length > 0;

		getRecordNotifyChange([{ recordId: this.lottery.id }]);
		this.updateAvailabilityOption();
	}

	updateAvailabilityOption() {
		this.noAvailability =
			this.lottery.availability == 0 ||
			this.selectedAppIds.length == 0 ||
			this.selectedAppIds.length > this.lottery.availability;
	}

	updateSelection(event) {
		this.selectedAppIds = [];
		var selectedRows = event.detail.selectedRows;

		for (var i = 0; i < selectedRows.length; i++) {
			this.selectedAppIds.push(selectedRows[i].Id);
		}

		this.updateAvailabilityOption();
	}

	addToLottery(event) {
		this.loading = true;
		if (this.lottery.availability == 0) {
			this.loading = false;
			return;
		}
		if (this.selectedAppIds.length > this.lottery.availability) {
			this.showMsgLwc(
				"error",
				"Error",
				this.labels.issueAddingApp.replace(
					"{0}",
					this.lottery.availability
				)
			);
			this.loading = false;
			return;
		}

		this.selectionRows = [];

		addAppsToLottery({
			lId: this.recordId,
			apps: this.selectedAppIds
		})
			.then((r) => {
				this.selectedAppIds = [];
				this.updateLotteryData(r);
				this.loading = false;
			})
			.catch((error) => {
				this.selectionRows = this.selectedAppIds;
				this.loading = false;
				this.onError(error);
			});
	}

	toogleMode(event) {
		this.modeEdit = !this.modeEdit;
	}

	handleSubmit(event) {
		event.preventDefault();
		this.save();
	}

	handleSave(event) {
		this.save();
	}

	handleCancel(event) {
		this.seats = this.lottery.initialSeats;
		this.toogleMode(event);
	}

	updateSeats(event) {
		this.seats = event.detail.value;
	}

	save() {
		if (!this.seats) {
			this.showMsgLwc("error", "Error!", this.labels.validationNull);
			return;
		}
		if (this.lottery.initialSeats > this.seats) {
			this.showMsgLwc("error", "Error!", this.labels.validationUnderflow);
			return;
		}
		if (this.lottery.initialSeats == this.seats) {
			this.toogleMode(null);
			return;
		}
		this.loading = true;
		saveLottery({
			l: {
				Id: this.lottery.id,
				Seats__c: this.seats
			}
		})
			.then((r) => {
				this.updateLotteryData(r);
				this.toogleMode(null);
				this.loading = false;
			})
			.catch((error) => {
				this.loading = false;
				this.onError(error);
			});
	}

	//#region ( Message Methods )

	onSuccess(msg) {
		this.showMsgLwc("success", "Success!", msg);
	}

	onError(err) {
		console.log("error:", err);
		this.showMsgLwc("error", "Error!", err.body.message);
	}

	showMsgLwc(aVariant, aTitle, aMessage) {
		const evt = new ShowToastEvent({
			title: aTitle,
			message: aMessage,
			variant: aVariant
		});
		this.dispatchEvent(evt);
	}

	//#endregion
}
