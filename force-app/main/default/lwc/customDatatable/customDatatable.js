import LightningDatatable from "lightning/datatable";
import { loadStyle } from "lightning/platformResourceLoader";
import SR_LOTTERY from "@salesforce/resourceUrl/lotteryResources";
import picklistTemplate from "./picklist.html";

export default class CustomDatatable extends LightningDatatable {
	static customTypes = {
		picklist: {
			template: picklistTemplate,
			typeAttributes: [
				"label",
				"value",
				"variant",
				"disabled",
				"placeholder",
				"options",
				"context"
			]
		}
	};

	constructor() {
		super();
		Promise.all([
			loadStyle(this, SR_LOTTERY + "/css/customDatatable.css")
		]).then(() => {});
	}
}
