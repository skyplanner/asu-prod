import { LightningElement, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getOptions from "@salesforce/apex/SpLotteryCntrl.getOptions";
import inactivateProcess from "@salesforce/apex/SpLotteryCntrl.inactivateProcess";
import LABEL_NO_LOTTERY from "@salesforce/label/c.SP_NO_LOTTERY";

const columns = [
	{
		label: "Grade Applying For",
		fieldName: "Grade_Applying_For__c",
		type: "picklist",
		typeAttributes: {
			label: "",
			variant: "label-hidden",
			value: { fieldName: "Grade_Applying_For__c" },
			disabled: true,
			options: [],
			context: { fieldName: "Id" } // binding account Id with context variable to be returned back
		}
	},
	{
		label: "Site of Interest",
		fieldName: "Site_of_Interest__c",
		type: "picklist",
		typeAttributes: {
			label: "",
			variant: "label-hidden",
			value: { fieldName: "Site_of_Interest__c" },
			disabled: true,
			options: [],
			context: { fieldName: "Id" } // binding account Id with context variable to be returned back
		}
	},
	{
		label: "Term Applying for",
		fieldName: "Term_Applying_for__c"
	},
	{
		label: "Year applying for",
		fieldName: "Year_applying_for__c"
	},
	{
		label: "Seats",
		fieldName: "Seats__c",
		type: "number"
	},
	{
		label: "Available Seats",
		fieldName: "Available_Seats__c",
		type: "number"
	},
	{
		label: "Run Date",
		fieldName: "Run_Date__c",
		type: "date"
	}
];

export default class LotteryListLWC extends LightningElement {
	@api lotteries;
	@api hasLottery;
	@api active;
	gradeOptions;
	siteOptions;
	labels = {
		noLottery: LABEL_NO_LOTTERY
	};
	columns;
	rowOffset = 0;

	connectedCallback() {
		getOptions({})
			.then((r) => {
				this.canEdit = r.canEdit;
				this.gradeOptions = r.gradeOptions;
				this.siteOptions = r.siteOptions;

				this.updateOptions();
				this.updateColumns();
			})
			.catch((error) => {
				this.onError(error);
			});
	}

	updateOptions() {
		this.columns = columns.map((data) => {
			if (data.label == "Grade Applying For") {
				data.typeAttributes.options = this.gradeOptions;
			} else if (data.label == "Site of Interest") {
				data.typeAttributes.options = this.siteOptions;
			}
			return { ...data };
		});
	}

	updateColumns() {
		this.columns.unshift({
			label: "Lottery",
			fieldName: "nameUrl",
			type: "url",
			typeAttributes: {
				label: { fieldName: "Name" }
			}
		});
		if (this.active == "true" && this.canEdit) {
			this.columns.push({
				label: "Action",
				type: "button",
				cellAttributes: {
					class: "custom-action"
				},
				typeAttributes: {
					label: "Deactivate",
					name: "deactivate",
					variant: "base"
				}
			});
		} else if (this.active != "true") {
			this.columns.push({
				label: "Inactivated Date",
				fieldName: "Inactivate_Date__c",
				type: "date",
				editable: false
			});
		}
	}

	callRowAction(event) {
		var recordId = event.detail.row.Id;
		var actionName = event.detail.action.name;
		if (actionName === "deactivate") {
			this.inactivateLottery(recordId);
		}
	}

	inactivateLottery(lotteryId) {
		inactivateProcess({
			lId: lotteryId
		})
			.then((r) => {
				this.showMsgLwc(r.variant, r.title, r.msg);
				var updateEvent = new CustomEvent("updatelotteries", {
					detail: true
				});
				this.dispatchEvent(updateEvent);
			})
			.catch((error) => {
				this.onError(error);
			});
	}

	//#region ( Message Methods )

	onSuccess(msg) {
		this.showMsgLwc("success", "Success!", msg);
	}

	onError(err) {
		console.log("error:", err);
		this.showMsgLwc("error", "Error!", err.body.message);
	}

	showMsgLwc(aVariant, aTitle, aMessage) {
		const evt = new ShowToastEvent({
			title: aTitle,
			message: aMessage,
			variant: aVariant
		});
		this.dispatchEvent(evt);
	}

	//#endregion
}
