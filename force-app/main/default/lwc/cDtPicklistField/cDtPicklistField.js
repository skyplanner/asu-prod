import { LightningElement, api } from "lwc";

export default class CDtPicklistField extends LightningElement {
	@api label;
	@api value;
	@api variant;
	@api disabled;
	@api placeholder;
	@api options;
	@api context;

	handleChange(event) {
		//show the selected value on UI
		this.value = event.detail.value;

		//fire event to send context and selected value to the data table
		this.dispatchEvent(
			new CustomEvent("picklistchanged", {
				composed: true,
				bubbles: true,
				cancelable: true,
				detail: {
					data: { context: this.context, value: this.value }
				}
			})
		);
	}
}
